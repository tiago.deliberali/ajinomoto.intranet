﻿using System;
using System.Linq;
using Microsoft.SharePoint;
using Ajinomoto.Intranet.Workflow.Business;
using System.Web.UI.WebControls.WebParts;
using DDay.iCal;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using Ajinomoto.Intranet.Business.Aniversariantes;
using Microsoft.SharePoint.Workflow;

namespace SharePointConsoleApplication1
{
    class Program
    {
        public static SPSite site;
        public delegate void Acao(string[] parametros);

        static void Main(string[] args)
        {
            ExecutarUsandoEstruturaBase((dados) => {
                var itens = AprovaLista.Aprovar(dados, site);

                foreach (var item in itens)
                {
                    Console.WriteLine(item.Title + " - " + item.ModerationInformation.Status);
                }
            });
        }

        public static void ExecutarUsandoEstruturaBase(Acao acao)
        {
            var eventos = new HandleEventFiring();

            try
            {
                eventos.DesabilitarEventos();

                Console.WriteLine("Digite a url do site (v4):");
                var entrada = Console.ReadLine();

                var dados = entrada.Split(';');
                site = new SPSite(dados[0]);

                acao(dados);
            }
            catch (Exception ex)
            {
                var error = ex;
                while (error.InnerException != null)
                    error = error.InnerException;

                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("finalizado...");

                eventos.HabilitarEventos();
                Console.ReadLine();
            }
        }
    }
}
