﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;

namespace SharePointConsoleApplication1
{
    public class AprovaLista
    {
        public static List<SPListItem> Aprovar(string[] dados, SPSite site)
        {
            string webName = dados[1];
            string listName = dados[2];

            var itens = new List<SPListItem>();

            var web = site.AllWebs[webName];
            var list = web.Lists[listName];

            SPQuery q = new SPQuery();
            q.Query = "<Where><And><Eq><FieldRef Name=\"_ModerationStatus\" /><Value Type=\"ModStat\">2</Value></Eq></And></Where>";

            var listItems = list.GetItems(q);

            foreach (SPListItem item in listItems)
            {
                itens.Add(item);
            }

            return itens;
        }
    }
}
