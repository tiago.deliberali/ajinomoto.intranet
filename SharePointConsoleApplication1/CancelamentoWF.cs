﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;

namespace SharePointConsoleApplication1
{
    public class CancelamentoWF
    {
        public static void CancelarTodosWF(SPSite site)
        {
            int count = 0;

            for (int webPos = site.AllWebs.Count - 1; webPos >= 0; webPos--)
            {
                SPWeb web = site.AllWebs[webPos];

                for (int listPos = web.Lists.Count - 1; listPos >= 0; listPos--)
                {
                    SPList lista = web.Lists[listPos];

                    Console.WriteLine("processando lista " + lista.Title + " (" + lista.ItemCount + " itens)...");
                    count = 0;
                    var items = new List<Guid>();

                    for (var itemPos = lista.Items.Count - 1; itemPos >= 0; itemPos--)
                    {
                        SPListItem item = lista.Items[itemPos];

                        for (int wfPos = item.Workflows.Count - 1; wfPos >= 0; wfPos--)
                        {
                            SPWorkflow wf = item.Workflows[wfPos];

                            if (wf.InternalState != SPWorkflowState.Cancelled
                                    && wf.InternalState != SPWorkflowState.Cancelled
                                    && wf.InternalState != SPWorkflowState.Completed
                                    && wf.InternalState != SPWorkflowState.Expired
                                    && wf.InternalState != SPWorkflowState.Terminated)
                            {
                                Console.WriteLine("   " + item.Title + " - " + wf.InternalState);

                                items.Add(item.UniqueId);
                                count++;

                                if (count > 200)
                                    return;
                            }
                        }
                    }

                    if (items.Count > 0)
                    {
                        Console.WriteLine("Deseja cancelar esses wf (s/n)?");
                        var cancelar = Console.ReadLine() == "s";
                        var listaEmProcessamento = web.Lists[lista.ID];

                        if (cancelar)
                        {
                            foreach (var id in items)
                            {
                                SPListItem item = listaEmProcessamento.Items[id];

                                for (int i = item.Workflows.Count - 1; i >= 0; i--)
                                {
                                    CancelarWf(item.Workflows[i]);
                                }
                            }
                        }
                    }
                }
            }
        }

        private static void CancelarWf(SPWorkflow wf)
        {
            try
            {
                SPWorkflowManager.CancelWorkflow(wf);
            }
            catch (Exception ex)
            {
                var erro = ex;

                while (erro.InnerException != null)
                {
                    erro = erro.InnerException;
                }

                Console.WriteLine("      erro: " + ex.Message);
                // nothing
            }
        }
    }
}
