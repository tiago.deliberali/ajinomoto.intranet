﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace Ajinomoto.FabricaIdeia.FluxoFabrica
{
    public sealed partial class FluxoFabrica
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind3 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind4 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind5 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind6 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind7 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind8 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind9 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind10 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind11 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind12 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind13 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind14 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind15 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind16 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind17 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind18 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind19 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind20 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind21 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind22 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind23 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind24 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind25 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind26 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind27 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind28 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind29 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind30 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Activities.CodeCondition codecondition1 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition2 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition3 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition4 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition5 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition6 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition7 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition8 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition9 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition10 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference1 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.CodeCondition codecondition11 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition12 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition13 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Runtime.CorrelationToken correlationtoken1 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind31 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind32 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind33 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind34 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind35 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind36 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind37 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken2 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind38 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind39 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind40 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind41 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind42 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind43 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken3 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind44 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind45 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind46 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind47 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind48 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind49 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind50 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind51 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind52 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind53 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken4 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind54 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind55 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken5 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind56 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind57 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken6 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind58 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind59 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken7 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind60 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind61 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken8 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind62 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind63 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken9 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind64 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind65 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken10 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind66 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken11 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind67 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind68 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind69 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind70 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind71 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind72 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind73 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind74 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind75 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind76 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind77 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind78 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind79 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken12 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind80 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind81 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind82 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken13 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind83 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken14 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind84 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind85 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind86 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind87 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind88 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind89 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind90 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind91 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind92 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind93 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken15 = new System.Workflow.Runtime.CorrelationToken();
            this.logErroExecucaoImplantacao = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.atribuirStatusInviavel = new Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity();
            this.DobrarPontuacao = new System.Workflow.Activities.CodeActivity();
            this.atribuirStatusImplantada = new Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity();
            this.logerroInicioImplantacao = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.logErroExecucaoDecisao = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.setStateActivity5 = new System.Workflow.Activities.SetStateActivity();
            this.prepararSolicitacaoComite = new System.Workflow.Activities.CodeActivity();
            this.setStateActivity4 = new System.Workflow.Activities.SetStateActivity();
            this.enviarEmailGrupoActivity1 = new Ajinomoto.FabricaIdeia.Business.Activities.EnviarEmailGrupoActivity();
            this.atribuirStatusWorkflowActivity1 = new Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity();
            this.AtribuirPontos = new System.Workflow.Activities.CodeActivity();
            this.setStateActivity3 = new System.Workflow.Activities.SetStateActivity();
            this.logeErroInicioDecisao = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.logFaultExecucaoAreaImpactada = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.logFailInicioAreaImpactada = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.setStateActivity8 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity7 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity6 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity1 = new System.Workflow.Activities.SetStateActivity();
            this.setStateComiteQA = new System.Workflow.Activities.SetStateActivity();
            this.setStateComiteRH = new System.Workflow.Activities.SetStateActivity();
            this.setStateComiteMA = new System.Workflow.Activities.SetStateActivity();
            this.logFaultInicioComite = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.terminateActivityInitialState = new System.Workflow.ComponentModel.TerminateActivity();
            this.logErroInitialState = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.setIniciadoSalvo = new System.Workflow.Activities.SetStateActivity();
            this.setIniciadoEnviado = new System.Workflow.Activities.SetStateActivity();
            this.logFaultConfirmacaoPiaExecucao = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.setMaisInformacaoPia = new System.Workflow.Activities.SetStateActivity();
            this.enviarEmailMaisInformacoes = new Ajinomoto.FabricaIdeia.Business.Activities.EnviarEmailGrupoActivity();
            this.atribuirStatusMaisInformacoesPia = new Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity();
            this.setReprovadoPia = new System.Workflow.Activities.SetStateActivity();
            this.enviarEmailIdeiaReprovada = new Ajinomoto.FabricaIdeia.Business.Activities.EnviarEmailGrupoActivity();
            this.atribuirStatusIdeiaReprovadaPia = new Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity();
            this.setAprovadoPia = new System.Workflow.Activities.SetStateActivity();
            this.logConfirmacaoPiaInicioErro = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.setStateFaultColaborador = new System.Workflow.Activities.SetStateActivity();
            this.logFaultColaborador = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.setItemSalvoState = new System.Workflow.Activities.SetStateActivity();
            this.setItemEnviadoState = new System.Workflow.Activities.SetStateActivity();
            this.faultHandlerExecucaoImplantacao = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseNaoImplantada = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseIdeiaImplantada = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerInicioImplantacao = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerExecucaoDecisao = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseBranchActivity9 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseDecisaoReprovar = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseDecisaoAprovar = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerInicioDecisao = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultExecucaoAreaImpactada = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultInicioAreaImpactada = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseBranchActivity15 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity14 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity13 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity12 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity11 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity10 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity8 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity7 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity6 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity5 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity4 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity3 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity2 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity1 = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultInicioComite = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandleInitialState = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseIniciadoSalvo = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseIniciadoEnviado = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultConfirmacaoPiaExecucao = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseMaisInformacao = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifIdeiaReprovada = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifIdeiaAprovada = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultConfirmacaoPiaInicio = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultActivityColaborador = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.elseItemSalvo = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifItemEnviado = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlersActivity8 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateConclusao = new System.Workflow.Activities.SetStateActivity();
            this.completeImplantacaoTask = new Microsoft.SharePoint.WorkflowActions.CompleteTask();
            this.ifElseActivity9 = new System.Workflow.Activities.IfElseActivity();
            this.atribuirCamposImplantacao = new System.Workflow.Activities.CodeActivity();
            this.logImplantacao = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.onTaskImplantacaoChanged = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.faultHandlersActivity7 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.atribuirStatusImplantacao = new Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity();
            this.logInicioImplantacao = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.createTaskImplantacao = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.faultHandlersActivity6 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseActivity4 = new System.Workflow.Activities.IfElseActivity();
            this.completeDecisaoTask = new Microsoft.SharePoint.WorkflowActions.CompleteTask();
            this.CopiarDadosDecisaoIdeia = new System.Workflow.Activities.CodeActivity();
            this.onDecisaoTaskChanged = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.faultHandlersActivity5 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.atribuirStatusDecisao = new Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity();
            this.createTaskDecisao = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.calcularResultadoComite = new System.Workflow.Activities.CodeActivity();
            this.logIncioDecisao = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.faultHandlersExecucaoAreaImpactada = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateComite = new System.Workflow.Activities.SetStateActivity();
            this.completeTaskAreaImpcatada = new Microsoft.SharePoint.WorkflowActions.CompleteTask();
            this.copiarDadosAreaImpactada = new System.Workflow.Activities.CodeActivity();
            this.onTaskAreaImpactadaChanged = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.faultHandlersActivity3 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.enviarEmailAreaImpactada = new Ajinomoto.FabricaIdeia.Business.Activities.EnviarEmailGrupoActivity();
            this.atribuirStatusAreaImpactada = new Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity();
            this.logInicioAreaImpactada = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.createTaskAreaImpactada = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.ifElseActivity8 = new System.Workflow.Activities.IfElseActivity();
            this.logAreaOpcionalConcluido = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.completeTask7 = new Microsoft.SharePoint.WorkflowActions.CompleteTask();
            this.concluirAreaOpcional = new System.Workflow.Activities.CodeActivity();
            this.onTaskChanged7 = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.ifElseActivity7 = new System.Workflow.Activities.IfElseActivity();
            this.logEngenhanriaConcluido = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.completeTask6 = new Microsoft.SharePoint.WorkflowActions.CompleteTask();
            this.concluirEngenharia = new System.Workflow.Activities.CodeActivity();
            this.onTaskChanged6 = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.ifElseActivity6 = new System.Workflow.Activities.IfElseActivity();
            this.logCPDConcluido = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.completeTask5 = new Microsoft.SharePoint.WorkflowActions.CompleteTask();
            this.concluirCPD = new System.Workflow.Activities.CodeActivity();
            this.onTaskChanged5 = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.ifElseActivity5 = new System.Workflow.Activities.IfElseActivity();
            this.logSeguranca = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.completeTask4 = new Microsoft.SharePoint.WorkflowActions.CompleteTask();
            this.concluirSeguranca = new System.Workflow.Activities.CodeActivity();
            this.onTaskChanged4 = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.ifElseActivity3 = new System.Workflow.Activities.IfElseActivity();
            this.logQAConcluido = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.completeTask3 = new Microsoft.SharePoint.WorkflowActions.CompleteTask();
            this.concluirQA = new System.Workflow.Activities.CodeActivity();
            this.onTaskChanged3 = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.ifElseActivity2 = new System.Workflow.Activities.IfElseActivity();
            this.logRHConcluido = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.completeTask2 = new Microsoft.SharePoint.WorkflowActions.CompleteTask();
            this.concluirRH = new System.Workflow.Activities.CodeActivity();
            this.onTaskChanged2 = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.ifElseActivity1 = new System.Workflow.Activities.IfElseActivity();
            this.logMAConcluido = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.completeTask1 = new Microsoft.SharePoint.WorkflowActions.CompleteTask();
            this.concluirMA = new System.Workflow.Activities.CodeActivity();
            this.onTaskChanged1 = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.faultHandlersActivity4 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.createTaskAreaOpcional = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.createTaskEngenharia = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.createTaskCPD = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.createTaskSeguranca = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.createTaskQA = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.createTaskRH = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.createTaskMA = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.preparaCriacaoTasks = new System.Workflow.Activities.CodeActivity();
            this.atribuirStatusInicioComite = new Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity();
            this.logInicioComite = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.faultInicialState = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseInicio = new System.Workflow.Activities.IfElseActivity();
            this.logInicioWorkflow = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.onWorkflowIniciado = new Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated();
            this.faultHandlersActivity2 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseConfirmacaoPia = new System.Workflow.Activities.IfElseActivity();
            this.completeTaskConfirmacaoPia = new Microsoft.SharePoint.WorkflowActions.CompleteTask();
            this.atribuirCamposPia = new System.Workflow.Activities.CodeActivity();
            this.logConfirmacaoPiaExecucao = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.onTaskConfirmacaoPiaChanged = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.cancellationHandlerActivity1 = new System.Workflow.ComponentModel.CancellationHandlerActivity();
            this.faultHandlersActivity1 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.enviarEmailConfirmacaoPia = new Ajinomoto.FabricaIdeia.Business.Activities.EnviarEmailGrupoActivity();
            this.atribuirStatusConfirmacaoPia = new Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity();
            this.logConfirmacaoPiaEmail = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.createConfirmacaoPiaTask = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.faultColaborador = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseColaborador = new System.Workflow.Activities.IfElseActivity();
            this.logIdeiaStatus = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.onIdeiaChanged = new Microsoft.SharePoint.WorkflowActions.OnWorkflowItemChanged();
            this.eventDrivenImplantacao = new System.Workflow.Activities.EventDrivenActivity();
            this.stateInitializationImplantacao = new System.Workflow.Activities.StateInitializationActivity();
            this.eventDrivenDecisao = new System.Workflow.Activities.EventDrivenActivity();
            this.stateInitializationDecisao = new System.Workflow.Activities.StateInitializationActivity();
            this.eventDrivenAreaImpactada = new System.Workflow.Activities.EventDrivenActivity();
            this.initializationAreaImpactada = new System.Workflow.Activities.StateInitializationActivity();
            this.eventDrivenAreaOpcional = new System.Workflow.Activities.EventDrivenActivity();
            this.eventDrivenEngenharia = new System.Workflow.Activities.EventDrivenActivity();
            this.eventDrivenCPD = new System.Workflow.Activities.EventDrivenActivity();
            this.eventDrivenSeguranca = new System.Workflow.Activities.EventDrivenActivity();
            this.eventDrivenQA = new System.Workflow.Activities.EventDrivenActivity();
            this.eventDrivenRH = new System.Workflow.Activities.EventDrivenActivity();
            this.eventDrivenMA = new System.Workflow.Activities.EventDrivenActivity();
            this.initializationComiteAvaliador = new System.Workflow.Activities.StateInitializationActivity();
            this.eventDrivenInitialState = new System.Workflow.Activities.EventDrivenActivity();
            this.eventDrivenConfirmacaoPia = new System.Workflow.Activities.EventDrivenActivity();
            this.initializationConfirmacaoPia = new System.Workflow.Activities.StateInitializationActivity();
            this.eventDrivenColaborador = new System.Workflow.Activities.EventDrivenActivity();
            this.stateImplantacao = new System.Workflow.Activities.StateActivity();
            this.stateDecisao = new System.Workflow.Activities.StateActivity();
            this.stateAreaImpactada = new System.Workflow.Activities.StateActivity();
            this.stateComiteAvaliador = new System.Workflow.Activities.StateActivity();
            this.InitialState = new System.Workflow.Activities.StateActivity();
            this.stateConclusao = new System.Workflow.Activities.StateActivity();
            this.stateConfirmacaoPIA = new System.Workflow.Activities.StateActivity();
            this.stateColaborador = new System.Workflow.Activities.StateActivity();
            // 
            // logErroExecucaoImplantacao
            // 
            this.logErroExecucaoImplantacao.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logErroExecucaoImplantacao.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind1.Name = "FluxoFabrica";
            activitybind1.Path = "Workflow_HistoryDescription";
            activitybind2.Name = "FluxoFabrica";
            activitybind2.Path = "Workflow_HistoryOutcome";
            this.logErroExecucaoImplantacao.Name = "logErroExecucaoImplantacao";
            this.logErroExecucaoImplantacao.OtherData = "";
            this.logErroExecucaoImplantacao.UserId = -1;
            this.logErroExecucaoImplantacao.MethodInvoking += new System.EventHandler(this.logErroExecucaoImplantacao_MethodInvoking);
            this.logErroExecucaoImplantacao.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            this.logErroExecucaoImplantacao.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            // 
            // atribuirStatusInviavel
            // 
            activitybind3.Name = "FluxoFabrica";
            activitybind3.Path = "Ideia";
            this.atribuirStatusInviavel.Name = "atribuirStatusInviavel";
            this.atribuirStatusInviavel.Status = Ajinomoto.FabricaIdeia.Business.Activities.StatusWorkflow.Inviavel;
            this.atribuirStatusInviavel.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity.IdeiaProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind3)));
            // 
            // DobrarPontuacao
            // 
            this.DobrarPontuacao.Name = "DobrarPontuacao";
            this.DobrarPontuacao.ExecuteCode += new System.EventHandler(this.DobrarPontuacao_ExecuteCode);
            // 
            // atribuirStatusImplantada
            // 
            activitybind4.Name = "FluxoFabrica";
            activitybind4.Path = "Ideia";
            this.atribuirStatusImplantada.Name = "atribuirStatusImplantada";
            this.atribuirStatusImplantada.Status = Ajinomoto.FabricaIdeia.Business.Activities.StatusWorkflow.Implantado;
            this.atribuirStatusImplantada.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity.IdeiaProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind4)));
            // 
            // logerroInicioImplantacao
            // 
            this.logerroInicioImplantacao.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logerroInicioImplantacao.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind5.Name = "FluxoFabrica";
            activitybind5.Path = "Workflow_HistoryDescription";
            activitybind6.Name = "FluxoFabrica";
            activitybind6.Path = "Workflow_HistoryOutcome";
            this.logerroInicioImplantacao.Name = "logerroInicioImplantacao";
            this.logerroInicioImplantacao.OtherData = "";
            this.logerroInicioImplantacao.UserId = -1;
            this.logerroInicioImplantacao.MethodInvoking += new System.EventHandler(this.logerroInicioImplantacao_MethodInvoking);
            this.logerroInicioImplantacao.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind5)));
            this.logerroInicioImplantacao.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind6)));
            // 
            // logErroExecucaoDecisao
            // 
            this.logErroExecucaoDecisao.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logErroExecucaoDecisao.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind7.Name = "FluxoFabrica";
            activitybind7.Path = "Workflow_HistoryDescription";
            activitybind8.Name = "FluxoFabrica";
            activitybind8.Path = "Workflow_HistoryOutcome";
            this.logErroExecucaoDecisao.Name = "logErroExecucaoDecisao";
            this.logErroExecucaoDecisao.OtherData = "";
            this.logErroExecucaoDecisao.UserId = -1;
            this.logErroExecucaoDecisao.MethodInvoking += new System.EventHandler(this.logErroExecucaoDecisao_MethodInvoking);
            this.logErroExecucaoDecisao.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind7)));
            this.logErroExecucaoDecisao.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind8)));
            // 
            // setStateActivity5
            // 
            this.setStateActivity5.Name = "setStateActivity5";
            this.setStateActivity5.TargetStateName = "stateComiteAvaliador";
            // 
            // prepararSolicitacaoComite
            // 
            this.prepararSolicitacaoComite.Name = "prepararSolicitacaoComite";
            this.prepararSolicitacaoComite.ExecuteCode += new System.EventHandler(this.prepararSolicitacaoComite_ExecuteCode);
            // 
            // setStateActivity4
            // 
            this.setStateActivity4.Name = "setStateActivity4";
            this.setStateActivity4.TargetStateName = "stateConclusao";
            // 
            // enviarEmailGrupoActivity1
            // 
            this.enviarEmailGrupoActivity1.Grupo = "[Ideia]";
            this.enviarEmailGrupoActivity1.Name = "enviarEmailGrupoActivity1";
            activitybind9.Name = "FluxoFabrica";
            activitybind9.Path = "workflowProperties";
            this.enviarEmailGrupoActivity1.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.EnviarEmailGrupoActivity.WorkflowPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind9)));
            // 
            // atribuirStatusWorkflowActivity1
            // 
            activitybind10.Name = "FluxoFabrica";
            activitybind10.Path = "Ideia";
            this.atribuirStatusWorkflowActivity1.Name = "atribuirStatusWorkflowActivity1";
            this.atribuirStatusWorkflowActivity1.Status = Ajinomoto.FabricaIdeia.Business.Activities.StatusWorkflow.Cancelado;
            this.atribuirStatusWorkflowActivity1.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity.IdeiaProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind10)));
            // 
            // AtribuirPontos
            // 
            this.AtribuirPontos.Name = "AtribuirPontos";
            this.AtribuirPontos.ExecuteCode += new System.EventHandler(this.AtribuirPontos_ExecuteCode);
            // 
            // setStateActivity3
            // 
            this.setStateActivity3.Name = "setStateActivity3";
            this.setStateActivity3.TargetStateName = "stateImplantacao";
            // 
            // logeErroInicioDecisao
            // 
            this.logeErroInicioDecisao.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logeErroInicioDecisao.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind11.Name = "FluxoFabrica";
            activitybind11.Path = "Workflow_HistoryDescription";
            activitybind12.Name = "FluxoFabrica";
            activitybind12.Path = "Workflow_HistoryOutcome";
            this.logeErroInicioDecisao.Name = "logeErroInicioDecisao";
            this.logeErroInicioDecisao.OtherData = "";
            this.logeErroInicioDecisao.UserId = -1;
            this.logeErroInicioDecisao.MethodInvoking += new System.EventHandler(this.logeErroInicioDecisao_MethodInvoking);
            this.logeErroInicioDecisao.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind11)));
            this.logeErroInicioDecisao.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind12)));
            // 
            // logFaultExecucaoAreaImpactada
            // 
            this.logFaultExecucaoAreaImpactada.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logFaultExecucaoAreaImpactada.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind13.Name = "FluxoFabrica";
            activitybind13.Path = "Workflow_HistoryDescription";
            activitybind14.Name = "FluxoFabrica";
            activitybind14.Path = "Workflow_HistoryOutcome";
            this.logFaultExecucaoAreaImpactada.Name = "logFaultExecucaoAreaImpactada";
            this.logFaultExecucaoAreaImpactada.OtherData = "";
            this.logFaultExecucaoAreaImpactada.UserId = -1;
            this.logFaultExecucaoAreaImpactada.MethodInvoking += new System.EventHandler(this.logFaultExecucaoAreaImpactada_MethodInvoking);
            this.logFaultExecucaoAreaImpactada.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind13)));
            this.logFaultExecucaoAreaImpactada.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind14)));
            // 
            // logFailInicioAreaImpactada
            // 
            this.logFailInicioAreaImpactada.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logFailInicioAreaImpactada.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind15.Name = "FluxoFabrica";
            activitybind15.Path = "Workflow_HistoryDescription";
            activitybind16.Name = "FluxoFabrica";
            activitybind16.Path = "Workflow_HistoryOutcome";
            this.logFailInicioAreaImpactada.Name = "logFailInicioAreaImpactada";
            this.logFailInicioAreaImpactada.OtherData = "";
            this.logFailInicioAreaImpactada.UserId = -1;
            this.logFailInicioAreaImpactada.MethodInvoking += new System.EventHandler(this.logFailInicioAreaImpactada_MethodInvoking);
            this.logFailInicioAreaImpactada.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind15)));
            this.logFailInicioAreaImpactada.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind16)));
            // 
            // setStateActivity8
            // 
            this.setStateActivity8.Name = "setStateActivity8";
            this.setStateActivity8.TargetStateName = "stateDecisao";
            // 
            // setStateActivity7
            // 
            this.setStateActivity7.Name = "setStateActivity7";
            this.setStateActivity7.TargetStateName = "stateDecisao";
            // 
            // setStateActivity6
            // 
            this.setStateActivity6.Name = "setStateActivity6";
            this.setStateActivity6.TargetStateName = "stateDecisao";
            // 
            // setStateActivity1
            // 
            this.setStateActivity1.Name = "setStateActivity1";
            this.setStateActivity1.TargetStateName = "stateDecisao";
            // 
            // setStateComiteQA
            // 
            this.setStateComiteQA.Name = "setStateComiteQA";
            this.setStateComiteQA.TargetStateName = "stateDecisao";
            // 
            // setStateComiteRH
            // 
            this.setStateComiteRH.Name = "setStateComiteRH";
            this.setStateComiteRH.TargetStateName = "stateDecisao";
            // 
            // setStateComiteMA
            // 
            this.setStateComiteMA.Name = "setStateComiteMA";
            this.setStateComiteMA.TargetStateName = "stateDecisao";
            // 
            // logFaultInicioComite
            // 
            this.logFaultInicioComite.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logFaultInicioComite.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind17.Name = "FluxoFabrica";
            activitybind17.Path = "Workflow_HistoryDescription";
            activitybind18.Name = "FluxoFabrica";
            activitybind18.Path = "Workflow_HistoryOutcome";
            this.logFaultInicioComite.Name = "logFaultInicioComite";
            this.logFaultInicioComite.OtherData = "";
            this.logFaultInicioComite.UserId = -1;
            this.logFaultInicioComite.MethodInvoking += new System.EventHandler(this.logFaultInicioComite_MethodInvoking);
            this.logFaultInicioComite.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind17)));
            this.logFaultInicioComite.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind18)));
            // 
            // terminateActivityInitialState
            // 
            this.terminateActivityInitialState.Name = "terminateActivityInitialState";
            // 
            // logErroInitialState
            // 
            this.logErroInitialState.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logErroInitialState.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind19.Name = "FluxoFabrica";
            activitybind19.Path = "Workflow_HistoryDescription";
            activitybind20.Name = "FluxoFabrica";
            activitybind20.Path = "Workflow_HistoryOutcome";
            this.logErroInitialState.Name = "logErroInitialState";
            this.logErroInitialState.OtherData = "";
            this.logErroInitialState.UserId = -1;
            this.logErroInitialState.MethodInvoking += new System.EventHandler(this.logInicioWorkflow_MethodInvoking);
            this.logErroInitialState.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind19)));
            this.logErroInitialState.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind20)));
            // 
            // setIniciadoSalvo
            // 
            this.setIniciadoSalvo.Name = "setIniciadoSalvo";
            this.setIniciadoSalvo.TargetStateName = "stateColaborador";
            // 
            // setIniciadoEnviado
            // 
            this.setIniciadoEnviado.Name = "setIniciadoEnviado";
            this.setIniciadoEnviado.TargetStateName = "stateConfirmacaoPIA";
            // 
            // logFaultConfirmacaoPiaExecucao
            // 
            this.logFaultConfirmacaoPiaExecucao.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logFaultConfirmacaoPiaExecucao.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind21.Name = "FluxoFabrica";
            activitybind21.Path = "Workflow_HistoryDescription";
            activitybind22.Name = "FluxoFabrica";
            activitybind22.Path = "Workflow_HistoryOutcome";
            this.logFaultConfirmacaoPiaExecucao.Name = "logFaultConfirmacaoPiaExecucao";
            this.logFaultConfirmacaoPiaExecucao.OtherData = "";
            this.logFaultConfirmacaoPiaExecucao.UserId = -1;
            this.logFaultConfirmacaoPiaExecucao.MethodInvoking += new System.EventHandler(this.logFaultConfirmacaoPiaExecucao_MethodInvoking);
            this.logFaultConfirmacaoPiaExecucao.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind21)));
            this.logFaultConfirmacaoPiaExecucao.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind22)));
            // 
            // setMaisInformacaoPia
            // 
            this.setMaisInformacaoPia.Name = "setMaisInformacaoPia";
            this.setMaisInformacaoPia.TargetStateName = "stateColaborador";
            // 
            // enviarEmailMaisInformacoes
            // 
            this.enviarEmailMaisInformacoes.Grupo = "[Ideia]";
            this.enviarEmailMaisInformacoes.Name = "enviarEmailMaisInformacoes";
            activitybind23.Name = "FluxoFabrica";
            activitybind23.Path = "workflowProperties";
            this.enviarEmailMaisInformacoes.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.EnviarEmailGrupoActivity.WorkflowPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind23)));
            // 
            // atribuirStatusMaisInformacoesPia
            // 
            activitybind24.Name = "FluxoFabrica";
            activitybind24.Path = "Ideia";
            this.atribuirStatusMaisInformacoesPia.Name = "atribuirStatusMaisInformacoesPia";
            this.atribuirStatusMaisInformacoesPia.Status = Ajinomoto.FabricaIdeia.Business.Activities.StatusWorkflow.Salvo;
            this.atribuirStatusMaisInformacoesPia.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity.IdeiaProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind24)));
            // 
            // setReprovadoPia
            // 
            this.setReprovadoPia.Name = "setReprovadoPia";
            this.setReprovadoPia.TargetStateName = "stateConclusao";
            // 
            // enviarEmailIdeiaReprovada
            // 
            this.enviarEmailIdeiaReprovada.Grupo = "[Ideia]";
            this.enviarEmailIdeiaReprovada.Name = "enviarEmailIdeiaReprovada";
            activitybind25.Name = "FluxoFabrica";
            activitybind25.Path = "workflowProperties";
            this.enviarEmailIdeiaReprovada.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.EnviarEmailGrupoActivity.WorkflowPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind25)));
            // 
            // atribuirStatusIdeiaReprovadaPia
            // 
            activitybind26.Name = "FluxoFabrica";
            activitybind26.Path = "Ideia";
            this.atribuirStatusIdeiaReprovadaPia.Name = "atribuirStatusIdeiaReprovadaPia";
            this.atribuirStatusIdeiaReprovadaPia.Status = Ajinomoto.FabricaIdeia.Business.Activities.StatusWorkflow.Cancelado;
            this.atribuirStatusIdeiaReprovadaPia.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity.IdeiaProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind26)));
            // 
            // setAprovadoPia
            // 
            this.setAprovadoPia.Name = "setAprovadoPia";
            this.setAprovadoPia.TargetStateName = "stateAreaImpactada";
            // 
            // logConfirmacaoPiaInicioErro
            // 
            this.logConfirmacaoPiaInicioErro.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logConfirmacaoPiaInicioErro.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind27.Name = "FluxoFabrica";
            activitybind27.Path = "Workflow_HistoryDescription";
            activitybind28.Name = "FluxoFabrica";
            activitybind28.Path = "Workflow_HistoryOutcome";
            this.logConfirmacaoPiaInicioErro.Name = "logConfirmacaoPiaInicioErro";
            this.logConfirmacaoPiaInicioErro.OtherData = "";
            this.logConfirmacaoPiaInicioErro.UserId = -1;
            this.logConfirmacaoPiaInicioErro.MethodInvoking += new System.EventHandler(this.logConfirmacaoPiaInicioErro_MethodInvoking);
            this.logConfirmacaoPiaInicioErro.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind27)));
            this.logConfirmacaoPiaInicioErro.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind28)));
            // 
            // setStateFaultColaborador
            // 
            this.setStateFaultColaborador.Name = "setStateFaultColaborador";
            this.setStateFaultColaborador.TargetStateName = "stateColaborador";
            // 
            // logFaultColaborador
            // 
            this.logFaultColaborador.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logFaultColaborador.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind29.Name = "FluxoFabrica";
            activitybind29.Path = "Workflow_HistoryDescription";
            activitybind30.Name = "FluxoFabrica";
            activitybind30.Path = "Workflow_HistoryOutcome";
            this.logFaultColaborador.Name = "logFaultColaborador";
            this.logFaultColaborador.OtherData = "";
            this.logFaultColaborador.UserId = -1;
            this.logFaultColaborador.MethodInvoking += new System.EventHandler(this.logFaultColaborador_MethodInvoking);
            this.logFaultColaborador.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind29)));
            this.logFaultColaborador.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind30)));
            // 
            // setItemSalvoState
            // 
            this.setItemSalvoState.Name = "setItemSalvoState";
            this.setItemSalvoState.TargetStateName = "stateColaborador";
            // 
            // setItemEnviadoState
            // 
            this.setItemEnviadoState.Name = "setItemEnviadoState";
            this.setItemEnviadoState.TargetStateName = "stateConfirmacaoPIA";
            // 
            // faultHandlerExecucaoImplantacao
            // 
            this.faultHandlerExecucaoImplantacao.Activities.Add(this.logErroExecucaoImplantacao);
            this.faultHandlerExecucaoImplantacao.FaultType = typeof(System.Exception);
            this.faultHandlerExecucaoImplantacao.Name = "faultHandlerExecucaoImplantacao";
            // 
            // ifElseNaoImplantada
            // 
            this.ifElseNaoImplantada.Activities.Add(this.atribuirStatusInviavel);
            this.ifElseNaoImplantada.Name = "ifElseNaoImplantada";
            // 
            // ifElseIdeiaImplantada
            // 
            this.ifElseIdeiaImplantada.Activities.Add(this.atribuirStatusImplantada);
            this.ifElseIdeiaImplantada.Activities.Add(this.DobrarPontuacao);
            codecondition1.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IfIdeiaImplantada_Implantacao);
            this.ifElseIdeiaImplantada.Condition = codecondition1;
            this.ifElseIdeiaImplantada.Name = "ifElseIdeiaImplantada";
            // 
            // faultHandlerInicioImplantacao
            // 
            this.faultHandlerInicioImplantacao.Activities.Add(this.logerroInicioImplantacao);
            this.faultHandlerInicioImplantacao.FaultType = typeof(System.Exception);
            this.faultHandlerInicioImplantacao.Name = "faultHandlerInicioImplantacao";
            // 
            // faultHandlerExecucaoDecisao
            // 
            this.faultHandlerExecucaoDecisao.Activities.Add(this.logErroExecucaoDecisao);
            this.faultHandlerExecucaoDecisao.FaultType = typeof(System.Exception);
            this.faultHandlerExecucaoDecisao.Name = "faultHandlerExecucaoDecisao";
            // 
            // ifElseBranchActivity9
            // 
            this.ifElseBranchActivity9.Activities.Add(this.prepararSolicitacaoComite);
            this.ifElseBranchActivity9.Activities.Add(this.setStateActivity5);
            this.ifElseBranchActivity9.Name = "ifElseBranchActivity9";
            // 
            // ifElseDecisaoReprovar
            // 
            this.ifElseDecisaoReprovar.Activities.Add(this.atribuirStatusWorkflowActivity1);
            this.ifElseDecisaoReprovar.Activities.Add(this.enviarEmailGrupoActivity1);
            this.ifElseDecisaoReprovar.Activities.Add(this.setStateActivity4);
            codecondition2.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IfIdeiaReprovada_Decisao);
            this.ifElseDecisaoReprovar.Condition = codecondition2;
            this.ifElseDecisaoReprovar.Name = "ifElseDecisaoReprovar";
            // 
            // ifElseDecisaoAprovar
            // 
            this.ifElseDecisaoAprovar.Activities.Add(this.setStateActivity3);
            this.ifElseDecisaoAprovar.Activities.Add(this.AtribuirPontos);
            codecondition3.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IfIdeiaAprovada_Decisao);
            this.ifElseDecisaoAprovar.Condition = codecondition3;
            this.ifElseDecisaoAprovar.Name = "ifElseDecisaoAprovar";
            // 
            // faultHandlerInicioDecisao
            // 
            this.faultHandlerInicioDecisao.Activities.Add(this.logeErroInicioDecisao);
            this.faultHandlerInicioDecisao.FaultType = typeof(System.Exception);
            this.faultHandlerInicioDecisao.Name = "faultHandlerInicioDecisao";
            // 
            // faultExecucaoAreaImpactada
            // 
            this.faultExecucaoAreaImpactada.Activities.Add(this.logFaultExecucaoAreaImpactada);
            this.faultExecucaoAreaImpactada.FaultType = typeof(System.Exception);
            this.faultExecucaoAreaImpactada.Name = "faultExecucaoAreaImpactada";
            // 
            // faultInicioAreaImpactada
            // 
            this.faultInicioAreaImpactada.Activities.Add(this.logFailInicioAreaImpactada);
            this.faultInicioAreaImpactada.FaultType = typeof(System.Exception);
            this.faultInicioAreaImpactada.Name = "faultInicioAreaImpactada";
            // 
            // ifElseBranchActivity15
            // 
            this.ifElseBranchActivity15.Name = "ifElseBranchActivity15";
            // 
            // ifElseBranchActivity14
            // 
            this.ifElseBranchActivity14.Activities.Add(this.setStateActivity8);
            codecondition4.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IfComiteConcluido);
            this.ifElseBranchActivity14.Condition = codecondition4;
            this.ifElseBranchActivity14.Name = "ifElseBranchActivity14";
            // 
            // ifElseBranchActivity13
            // 
            this.ifElseBranchActivity13.Name = "ifElseBranchActivity13";
            // 
            // ifElseBranchActivity12
            // 
            this.ifElseBranchActivity12.Activities.Add(this.setStateActivity7);
            codecondition5.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IfComiteConcluido);
            this.ifElseBranchActivity12.Condition = codecondition5;
            this.ifElseBranchActivity12.Name = "ifElseBranchActivity12";
            // 
            // ifElseBranchActivity11
            // 
            this.ifElseBranchActivity11.Name = "ifElseBranchActivity11";
            // 
            // ifElseBranchActivity10
            // 
            this.ifElseBranchActivity10.Activities.Add(this.setStateActivity6);
            codecondition6.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IfComiteConcluido);
            this.ifElseBranchActivity10.Condition = codecondition6;
            this.ifElseBranchActivity10.Name = "ifElseBranchActivity10";
            // 
            // ifElseBranchActivity8
            // 
            this.ifElseBranchActivity8.Name = "ifElseBranchActivity8";
            // 
            // ifElseBranchActivity7
            // 
            this.ifElseBranchActivity7.Activities.Add(this.setStateActivity1);
            codecondition7.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IfComiteConcluido);
            this.ifElseBranchActivity7.Condition = codecondition7;
            this.ifElseBranchActivity7.Name = "ifElseBranchActivity7";
            // 
            // ifElseBranchActivity6
            // 
            this.ifElseBranchActivity6.Name = "ifElseBranchActivity6";
            // 
            // ifElseBranchActivity5
            // 
            this.ifElseBranchActivity5.Activities.Add(this.setStateComiteQA);
            codecondition8.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IfComiteConcluido);
            this.ifElseBranchActivity5.Condition = codecondition8;
            this.ifElseBranchActivity5.Name = "ifElseBranchActivity5";
            // 
            // ifElseBranchActivity4
            // 
            this.ifElseBranchActivity4.Name = "ifElseBranchActivity4";
            // 
            // ifElseBranchActivity3
            // 
            this.ifElseBranchActivity3.Activities.Add(this.setStateComiteRH);
            codecondition9.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IfComiteConcluido);
            this.ifElseBranchActivity3.Condition = codecondition9;
            this.ifElseBranchActivity3.Name = "ifElseBranchActivity3";
            // 
            // ifElseBranchActivity2
            // 
            this.ifElseBranchActivity2.Name = "ifElseBranchActivity2";
            // 
            // ifElseBranchActivity1
            // 
            this.ifElseBranchActivity1.Activities.Add(this.setStateComiteMA);
            codecondition10.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IfComiteConcluido);
            this.ifElseBranchActivity1.Condition = codecondition10;
            this.ifElseBranchActivity1.Name = "ifElseBranchActivity1";
            // 
            // faultInicioComite
            // 
            this.faultInicioComite.Activities.Add(this.logFaultInicioComite);
            this.faultInicioComite.FaultType = typeof(System.Exception);
            this.faultInicioComite.Name = "faultInicioComite";
            // 
            // faultHandleInitialState
            // 
            this.faultHandleInitialState.Activities.Add(this.logErroInitialState);
            this.faultHandleInitialState.Activities.Add(this.terminateActivityInitialState);
            this.faultHandleInitialState.FaultType = typeof(System.Exception);
            this.faultHandleInitialState.Name = "faultHandleInitialState";
            // 
            // ifElseIniciadoSalvo
            // 
            this.ifElseIniciadoSalvo.Activities.Add(this.setIniciadoSalvo);
            this.ifElseIniciadoSalvo.Name = "ifElseIniciadoSalvo";
            // 
            // ifElseIniciadoEnviado
            // 
            this.ifElseIniciadoEnviado.Activities.Add(this.setIniciadoEnviado);
            ruleconditionreference1.ConditionName = "ItemEviadoPorColaborador";
            this.ifElseIniciadoEnviado.Condition = ruleconditionreference1;
            this.ifElseIniciadoEnviado.Name = "ifElseIniciadoEnviado";
            // 
            // faultConfirmacaoPiaExecucao
            // 
            this.faultConfirmacaoPiaExecucao.Activities.Add(this.logFaultConfirmacaoPiaExecucao);
            this.faultConfirmacaoPiaExecucao.FaultType = typeof(System.Exception);
            this.faultConfirmacaoPiaExecucao.Name = "faultConfirmacaoPiaExecucao";
            // 
            // ifElseMaisInformacao
            // 
            this.ifElseMaisInformacao.Activities.Add(this.atribuirStatusMaisInformacoesPia);
            this.ifElseMaisInformacao.Activities.Add(this.enviarEmailMaisInformacoes);
            this.ifElseMaisInformacao.Activities.Add(this.setMaisInformacaoPia);
            this.ifElseMaisInformacao.Name = "ifElseMaisInformacao";
            // 
            // ifIdeiaReprovada
            // 
            this.ifIdeiaReprovada.Activities.Add(this.atribuirStatusIdeiaReprovadaPia);
            this.ifIdeiaReprovada.Activities.Add(this.enviarEmailIdeiaReprovada);
            this.ifIdeiaReprovada.Activities.Add(this.setReprovadoPia);
            codecondition11.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IfIdeiaReprovada_ConfirmacaoPia);
            this.ifIdeiaReprovada.Condition = codecondition11;
            this.ifIdeiaReprovada.Name = "ifIdeiaReprovada";
            // 
            // ifIdeiaAprovada
            // 
            this.ifIdeiaAprovada.Activities.Add(this.setAprovadoPia);
            codecondition12.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IfIdeiaAprovada_ConfirmacaoPia);
            this.ifIdeiaAprovada.Condition = codecondition12;
            this.ifIdeiaAprovada.Name = "ifIdeiaAprovada";
            // 
            // faultConfirmacaoPiaInicio
            // 
            this.faultConfirmacaoPiaInicio.Activities.Add(this.logConfirmacaoPiaInicioErro);
            this.faultConfirmacaoPiaInicio.FaultType = typeof(System.Exception);
            this.faultConfirmacaoPiaInicio.Name = "faultConfirmacaoPiaInicio";
            // 
            // faultActivityColaborador
            // 
            this.faultActivityColaborador.Activities.Add(this.logFaultColaborador);
            this.faultActivityColaborador.Activities.Add(this.setStateFaultColaborador);
            this.faultActivityColaborador.FaultType = typeof(System.Exception);
            this.faultActivityColaborador.Name = "faultActivityColaborador";
            // 
            // elseItemSalvo
            // 
            this.elseItemSalvo.Activities.Add(this.setItemSalvoState);
            this.elseItemSalvo.Name = "elseItemSalvo";
            // 
            // ifItemEnviado
            // 
            this.ifItemEnviado.Activities.Add(this.setItemEnviadoState);
            codecondition13.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.ColaboradorEnviouIdeia);
            this.ifItemEnviado.Condition = codecondition13;
            this.ifItemEnviado.Name = "ifItemEnviado";
            // 
            // faultHandlersActivity8
            // 
            this.faultHandlersActivity8.Activities.Add(this.faultHandlerExecucaoImplantacao);
            this.faultHandlersActivity8.Name = "faultHandlersActivity8";
            // 
            // setStateConclusao
            // 
            this.setStateConclusao.Name = "setStateConclusao";
            this.setStateConclusao.TargetStateName = "stateConclusao";
            // 
            // completeImplantacaoTask
            // 
            correlationtoken1.Name = "implantacaoToken";
            correlationtoken1.OwnerActivityName = "stateImplantacao";
            this.completeImplantacaoTask.CorrelationToken = correlationtoken1;
            this.completeImplantacaoTask.Name = "completeImplantacaoTask";
            activitybind31.Name = "FluxoFabrica";
            activitybind31.Path = "Implantacao_TaskId";
            this.completeImplantacaoTask.TaskOutcome = null;
            this.completeImplantacaoTask.SetBinding(Microsoft.SharePoint.WorkflowActions.CompleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind31)));
            // 
            // ifElseActivity9
            // 
            this.ifElseActivity9.Activities.Add(this.ifElseIdeiaImplantada);
            this.ifElseActivity9.Activities.Add(this.ifElseNaoImplantada);
            this.ifElseActivity9.Name = "ifElseActivity9";
            // 
            // atribuirCamposImplantacao
            // 
            this.atribuirCamposImplantacao.Name = "atribuirCamposImplantacao";
            this.atribuirCamposImplantacao.ExecuteCode += new System.EventHandler(this.atribuirCamposImplantacao_ExecuteCode);
            // 
            // logImplantacao
            // 
            this.logImplantacao.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logImplantacao.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logImplantacao.HistoryDescription = "Definição de implantação";
            activitybind32.Name = "FluxoFabrica";
            activitybind32.Path = "Implantacao_HistoryOutcome";
            this.logImplantacao.Name = "logImplantacao";
            this.logImplantacao.OtherData = "";
            this.logImplantacao.UserId = -1;
            this.logImplantacao.MethodInvoking += new System.EventHandler(this.logImplantacao_MethodInvoking);
            this.logImplantacao.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind32)));
            // 
            // onTaskImplantacaoChanged
            // 
            activitybind33.Name = "FluxoFabrica";
            activitybind33.Path = "Implantacao_AfterProperties";
            this.onTaskImplantacaoChanged.BeforeProperties = null;
            this.onTaskImplantacaoChanged.CorrelationToken = correlationtoken1;
            this.onTaskImplantacaoChanged.Executor = null;
            this.onTaskImplantacaoChanged.Name = "onTaskImplantacaoChanged";
            activitybind34.Name = "FluxoFabrica";
            activitybind34.Path = "Implantacao_TaskId";
            this.onTaskImplantacaoChanged.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.onTaskImplantacaoChanged_Invoked);
            this.onTaskImplantacaoChanged.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind34)));
            this.onTaskImplantacaoChanged.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.AfterPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind33)));
            // 
            // faultHandlersActivity7
            // 
            this.faultHandlersActivity7.Activities.Add(this.faultHandlerInicioImplantacao);
            this.faultHandlersActivity7.Name = "faultHandlersActivity7";
            // 
            // atribuirStatusImplantacao
            // 
            activitybind35.Name = "FluxoFabrica";
            activitybind35.Path = "Ideia";
            this.atribuirStatusImplantacao.Name = "atribuirStatusImplantacao";
            this.atribuirStatusImplantacao.Status = Ajinomoto.FabricaIdeia.Business.Activities.StatusWorkflow.Implantacao;
            this.atribuirStatusImplantacao.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity.IdeiaProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind35)));
            // 
            // logInicioImplantacao
            // 
            this.logInicioImplantacao.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logInicioImplantacao.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logInicioImplantacao.HistoryDescription = "Aguardando avaliação de implantacao";
            this.logInicioImplantacao.HistoryOutcome = "Implantacao";
            this.logInicioImplantacao.Name = "logInicioImplantacao";
            this.logInicioImplantacao.OtherData = "";
            this.logInicioImplantacao.UserId = -1;
            // 
            // createTaskImplantacao
            // 
            this.createTaskImplantacao.CorrelationToken = correlationtoken1;
            this.createTaskImplantacao.ListItemId = -1;
            this.createTaskImplantacao.Name = "createTaskImplantacao";
            this.createTaskImplantacao.SpecialPermissions = null;
            activitybind36.Name = "FluxoFabrica";
            activitybind36.Path = "Implantacao_TaskId";
            activitybind37.Name = "FluxoFabrica";
            activitybind37.Path = "Implantacao_TaskProperties";
            this.createTaskImplantacao.MethodInvoking += new System.EventHandler(this.createTaskImplantacao_MethodInvoking);
            this.createTaskImplantacao.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind36)));
            this.createTaskImplantacao.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind37)));
            // 
            // faultHandlersActivity6
            // 
            this.faultHandlersActivity6.Activities.Add(this.faultHandlerExecucaoDecisao);
            this.faultHandlersActivity6.Name = "faultHandlersActivity6";
            // 
            // ifElseActivity4
            // 
            this.ifElseActivity4.Activities.Add(this.ifElseDecisaoAprovar);
            this.ifElseActivity4.Activities.Add(this.ifElseDecisaoReprovar);
            this.ifElseActivity4.Activities.Add(this.ifElseBranchActivity9);
            this.ifElseActivity4.Name = "ifElseActivity4";
            // 
            // completeDecisaoTask
            // 
            correlationtoken2.Name = "decisaoToken";
            correlationtoken2.OwnerActivityName = "stateDecisao";
            this.completeDecisaoTask.CorrelationToken = correlationtoken2;
            this.completeDecisaoTask.Name = "completeDecisaoTask";
            activitybind38.Name = "FluxoFabrica";
            activitybind38.Path = "Decisao_TaskId";
            this.completeDecisaoTask.TaskOutcome = null;
            this.completeDecisaoTask.SetBinding(Microsoft.SharePoint.WorkflowActions.CompleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind38)));
            // 
            // CopiarDadosDecisaoIdeia
            // 
            this.CopiarDadosDecisaoIdeia.Name = "CopiarDadosDecisaoIdeia";
            this.CopiarDadosDecisaoIdeia.ExecuteCode += new System.EventHandler(this.CopiarDadosDecisaoIdeia_ExecuteCode);
            // 
            // onDecisaoTaskChanged
            // 
            activitybind39.Name = "FluxoFabrica";
            activitybind39.Path = "Decisao_AfterProperties";
            this.onDecisaoTaskChanged.BeforeProperties = null;
            this.onDecisaoTaskChanged.CorrelationToken = correlationtoken2;
            this.onDecisaoTaskChanged.Executor = null;
            this.onDecisaoTaskChanged.Name = "onDecisaoTaskChanged";
            activitybind40.Name = "FluxoFabrica";
            activitybind40.Path = "Decisao_TaskId";
            this.onDecisaoTaskChanged.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.onDecisaoTaskChanged_Invoked);
            this.onDecisaoTaskChanged.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind40)));
            this.onDecisaoTaskChanged.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.AfterPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind39)));
            // 
            // faultHandlersActivity5
            // 
            this.faultHandlersActivity5.Activities.Add(this.faultHandlerInicioDecisao);
            this.faultHandlersActivity5.Name = "faultHandlersActivity5";
            // 
            // atribuirStatusDecisao
            // 
            activitybind41.Name = "FluxoFabrica";
            activitybind41.Path = "Ideia";
            this.atribuirStatusDecisao.Name = "atribuirStatusDecisao";
            this.atribuirStatusDecisao.Status = Ajinomoto.FabricaIdeia.Business.Activities.StatusWorkflow.Decisao_Comite;
            this.atribuirStatusDecisao.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity.IdeiaProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind41)));
            // 
            // createTaskDecisao
            // 
            this.createTaskDecisao.CorrelationToken = correlationtoken2;
            this.createTaskDecisao.ListItemId = -1;
            this.createTaskDecisao.Name = "createTaskDecisao";
            this.createTaskDecisao.SpecialPermissions = null;
            activitybind42.Name = "FluxoFabrica";
            activitybind42.Path = "Decisao_TaskId";
            activitybind43.Name = "FluxoFabrica";
            activitybind43.Path = "Decisao_TaskProperties";
            this.createTaskDecisao.MethodInvoking += new System.EventHandler(this.createTaskDecisao_MethodInvoking);
            this.createTaskDecisao.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind42)));
            this.createTaskDecisao.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind43)));
            // 
            // calcularResultadoComite
            // 
            this.calcularResultadoComite.Name = "calcularResultadoComite";
            this.calcularResultadoComite.ExecuteCode += new System.EventHandler(this.calcularResultadoComite_ExecuteCode);
            // 
            // logIncioDecisao
            // 
            this.logIncioDecisao.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logIncioDecisao.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logIncioDecisao.HistoryDescription = "Comite concluído. Aguardando PIA";
            this.logIncioDecisao.HistoryOutcome = "Decisão Comitê";
            this.logIncioDecisao.Name = "logIncioDecisao";
            this.logIncioDecisao.OtherData = "";
            this.logIncioDecisao.UserId = -1;
            // 
            // faultHandlersExecucaoAreaImpactada
            // 
            this.faultHandlersExecucaoAreaImpactada.Activities.Add(this.faultExecucaoAreaImpactada);
            this.faultHandlersExecucaoAreaImpactada.Name = "faultHandlersExecucaoAreaImpactada";
            // 
            // setStateComite
            // 
            this.setStateComite.Name = "setStateComite";
            this.setStateComite.TargetStateName = "stateComiteAvaliador";
            // 
            // completeTaskAreaImpcatada
            // 
            correlationtoken3.Name = "areaImpactadaToken";
            correlationtoken3.OwnerActivityName = "stateAreaImpactada";
            this.completeTaskAreaImpcatada.CorrelationToken = correlationtoken3;
            this.completeTaskAreaImpcatada.Name = "completeTaskAreaImpcatada";
            activitybind44.Name = "FluxoFabrica";
            activitybind44.Path = "AreaImpactada_TaskId";
            this.completeTaskAreaImpcatada.TaskOutcome = null;
            this.completeTaskAreaImpcatada.SetBinding(Microsoft.SharePoint.WorkflowActions.CompleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind44)));
            // 
            // copiarDadosAreaImpactada
            // 
            this.copiarDadosAreaImpactada.Name = "copiarDadosAreaImpactada";
            this.copiarDadosAreaImpactada.ExecuteCode += new System.EventHandler(this.copiarDadosAreaImpactada_ExecuteCode);
            // 
            // onTaskAreaImpactadaChanged
            // 
            activitybind45.Name = "FluxoFabrica";
            activitybind45.Path = "AreaImpactada_AfterProperties";
            activitybind46.Name = "FluxoFabrica";
            activitybind46.Path = "AreaImpactada_BeforeProperties";
            this.onTaskAreaImpactadaChanged.CorrelationToken = correlationtoken3;
            this.onTaskAreaImpactadaChanged.Executor = null;
            this.onTaskAreaImpactadaChanged.Name = "onTaskAreaImpactadaChanged";
            activitybind47.Name = "FluxoFabrica";
            activitybind47.Path = "AreaImpactada_TaskId";
            this.onTaskAreaImpactadaChanged.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.onTaskAreaImpactadaChanged_Invoked);
            this.onTaskAreaImpactadaChanged.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind47)));
            this.onTaskAreaImpactadaChanged.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.AfterPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind45)));
            this.onTaskAreaImpactadaChanged.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.BeforePropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind46)));
            // 
            // faultHandlersActivity3
            // 
            this.faultHandlersActivity3.Activities.Add(this.faultInicioAreaImpactada);
            this.faultHandlersActivity3.Name = "faultHandlersActivity3";
            // 
            // enviarEmailAreaImpactada
            // 
            activitybind48.Name = "FluxoFabrica";
            activitybind48.Path = "AreaImpactada_Grupo";
            this.enviarEmailAreaImpactada.Name = "enviarEmailAreaImpactada";
            activitybind49.Name = "FluxoFabrica";
            activitybind49.Path = "workflowProperties";
            this.enviarEmailAreaImpactada.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.EnviarEmailGrupoActivity.GrupoProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind48)));
            this.enviarEmailAreaImpactada.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.EnviarEmailGrupoActivity.WorkflowPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind49)));
            // 
            // atribuirStatusAreaImpactada
            // 
            activitybind50.Name = "FluxoFabrica";
            activitybind50.Path = "Ideia";
            this.atribuirStatusAreaImpactada.Name = "atribuirStatusAreaImpactada";
            this.atribuirStatusAreaImpactada.Status = Ajinomoto.FabricaIdeia.Business.Activities.StatusWorkflow.Analise_AreaImpactada;
            this.atribuirStatusAreaImpactada.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity.IdeiaProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind50)));
            // 
            // logInicioAreaImpactada
            // 
            this.logInicioAreaImpactada.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logInicioAreaImpactada.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logInicioAreaImpactada.HistoryDescription = "Tarefa criada para área impcatada";
            activitybind51.Name = "FluxoFabrica";
            activitybind51.Path = "AreaImpactada_Grupo";
            this.logInicioAreaImpactada.Name = "logInicioAreaImpactada";
            this.logInicioAreaImpactada.OtherData = "";
            this.logInicioAreaImpactada.UserId = -1;
            this.logInicioAreaImpactada.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind51)));
            // 
            // createTaskAreaImpactada
            // 
            this.createTaskAreaImpactada.CorrelationToken = correlationtoken3;
            this.createTaskAreaImpactada.ListItemId = -1;
            this.createTaskAreaImpactada.Name = "createTaskAreaImpactada";
            this.createTaskAreaImpactada.SpecialPermissions = null;
            activitybind52.Name = "FluxoFabrica";
            activitybind52.Path = "AreaImpactada_TaskId";
            activitybind53.Name = "FluxoFabrica";
            activitybind53.Path = "AreaImpactada_TaskProperties";
            this.createTaskAreaImpactada.MethodInvoking += new System.EventHandler(this.createTaskAreaImpactada_MethodInvoking);
            this.createTaskAreaImpactada.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind52)));
            this.createTaskAreaImpactada.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind53)));
            // 
            // ifElseActivity8
            // 
            this.ifElseActivity8.Activities.Add(this.ifElseBranchActivity14);
            this.ifElseActivity8.Activities.Add(this.ifElseBranchActivity15);
            this.ifElseActivity8.Name = "ifElseActivity8";
            // 
            // logAreaOpcionalConcluido
            // 
            this.logAreaOpcionalConcluido.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logAreaOpcionalConcluido.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logAreaOpcionalConcluido.HistoryDescription = "Area Opcional Concluido";
            this.logAreaOpcionalConcluido.HistoryOutcome = "Comite";
            this.logAreaOpcionalConcluido.Name = "logAreaOpcionalConcluido";
            this.logAreaOpcionalConcluido.OtherData = "";
            this.logAreaOpcionalConcluido.UserId = -1;
            // 
            // completeTask7
            // 
            correlationtoken4.Name = "comiteAreaOpcionalToken";
            correlationtoken4.OwnerActivityName = "stateComiteAvaliador";
            this.completeTask7.CorrelationToken = correlationtoken4;
            this.completeTask7.Name = "completeTask7";
            activitybind54.Name = "FluxoFabrica";
            activitybind54.Path = "Comite_TaskId.Item[6]";
            this.completeTask7.TaskOutcome = null;
            this.completeTask7.SetBinding(Microsoft.SharePoint.WorkflowActions.CompleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind54)));
            // 
            // concluirAreaOpcional
            // 
            this.concluirAreaOpcional.Name = "concluirAreaOpcional";
            this.concluirAreaOpcional.ExecuteCode += new System.EventHandler(this.concluirTarefaComissao_ExecuteCode);
            // 
            // onTaskChanged7
            // 
            this.onTaskChanged7.AfterProperties = null;
            this.onTaskChanged7.BeforeProperties = null;
            this.onTaskChanged7.CorrelationToken = correlationtoken4;
            this.onTaskChanged7.Executor = null;
            this.onTaskChanged7.Name = "onTaskChanged7";
            activitybind55.Name = "FluxoFabrica";
            activitybind55.Path = "Comite_TaskId.Item[6]";
            this.onTaskChanged7.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind55)));
            // 
            // ifElseActivity7
            // 
            this.ifElseActivity7.Activities.Add(this.ifElseBranchActivity12);
            this.ifElseActivity7.Activities.Add(this.ifElseBranchActivity13);
            this.ifElseActivity7.Name = "ifElseActivity7";
            // 
            // logEngenhanriaConcluido
            // 
            this.logEngenhanriaConcluido.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logEngenhanriaConcluido.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logEngenhanriaConcluido.HistoryDescription = "Engenharia Concluido";
            this.logEngenhanriaConcluido.HistoryOutcome = "Comite";
            this.logEngenhanriaConcluido.Name = "logEngenhanriaConcluido";
            this.logEngenhanriaConcluido.OtherData = "";
            this.logEngenhanriaConcluido.UserId = -1;
            // 
            // completeTask6
            // 
            correlationtoken5.Name = "comiteEngenhariaToken";
            correlationtoken5.OwnerActivityName = "stateComiteAvaliador";
            this.completeTask6.CorrelationToken = correlationtoken5;
            this.completeTask6.Name = "completeTask6";
            activitybind56.Name = "FluxoFabrica";
            activitybind56.Path = "Comite_TaskId.Item[5]";
            this.completeTask6.TaskOutcome = null;
            this.completeTask6.SetBinding(Microsoft.SharePoint.WorkflowActions.CompleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind56)));
            // 
            // concluirEngenharia
            // 
            this.concluirEngenharia.Name = "concluirEngenharia";
            this.concluirEngenharia.ExecuteCode += new System.EventHandler(this.concluirTarefaComissao_ExecuteCode);
            // 
            // onTaskChanged6
            // 
            this.onTaskChanged6.AfterProperties = null;
            this.onTaskChanged6.BeforeProperties = null;
            this.onTaskChanged6.CorrelationToken = correlationtoken5;
            this.onTaskChanged6.Executor = null;
            this.onTaskChanged6.Name = "onTaskChanged6";
            activitybind57.Name = "FluxoFabrica";
            activitybind57.Path = "Comite_TaskId.Item[5]";
            this.onTaskChanged6.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind57)));
            // 
            // ifElseActivity6
            // 
            this.ifElseActivity6.Activities.Add(this.ifElseBranchActivity10);
            this.ifElseActivity6.Activities.Add(this.ifElseBranchActivity11);
            this.ifElseActivity6.Name = "ifElseActivity6";
            // 
            // logCPDConcluido
            // 
            this.logCPDConcluido.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logCPDConcluido.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logCPDConcluido.HistoryDescription = "CPD Concluido";
            this.logCPDConcluido.HistoryOutcome = "Comite";
            this.logCPDConcluido.Name = "logCPDConcluido";
            this.logCPDConcluido.OtherData = "";
            this.logCPDConcluido.UserId = -1;
            // 
            // completeTask5
            // 
            correlationtoken6.Name = "comiteCPDToken";
            correlationtoken6.OwnerActivityName = "stateComiteAvaliador";
            this.completeTask5.CorrelationToken = correlationtoken6;
            this.completeTask5.Name = "completeTask5";
            activitybind58.Name = "FluxoFabrica";
            activitybind58.Path = "Comite_TaskId.Item[4]";
            this.completeTask5.TaskOutcome = null;
            this.completeTask5.SetBinding(Microsoft.SharePoint.WorkflowActions.CompleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind58)));
            // 
            // concluirCPD
            // 
            this.concluirCPD.Name = "concluirCPD";
            this.concluirCPD.ExecuteCode += new System.EventHandler(this.concluirTarefaComissao_ExecuteCode);
            // 
            // onTaskChanged5
            // 
            this.onTaskChanged5.AfterProperties = null;
            this.onTaskChanged5.BeforeProperties = null;
            this.onTaskChanged5.CorrelationToken = correlationtoken6;
            this.onTaskChanged5.Executor = null;
            this.onTaskChanged5.Name = "onTaskChanged5";
            activitybind59.Name = "FluxoFabrica";
            activitybind59.Path = "Comite_TaskId.Item[4]";
            this.onTaskChanged5.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind59)));
            // 
            // ifElseActivity5
            // 
            this.ifElseActivity5.Activities.Add(this.ifElseBranchActivity7);
            this.ifElseActivity5.Activities.Add(this.ifElseBranchActivity8);
            this.ifElseActivity5.Name = "ifElseActivity5";
            // 
            // logSeguranca
            // 
            this.logSeguranca.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logSeguranca.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logSeguranca.HistoryDescription = "Seguranca Concluido";
            this.logSeguranca.HistoryOutcome = "Comite";
            this.logSeguranca.Name = "logSeguranca";
            this.logSeguranca.OtherData = "";
            this.logSeguranca.UserId = -1;
            // 
            // completeTask4
            // 
            correlationtoken7.Name = "comiteSegurancaToken";
            correlationtoken7.OwnerActivityName = "stateComiteAvaliador";
            this.completeTask4.CorrelationToken = correlationtoken7;
            this.completeTask4.Name = "completeTask4";
            activitybind60.Name = "FluxoFabrica";
            activitybind60.Path = "Comite_TaskId.Item[3]";
            this.completeTask4.TaskOutcome = null;
            this.completeTask4.SetBinding(Microsoft.SharePoint.WorkflowActions.CompleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind60)));
            // 
            // concluirSeguranca
            // 
            this.concluirSeguranca.Name = "concluirSeguranca";
            this.concluirSeguranca.ExecuteCode += new System.EventHandler(this.concluirTarefaComissao_ExecuteCode);
            // 
            // onTaskChanged4
            // 
            this.onTaskChanged4.AfterProperties = null;
            this.onTaskChanged4.BeforeProperties = null;
            this.onTaskChanged4.CorrelationToken = correlationtoken7;
            this.onTaskChanged4.Executor = null;
            this.onTaskChanged4.Name = "onTaskChanged4";
            activitybind61.Name = "FluxoFabrica";
            activitybind61.Path = "Comite_TaskId.Item[3]";
            this.onTaskChanged4.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind61)));
            // 
            // ifElseActivity3
            // 
            this.ifElseActivity3.Activities.Add(this.ifElseBranchActivity5);
            this.ifElseActivity3.Activities.Add(this.ifElseBranchActivity6);
            this.ifElseActivity3.Name = "ifElseActivity3";
            // 
            // logQAConcluido
            // 
            this.logQAConcluido.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logQAConcluido.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logQAConcluido.HistoryDescription = "QA Concluido";
            this.logQAConcluido.HistoryOutcome = "Comite";
            this.logQAConcluido.Name = "logQAConcluido";
            this.logQAConcluido.OtherData = "";
            this.logQAConcluido.UserId = -1;
            // 
            // completeTask3
            // 
            correlationtoken8.Name = "comiteQAToken";
            correlationtoken8.OwnerActivityName = "stateComiteAvaliador";
            this.completeTask3.CorrelationToken = correlationtoken8;
            this.completeTask3.Name = "completeTask3";
            activitybind62.Name = "FluxoFabrica";
            activitybind62.Path = "Comite_TaskId.Item[2]";
            this.completeTask3.TaskOutcome = null;
            this.completeTask3.SetBinding(Microsoft.SharePoint.WorkflowActions.CompleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind62)));
            // 
            // concluirQA
            // 
            this.concluirQA.Name = "concluirQA";
            this.concluirQA.ExecuteCode += new System.EventHandler(this.concluirTarefaComissao_ExecuteCode);
            // 
            // onTaskChanged3
            // 
            this.onTaskChanged3.AfterProperties = null;
            this.onTaskChanged3.BeforeProperties = null;
            this.onTaskChanged3.CorrelationToken = correlationtoken8;
            this.onTaskChanged3.Executor = null;
            this.onTaskChanged3.Name = "onTaskChanged3";
            activitybind63.Name = "FluxoFabrica";
            activitybind63.Path = "Comite_TaskId.Item[2]";
            this.onTaskChanged3.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind63)));
            // 
            // ifElseActivity2
            // 
            this.ifElseActivity2.Activities.Add(this.ifElseBranchActivity3);
            this.ifElseActivity2.Activities.Add(this.ifElseBranchActivity4);
            this.ifElseActivity2.Name = "ifElseActivity2";
            // 
            // logRHConcluido
            // 
            this.logRHConcluido.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logRHConcluido.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logRHConcluido.HistoryDescription = "RH Concluido";
            this.logRHConcluido.HistoryOutcome = "Comite";
            this.logRHConcluido.Name = "logRHConcluido";
            this.logRHConcluido.OtherData = "";
            this.logRHConcluido.UserId = -1;
            // 
            // completeTask2
            // 
            correlationtoken9.Name = "comiteRHToken";
            correlationtoken9.OwnerActivityName = "stateComiteAvaliador";
            this.completeTask2.CorrelationToken = correlationtoken9;
            this.completeTask2.Name = "completeTask2";
            activitybind64.Name = "FluxoFabrica";
            activitybind64.Path = "Comite_TaskId.Item[1]";
            this.completeTask2.TaskOutcome = null;
            this.completeTask2.SetBinding(Microsoft.SharePoint.WorkflowActions.CompleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind64)));
            // 
            // concluirRH
            // 
            this.concluirRH.Name = "concluirRH";
            this.concluirRH.ExecuteCode += new System.EventHandler(this.concluirTarefaComissao_ExecuteCode);
            // 
            // onTaskChanged2
            // 
            this.onTaskChanged2.AfterProperties = null;
            this.onTaskChanged2.BeforeProperties = null;
            this.onTaskChanged2.CorrelationToken = correlationtoken9;
            this.onTaskChanged2.Executor = null;
            this.onTaskChanged2.Name = "onTaskChanged2";
            activitybind65.Name = "FluxoFabrica";
            activitybind65.Path = "Comite_TaskId.Item[1]";
            this.onTaskChanged2.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind65)));
            // 
            // ifElseActivity1
            // 
            this.ifElseActivity1.Activities.Add(this.ifElseBranchActivity1);
            this.ifElseActivity1.Activities.Add(this.ifElseBranchActivity2);
            this.ifElseActivity1.Name = "ifElseActivity1";
            // 
            // logMAConcluido
            // 
            this.logMAConcluido.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logMAConcluido.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logMAConcluido.HistoryDescription = "MA Concluido";
            this.logMAConcluido.HistoryOutcome = "Comite";
            this.logMAConcluido.Name = "logMAConcluido";
            this.logMAConcluido.OtherData = "";
            this.logMAConcluido.UserId = -1;
            // 
            // completeTask1
            // 
            correlationtoken10.Name = "comiteMAToken";
            correlationtoken10.OwnerActivityName = "stateComiteAvaliador";
            this.completeTask1.CorrelationToken = correlationtoken10;
            this.completeTask1.Name = "completeTask1";
            activitybind66.Name = "FluxoFabrica";
            activitybind66.Path = "Comite_TaskId.Item[0]";
            this.completeTask1.TaskOutcome = null;
            this.completeTask1.SetBinding(Microsoft.SharePoint.WorkflowActions.CompleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind66)));
            // 
            // concluirMA
            // 
            this.concluirMA.Name = "concluirMA";
            this.concluirMA.ExecuteCode += new System.EventHandler(this.concluirTarefaComissao_ExecuteCode);
            // 
            // onTaskChanged1
            // 
            this.onTaskChanged1.AfterProperties = null;
            this.onTaskChanged1.BeforeProperties = null;
            correlationtoken11.Name = "comiteMAToken";
            correlationtoken11.OwnerActivityName = "stateComiteAvaliador";
            this.onTaskChanged1.CorrelationToken = correlationtoken11;
            this.onTaskChanged1.Executor = null;
            this.onTaskChanged1.Name = "onTaskChanged1";
            activitybind67.Name = "FluxoFabrica";
            activitybind67.Path = "Comite_TaskId.Item[0]";
            this.onTaskChanged1.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind67)));
            // 
            // faultHandlersActivity4
            // 
            this.faultHandlersActivity4.Activities.Add(this.faultInicioComite);
            this.faultHandlersActivity4.Name = "faultHandlersActivity4";
            // 
            // createTaskAreaOpcional
            // 
            this.createTaskAreaOpcional.CorrelationToken = correlationtoken4;
            this.createTaskAreaOpcional.ListItemId = -1;
            this.createTaskAreaOpcional.Name = "createTaskAreaOpcional";
            this.createTaskAreaOpcional.SpecialPermissions = null;
            activitybind68.Name = "FluxoFabrica";
            activitybind68.Path = "Comite_TaskId.Item[6]";
            activitybind69.Name = "FluxoFabrica";
            activitybind69.Path = "Comite_TaskProperties.Item[6]";
            this.createTaskAreaOpcional.MethodInvoking += new System.EventHandler(this.createComiteTask_MethodInvoking);
            this.createTaskAreaOpcional.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind69)));
            this.createTaskAreaOpcional.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind68)));
            // 
            // createTaskEngenharia
            // 
            this.createTaskEngenharia.CorrelationToken = correlationtoken5;
            this.createTaskEngenharia.ListItemId = -1;
            this.createTaskEngenharia.Name = "createTaskEngenharia";
            this.createTaskEngenharia.SpecialPermissions = null;
            activitybind70.Name = "FluxoFabrica";
            activitybind70.Path = "Comite_TaskId.Item[5]";
            activitybind71.Name = "FluxoFabrica";
            activitybind71.Path = "Comite_TaskProperties.Item[5]";
            this.createTaskEngenharia.MethodInvoking += new System.EventHandler(this.createComiteTask_MethodInvoking);
            this.createTaskEngenharia.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind71)));
            this.createTaskEngenharia.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind70)));
            // 
            // createTaskCPD
            // 
            this.createTaskCPD.CorrelationToken = correlationtoken6;
            this.createTaskCPD.ListItemId = -1;
            this.createTaskCPD.Name = "createTaskCPD";
            this.createTaskCPD.SpecialPermissions = null;
            activitybind72.Name = "FluxoFabrica";
            activitybind72.Path = "Comite_TaskId.Item[4]";
            activitybind73.Name = "FluxoFabrica";
            activitybind73.Path = "Comite_TaskProperties.Item[4]";
            this.createTaskCPD.MethodInvoking += new System.EventHandler(this.createComiteTask_MethodInvoking);
            this.createTaskCPD.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind73)));
            this.createTaskCPD.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind72)));
            // 
            // createTaskSeguranca
            // 
            this.createTaskSeguranca.CorrelationToken = correlationtoken7;
            this.createTaskSeguranca.ListItemId = -1;
            this.createTaskSeguranca.Name = "createTaskSeguranca";
            this.createTaskSeguranca.SpecialPermissions = null;
            activitybind74.Name = "FluxoFabrica";
            activitybind74.Path = "Comite_TaskId.Item[3]";
            activitybind75.Name = "FluxoFabrica";
            activitybind75.Path = "Comite_TaskProperties.Item[3]";
            this.createTaskSeguranca.MethodInvoking += new System.EventHandler(this.createComiteTask_MethodInvoking);
            this.createTaskSeguranca.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind75)));
            this.createTaskSeguranca.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind74)));
            // 
            // createTaskQA
            // 
            this.createTaskQA.CorrelationToken = correlationtoken8;
            this.createTaskQA.ListItemId = -1;
            this.createTaskQA.Name = "createTaskQA";
            this.createTaskQA.SpecialPermissions = null;
            activitybind76.Name = "FluxoFabrica";
            activitybind76.Path = "Comite_TaskId.Item[2]";
            activitybind77.Name = "FluxoFabrica";
            activitybind77.Path = "Comite_TaskProperties.Item[2]";
            this.createTaskQA.MethodInvoking += new System.EventHandler(this.createComiteTask_MethodInvoking);
            this.createTaskQA.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind76)));
            this.createTaskQA.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind77)));
            // 
            // createTaskRH
            // 
            this.createTaskRH.CorrelationToken = correlationtoken9;
            this.createTaskRH.ListItemId = -1;
            this.createTaskRH.Name = "createTaskRH";
            this.createTaskRH.SpecialPermissions = null;
            activitybind78.Name = "FluxoFabrica";
            activitybind78.Path = "Comite_TaskId.Item[1]";
            activitybind79.Name = "FluxoFabrica";
            activitybind79.Path = "Comite_TaskProperties.Item[1]";
            this.createTaskRH.MethodInvoking += new System.EventHandler(this.createComiteTask_MethodInvoking);
            this.createTaskRH.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind78)));
            this.createTaskRH.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind79)));
            // 
            // createTaskMA
            // 
            correlationtoken12.Name = "comiteMAToken";
            correlationtoken12.OwnerActivityName = "stateComiteAvaliador";
            this.createTaskMA.CorrelationToken = correlationtoken12;
            this.createTaskMA.ListItemId = -1;
            this.createTaskMA.Name = "createTaskMA";
            this.createTaskMA.SpecialPermissions = null;
            activitybind80.Name = "FluxoFabrica";
            activitybind80.Path = "Comite_TaskId.Item[0]";
            activitybind81.Name = "FluxoFabrica";
            activitybind81.Path = "Comite_TaskProperties.Item[0]";
            this.createTaskMA.MethodInvoking += new System.EventHandler(this.createComiteTask_MethodInvoking);
            this.createTaskMA.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind80)));
            this.createTaskMA.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind81)));
            // 
            // preparaCriacaoTasks
            // 
            this.preparaCriacaoTasks.Name = "preparaCriacaoTasks";
            this.preparaCriacaoTasks.ExecuteCode += new System.EventHandler(this.preparaCriacaoTasks_ExecuteCode);
            // 
            // atribuirStatusInicioComite
            // 
            activitybind82.Name = "FluxoFabrica";
            activitybind82.Path = "Ideia";
            this.atribuirStatusInicioComite.Name = "atribuirStatusInicioComite";
            this.atribuirStatusInicioComite.Status = Ajinomoto.FabricaIdeia.Business.Activities.StatusWorkflow.Analise_Comite;
            this.atribuirStatusInicioComite.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity.IdeiaProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind82)));
            // 
            // logInicioComite
            // 
            this.logInicioComite.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logInicioComite.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logInicioComite.HistoryDescription = "Enviado tarefas para o comitê";
            this.logInicioComite.HistoryOutcome = "Comitê";
            this.logInicioComite.Name = "logInicioComite";
            this.logInicioComite.OtherData = "";
            this.logInicioComite.UserId = -1;
            // 
            // faultInicialState
            // 
            this.faultInicialState.Activities.Add(this.faultHandleInitialState);
            this.faultInicialState.Name = "faultInicialState";
            // 
            // ifElseInicio
            // 
            this.ifElseInicio.Activities.Add(this.ifElseIniciadoEnviado);
            this.ifElseInicio.Activities.Add(this.ifElseIniciadoSalvo);
            this.ifElseInicio.Name = "ifElseInicio";
            // 
            // logInicioWorkflow
            // 
            this.logInicioWorkflow.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logInicioWorkflow.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logInicioWorkflow.HistoryDescription = "Workflow iniciado";
            this.logInicioWorkflow.HistoryOutcome = "Iniciado";
            this.logInicioWorkflow.Name = "logInicioWorkflow";
            this.logInicioWorkflow.OtherData = "";
            this.logInicioWorkflow.UserId = -1;
            // 
            // onWorkflowIniciado
            // 
            correlationtoken13.Name = "workflowToken";
            correlationtoken13.OwnerActivityName = "FluxoFabrica";
            this.onWorkflowIniciado.CorrelationToken = correlationtoken13;
            this.onWorkflowIniciado.EventName = "OnWorkflowActivated";
            this.onWorkflowIniciado.Name = "onWorkflowIniciado";
            activitybind83.Name = "FluxoFabrica";
            activitybind83.Path = "workflowProperties";
            this.onWorkflowIniciado.SetBinding(Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated.WorkflowPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind83)));
            // 
            // faultHandlersActivity2
            // 
            this.faultHandlersActivity2.Activities.Add(this.faultConfirmacaoPiaExecucao);
            this.faultHandlersActivity2.Name = "faultHandlersActivity2";
            // 
            // ifElseConfirmacaoPia
            // 
            this.ifElseConfirmacaoPia.Activities.Add(this.ifIdeiaAprovada);
            this.ifElseConfirmacaoPia.Activities.Add(this.ifIdeiaReprovada);
            this.ifElseConfirmacaoPia.Activities.Add(this.ifElseMaisInformacao);
            this.ifElseConfirmacaoPia.Name = "ifElseConfirmacaoPia";
            // 
            // completeTaskConfirmacaoPia
            // 
            correlationtoken14.Name = "confirmacaoPiaToken";
            correlationtoken14.OwnerActivityName = "stateConfirmacaoPIA";
            this.completeTaskConfirmacaoPia.CorrelationToken = correlationtoken14;
            this.completeTaskConfirmacaoPia.Name = "completeTaskConfirmacaoPia";
            activitybind84.Name = "FluxoFabrica";
            activitybind84.Path = "ConfirmacaoPia_TaskId";
            this.completeTaskConfirmacaoPia.TaskOutcome = null;
            this.completeTaskConfirmacaoPia.SetBinding(Microsoft.SharePoint.WorkflowActions.CompleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind84)));
            // 
            // atribuirCamposPia
            // 
            this.atribuirCamposPia.Name = "atribuirCamposPia";
            this.atribuirCamposPia.ExecuteCode += new System.EventHandler(this.atribuirCamposPia_ExecuteCode);
            // 
            // logConfirmacaoPiaExecucao
            // 
            this.logConfirmacaoPiaExecucao.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logConfirmacaoPiaExecucao.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logConfirmacaoPiaExecucao.HistoryDescription = "Tarefa alterada pelo PIA";
            activitybind85.Name = "FluxoFabrica";
            activitybind85.Path = "Workflow_HistoryOutcome";
            this.logConfirmacaoPiaExecucao.Name = "logConfirmacaoPiaExecucao";
            this.logConfirmacaoPiaExecucao.OtherData = "";
            this.logConfirmacaoPiaExecucao.UserId = -1;
            this.logConfirmacaoPiaExecucao.MethodInvoking += new System.EventHandler(this.logConfirmacaoPiaExecucao_MethodInvoking);
            this.logConfirmacaoPiaExecucao.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind85)));
            // 
            // onTaskConfirmacaoPiaChanged
            // 
            activitybind86.Name = "FluxoFabrica";
            activitybind86.Path = "ConfirmacaoPia_AfterProperties";
            activitybind87.Name = "FluxoFabrica";
            activitybind87.Path = "ConfirmacaoPia_BeforeProperties";
            this.onTaskConfirmacaoPiaChanged.CorrelationToken = correlationtoken14;
            this.onTaskConfirmacaoPiaChanged.Executor = null;
            this.onTaskConfirmacaoPiaChanged.Name = "onTaskConfirmacaoPiaChanged";
            activitybind88.Name = "FluxoFabrica";
            activitybind88.Path = "ConfirmacaoPia_TaskId";
            this.onTaskConfirmacaoPiaChanged.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.onTaskConfirmacaoPiaChanged_Invoked);
            this.onTaskConfirmacaoPiaChanged.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind88)));
            this.onTaskConfirmacaoPiaChanged.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.AfterPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind86)));
            this.onTaskConfirmacaoPiaChanged.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.BeforePropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind87)));
            // 
            // cancellationHandlerActivity1
            // 
            this.cancellationHandlerActivity1.Name = "cancellationHandlerActivity1";
            // 
            // faultHandlersActivity1
            // 
            this.faultHandlersActivity1.Activities.Add(this.faultConfirmacaoPiaInicio);
            this.faultHandlersActivity1.Name = "faultHandlersActivity1";
            // 
            // enviarEmailConfirmacaoPia
            // 
            this.enviarEmailConfirmacaoPia.Grupo = "PIA";
            this.enviarEmailConfirmacaoPia.Name = "enviarEmailConfirmacaoPia";
            activitybind89.Name = "FluxoFabrica";
            activitybind89.Path = "workflowProperties";
            this.enviarEmailConfirmacaoPia.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.EnviarEmailGrupoActivity.WorkflowPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind89)));
            // 
            // atribuirStatusConfirmacaoPia
            // 
            activitybind90.Name = "FluxoFabrica";
            activitybind90.Path = "Ideia";
            this.atribuirStatusConfirmacaoPia.Name = "atribuirStatusConfirmacaoPia";
            this.atribuirStatusConfirmacaoPia.Status = Ajinomoto.FabricaIdeia.Business.Activities.StatusWorkflow.Analise_Pia;
            this.atribuirStatusConfirmacaoPia.SetBinding(Ajinomoto.FabricaIdeia.Business.Activities.AtribuirStatusWorkflowActivity.IdeiaProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind90)));
            // 
            // logConfirmacaoPiaEmail
            // 
            this.logConfirmacaoPiaEmail.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logConfirmacaoPiaEmail.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logConfirmacaoPiaEmail.HistoryDescription = "Tarefa criada para o PIA";
            this.logConfirmacaoPiaEmail.HistoryOutcome = "Email para o grupo PIA";
            this.logConfirmacaoPiaEmail.Name = "logConfirmacaoPiaEmail";
            this.logConfirmacaoPiaEmail.OtherData = "";
            this.logConfirmacaoPiaEmail.UserId = -1;
            // 
            // createConfirmacaoPiaTask
            // 
            this.createConfirmacaoPiaTask.CorrelationToken = correlationtoken14;
            this.createConfirmacaoPiaTask.ListItemId = -1;
            this.createConfirmacaoPiaTask.Name = "createConfirmacaoPiaTask";
            this.createConfirmacaoPiaTask.SpecialPermissions = null;
            activitybind91.Name = "FluxoFabrica";
            activitybind91.Path = "ConfirmacaoPia_TaskId";
            activitybind92.Name = "FluxoFabrica";
            activitybind92.Path = "ConfirmacaoPia_TaskProperties";
            this.createConfirmacaoPiaTask.MethodInvoking += new System.EventHandler(this.createConfirmacaoPiaTask_MethodInvoking);
            this.createConfirmacaoPiaTask.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind91)));
            this.createConfirmacaoPiaTask.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind92)));
            // 
            // faultColaborador
            // 
            this.faultColaborador.Activities.Add(this.faultActivityColaborador);
            this.faultColaborador.Name = "faultColaborador";
            // 
            // ifElseColaborador
            // 
            this.ifElseColaborador.Activities.Add(this.ifItemEnviado);
            this.ifElseColaborador.Activities.Add(this.elseItemSalvo);
            this.ifElseColaborador.Name = "ifElseColaborador";
            // 
            // logIdeiaStatus
            // 
            this.logIdeiaStatus.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logIdeiaStatus.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logIdeiaStatus.HistoryDescription = "Ideia alterada. Status atual registrado.";
            activitybind93.Name = "FluxoFabrica";
            activitybind93.Path = "StatusWorkflow";
            this.logIdeiaStatus.Name = "logIdeiaStatus";
            this.logIdeiaStatus.OtherData = "";
            this.logIdeiaStatus.UserId = -1;
            this.logIdeiaStatus.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind93)));
            // 
            // onIdeiaChanged
            // 
            this.onIdeiaChanged.AfterProperties = null;
            this.onIdeiaChanged.BeforeProperties = null;
            correlationtoken15.Name = "workflowToken";
            correlationtoken15.OwnerActivityName = "FluxoFabrica";
            this.onIdeiaChanged.CorrelationToken = correlationtoken15;
            this.onIdeiaChanged.Name = "onIdeiaChanged";
            // 
            // eventDrivenImplantacao
            // 
            this.eventDrivenImplantacao.Activities.Add(this.onTaskImplantacaoChanged);
            this.eventDrivenImplantacao.Activities.Add(this.logImplantacao);
            this.eventDrivenImplantacao.Activities.Add(this.atribuirCamposImplantacao);
            this.eventDrivenImplantacao.Activities.Add(this.ifElseActivity9);
            this.eventDrivenImplantacao.Activities.Add(this.completeImplantacaoTask);
            this.eventDrivenImplantacao.Activities.Add(this.setStateConclusao);
            this.eventDrivenImplantacao.Activities.Add(this.faultHandlersActivity8);
            this.eventDrivenImplantacao.Name = "eventDrivenImplantacao";
            // 
            // stateInitializationImplantacao
            // 
            this.stateInitializationImplantacao.Activities.Add(this.createTaskImplantacao);
            this.stateInitializationImplantacao.Activities.Add(this.logInicioImplantacao);
            this.stateInitializationImplantacao.Activities.Add(this.atribuirStatusImplantacao);
            this.stateInitializationImplantacao.Activities.Add(this.faultHandlersActivity7);
            this.stateInitializationImplantacao.Name = "stateInitializationImplantacao";
            // 
            // eventDrivenDecisao
            // 
            this.eventDrivenDecisao.Activities.Add(this.onDecisaoTaskChanged);
            this.eventDrivenDecisao.Activities.Add(this.CopiarDadosDecisaoIdeia);
            this.eventDrivenDecisao.Activities.Add(this.completeDecisaoTask);
            this.eventDrivenDecisao.Activities.Add(this.ifElseActivity4);
            this.eventDrivenDecisao.Activities.Add(this.faultHandlersActivity6);
            this.eventDrivenDecisao.Name = "eventDrivenDecisao";
            // 
            // stateInitializationDecisao
            // 
            this.stateInitializationDecisao.Activities.Add(this.logIncioDecisao);
            this.stateInitializationDecisao.Activities.Add(this.calcularResultadoComite);
            this.stateInitializationDecisao.Activities.Add(this.createTaskDecisao);
            this.stateInitializationDecisao.Activities.Add(this.atribuirStatusDecisao);
            this.stateInitializationDecisao.Activities.Add(this.faultHandlersActivity5);
            this.stateInitializationDecisao.Name = "stateInitializationDecisao";
            // 
            // eventDrivenAreaImpactada
            // 
            this.eventDrivenAreaImpactada.Activities.Add(this.onTaskAreaImpactadaChanged);
            this.eventDrivenAreaImpactada.Activities.Add(this.copiarDadosAreaImpactada);
            this.eventDrivenAreaImpactada.Activities.Add(this.completeTaskAreaImpcatada);
            this.eventDrivenAreaImpactada.Activities.Add(this.setStateComite);
            this.eventDrivenAreaImpactada.Activities.Add(this.faultHandlersExecucaoAreaImpactada);
            this.eventDrivenAreaImpactada.Name = "eventDrivenAreaImpactada";
            // 
            // initializationAreaImpactada
            // 
            this.initializationAreaImpactada.Activities.Add(this.createTaskAreaImpactada);
            this.initializationAreaImpactada.Activities.Add(this.logInicioAreaImpactada);
            this.initializationAreaImpactada.Activities.Add(this.atribuirStatusAreaImpactada);
            this.initializationAreaImpactada.Activities.Add(this.enviarEmailAreaImpactada);
            this.initializationAreaImpactada.Activities.Add(this.faultHandlersActivity3);
            this.initializationAreaImpactada.Name = "initializationAreaImpactada";
            // 
            // eventDrivenAreaOpcional
            // 
            this.eventDrivenAreaOpcional.Activities.Add(this.onTaskChanged7);
            this.eventDrivenAreaOpcional.Activities.Add(this.concluirAreaOpcional);
            this.eventDrivenAreaOpcional.Activities.Add(this.completeTask7);
            this.eventDrivenAreaOpcional.Activities.Add(this.logAreaOpcionalConcluido);
            this.eventDrivenAreaOpcional.Activities.Add(this.ifElseActivity8);
            this.eventDrivenAreaOpcional.Name = "eventDrivenAreaOpcional";
            // 
            // eventDrivenEngenharia
            // 
            this.eventDrivenEngenharia.Activities.Add(this.onTaskChanged6);
            this.eventDrivenEngenharia.Activities.Add(this.concluirEngenharia);
            this.eventDrivenEngenharia.Activities.Add(this.completeTask6);
            this.eventDrivenEngenharia.Activities.Add(this.logEngenhanriaConcluido);
            this.eventDrivenEngenharia.Activities.Add(this.ifElseActivity7);
            this.eventDrivenEngenharia.Name = "eventDrivenEngenharia";
            // 
            // eventDrivenCPD
            // 
            this.eventDrivenCPD.Activities.Add(this.onTaskChanged5);
            this.eventDrivenCPD.Activities.Add(this.concluirCPD);
            this.eventDrivenCPD.Activities.Add(this.completeTask5);
            this.eventDrivenCPD.Activities.Add(this.logCPDConcluido);
            this.eventDrivenCPD.Activities.Add(this.ifElseActivity6);
            this.eventDrivenCPD.Name = "eventDrivenCPD";
            // 
            // eventDrivenSeguranca
            // 
            this.eventDrivenSeguranca.Activities.Add(this.onTaskChanged4);
            this.eventDrivenSeguranca.Activities.Add(this.concluirSeguranca);
            this.eventDrivenSeguranca.Activities.Add(this.completeTask4);
            this.eventDrivenSeguranca.Activities.Add(this.logSeguranca);
            this.eventDrivenSeguranca.Activities.Add(this.ifElseActivity5);
            this.eventDrivenSeguranca.Name = "eventDrivenSeguranca";
            // 
            // eventDrivenQA
            // 
            this.eventDrivenQA.Activities.Add(this.onTaskChanged3);
            this.eventDrivenQA.Activities.Add(this.concluirQA);
            this.eventDrivenQA.Activities.Add(this.completeTask3);
            this.eventDrivenQA.Activities.Add(this.logQAConcluido);
            this.eventDrivenQA.Activities.Add(this.ifElseActivity3);
            this.eventDrivenQA.Name = "eventDrivenQA";
            // 
            // eventDrivenRH
            // 
            this.eventDrivenRH.Activities.Add(this.onTaskChanged2);
            this.eventDrivenRH.Activities.Add(this.concluirRH);
            this.eventDrivenRH.Activities.Add(this.completeTask2);
            this.eventDrivenRH.Activities.Add(this.logRHConcluido);
            this.eventDrivenRH.Activities.Add(this.ifElseActivity2);
            this.eventDrivenRH.Name = "eventDrivenRH";
            // 
            // eventDrivenMA
            // 
            this.eventDrivenMA.Activities.Add(this.onTaskChanged1);
            this.eventDrivenMA.Activities.Add(this.concluirMA);
            this.eventDrivenMA.Activities.Add(this.completeTask1);
            this.eventDrivenMA.Activities.Add(this.logMAConcluido);
            this.eventDrivenMA.Activities.Add(this.ifElseActivity1);
            this.eventDrivenMA.Name = "eventDrivenMA";
            // 
            // initializationComiteAvaliador
            // 
            this.initializationComiteAvaliador.Activities.Add(this.logInicioComite);
            this.initializationComiteAvaliador.Activities.Add(this.atribuirStatusInicioComite);
            this.initializationComiteAvaliador.Activities.Add(this.preparaCriacaoTasks);
            this.initializationComiteAvaliador.Activities.Add(this.createTaskMA);
            this.initializationComiteAvaliador.Activities.Add(this.createTaskRH);
            this.initializationComiteAvaliador.Activities.Add(this.createTaskQA);
            this.initializationComiteAvaliador.Activities.Add(this.createTaskSeguranca);
            this.initializationComiteAvaliador.Activities.Add(this.createTaskCPD);
            this.initializationComiteAvaliador.Activities.Add(this.createTaskEngenharia);
            this.initializationComiteAvaliador.Activities.Add(this.createTaskAreaOpcional);
            this.initializationComiteAvaliador.Activities.Add(this.faultHandlersActivity4);
            this.initializationComiteAvaliador.Name = "initializationComiteAvaliador";
            // 
            // eventDrivenInitialState
            // 
            this.eventDrivenInitialState.Activities.Add(this.onWorkflowIniciado);
            this.eventDrivenInitialState.Activities.Add(this.logInicioWorkflow);
            this.eventDrivenInitialState.Activities.Add(this.ifElseInicio);
            this.eventDrivenInitialState.Activities.Add(this.faultInicialState);
            this.eventDrivenInitialState.Name = "eventDrivenInitialState";
            // 
            // eventDrivenConfirmacaoPia
            // 
            this.eventDrivenConfirmacaoPia.Activities.Add(this.onTaskConfirmacaoPiaChanged);
            this.eventDrivenConfirmacaoPia.Activities.Add(this.logConfirmacaoPiaExecucao);
            this.eventDrivenConfirmacaoPia.Activities.Add(this.atribuirCamposPia);
            this.eventDrivenConfirmacaoPia.Activities.Add(this.completeTaskConfirmacaoPia);
            this.eventDrivenConfirmacaoPia.Activities.Add(this.ifElseConfirmacaoPia);
            this.eventDrivenConfirmacaoPia.Activities.Add(this.faultHandlersActivity2);
            this.eventDrivenConfirmacaoPia.Name = "eventDrivenConfirmacaoPia";
            // 
            // initializationConfirmacaoPia
            // 
            this.initializationConfirmacaoPia.Activities.Add(this.createConfirmacaoPiaTask);
            this.initializationConfirmacaoPia.Activities.Add(this.logConfirmacaoPiaEmail);
            this.initializationConfirmacaoPia.Activities.Add(this.atribuirStatusConfirmacaoPia);
            this.initializationConfirmacaoPia.Activities.Add(this.enviarEmailConfirmacaoPia);
            this.initializationConfirmacaoPia.Activities.Add(this.faultHandlersActivity1);
            this.initializationConfirmacaoPia.Activities.Add(this.cancellationHandlerActivity1);
            this.initializationConfirmacaoPia.Name = "initializationConfirmacaoPia";
            // 
            // eventDrivenColaborador
            // 
            this.eventDrivenColaborador.Activities.Add(this.onIdeiaChanged);
            this.eventDrivenColaborador.Activities.Add(this.logIdeiaStatus);
            this.eventDrivenColaborador.Activities.Add(this.ifElseColaborador);
            this.eventDrivenColaborador.Activities.Add(this.faultColaborador);
            this.eventDrivenColaborador.Name = "eventDrivenColaborador";
            // 
            // stateImplantacao
            // 
            this.stateImplantacao.Activities.Add(this.stateInitializationImplantacao);
            this.stateImplantacao.Activities.Add(this.eventDrivenImplantacao);
            this.stateImplantacao.Name = "stateImplantacao";
            // 
            // stateDecisao
            // 
            this.stateDecisao.Activities.Add(this.stateInitializationDecisao);
            this.stateDecisao.Activities.Add(this.eventDrivenDecisao);
            this.stateDecisao.Name = "stateDecisao";
            // 
            // stateAreaImpactada
            // 
            this.stateAreaImpactada.Activities.Add(this.initializationAreaImpactada);
            this.stateAreaImpactada.Activities.Add(this.eventDrivenAreaImpactada);
            this.stateAreaImpactada.Name = "stateAreaImpactada";
            // 
            // stateComiteAvaliador
            // 
            this.stateComiteAvaliador.Activities.Add(this.initializationComiteAvaliador);
            this.stateComiteAvaliador.Activities.Add(this.eventDrivenMA);
            this.stateComiteAvaliador.Activities.Add(this.eventDrivenRH);
            this.stateComiteAvaliador.Activities.Add(this.eventDrivenQA);
            this.stateComiteAvaliador.Activities.Add(this.eventDrivenSeguranca);
            this.stateComiteAvaliador.Activities.Add(this.eventDrivenCPD);
            this.stateComiteAvaliador.Activities.Add(this.eventDrivenEngenharia);
            this.stateComiteAvaliador.Activities.Add(this.eventDrivenAreaOpcional);
            this.stateComiteAvaliador.Name = "stateComiteAvaliador";
            // 
            // InitialState
            // 
            this.InitialState.Activities.Add(this.eventDrivenInitialState);
            this.InitialState.Name = "InitialState";
            // 
            // stateConclusao
            // 
            this.stateConclusao.Name = "stateConclusao";
            // 
            // stateConfirmacaoPIA
            // 
            this.stateConfirmacaoPIA.Activities.Add(this.initializationConfirmacaoPia);
            this.stateConfirmacaoPIA.Activities.Add(this.eventDrivenConfirmacaoPia);
            this.stateConfirmacaoPIA.Name = "stateConfirmacaoPIA";
            // 
            // stateColaborador
            // 
            this.stateColaborador.Activities.Add(this.eventDrivenColaborador);
            this.stateColaborador.Name = "stateColaborador";
            // 
            // FluxoFabrica
            // 
            this.Activities.Add(this.stateColaborador);
            this.Activities.Add(this.stateConfirmacaoPIA);
            this.Activities.Add(this.stateConclusao);
            this.Activities.Add(this.InitialState);
            this.Activities.Add(this.stateComiteAvaliador);
            this.Activities.Add(this.stateAreaImpactada);
            this.Activities.Add(this.stateDecisao);
            this.Activities.Add(this.stateImplantacao);
            this.CompletedStateName = "stateConclusao";
            this.DynamicUpdateCondition = null;
            this.InitialStateName = "InitialState";
            this.Name = "FluxoFabrica";
            this.CanModifyActivities = false;

        }

        #endregion

        private CodeActivity prepararSolicitacaoComite;

        private CodeActivity atribuirCamposImplantacao;

        private FaultHandlerActivity faultHandlerExecucaoImplantacao;

        private FaultHandlersActivity faultHandlersActivity8;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logErroExecucaoImplantacao;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logerroInicioImplantacao;

        private FaultHandlerActivity faultHandlerInicioImplantacao;

        private FaultHandlersActivity faultHandlersActivity7;

        private FaultHandlerActivity faultHandlerExecucaoDecisao;

        private FaultHandlersActivity faultHandlersActivity6;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logErroExecucaoDecisao;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logeErroInicioDecisao;

        private FaultHandlerActivity faultHandlerInicioDecisao;

        private FaultHandlersActivity faultHandlersActivity5;

        private CodeActivity DobrarPontuacao;

        private Microsoft.SharePoint.WorkflowActions.CompleteTask completeDecisaoTask;

        private Business.Activities.AtribuirStatusWorkflowActivity atribuirStatusImplantada;

        private Business.Activities.AtribuirStatusWorkflowActivity atribuirStatusInviavel;

        private Microsoft.SharePoint.WorkflowActions.CompleteTask completeImplantacaoTask;

        private SetStateActivity setStateConclusao;

        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged onTaskImplantacaoChanged;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logImplantacao;

        private IfElseBranchActivity ifElseNaoImplantada;

        private IfElseBranchActivity ifElseIdeiaImplantada;

        private IfElseActivity ifElseActivity9;

        private Business.Activities.AtribuirStatusWorkflowActivity atribuirStatusImplantacao;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logInicioImplantacao;

        private Microsoft.SharePoint.WorkflowActions.CreateTask createTaskImplantacao;

        private EventDrivenActivity eventDrivenImplantacao;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logMAConcluido;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logAreaOpcionalConcluido;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logEngenhanriaConcluido;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logCPDConcluido;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logSeguranca;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logQAConcluido;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logRHConcluido;

        private Microsoft.SharePoint.WorkflowActions.CreateTask createTaskAreaOpcional;

        private Microsoft.SharePoint.WorkflowActions.CreateTask createTaskEngenharia;

        private Microsoft.SharePoint.WorkflowActions.CreateTask createTaskCPD;

        private Microsoft.SharePoint.WorkflowActions.CreateTask createTaskSeguranca;

        private SetStateActivity setStateActivity1;

        private IfElseBranchActivity ifElseBranchActivity8;

        private IfElseBranchActivity ifElseBranchActivity7;

        private IfElseActivity ifElseActivity5;

        private Microsoft.SharePoint.WorkflowActions.CompleteTask completeTask4;

        private CodeActivity concluirSeguranca;

        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged onTaskChanged4;

        private EventDrivenActivity eventDrivenSeguranca;

        private SetStateActivity setStateActivity6;

        private IfElseBranchActivity ifElseBranchActivity11;

        private IfElseBranchActivity ifElseBranchActivity10;

        private IfElseActivity ifElseActivity6;

        private Microsoft.SharePoint.WorkflowActions.CompleteTask completeTask5;

        private CodeActivity concluirCPD;

        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged onTaskChanged5;

        private EventDrivenActivity eventDrivenCPD;

        private SetStateActivity setStateActivity7;

        private IfElseBranchActivity ifElseBranchActivity13;

        private IfElseBranchActivity ifElseBranchActivity12;

        private IfElseActivity ifElseActivity7;

        private Microsoft.SharePoint.WorkflowActions.CompleteTask completeTask6;

        private CodeActivity concluirEngenharia;

        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged onTaskChanged6;

        private EventDrivenActivity eventDrivenEngenharia;

        private SetStateActivity setStateActivity8;

        private IfElseBranchActivity ifElseBranchActivity15;

        private IfElseBranchActivity ifElseBranchActivity14;

        private IfElseActivity ifElseActivity8;

        private Microsoft.SharePoint.WorkflowActions.CompleteTask completeTask7;

        private CodeActivity concluirAreaOpcional;

        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged onTaskChanged7;

        private EventDrivenActivity eventDrivenAreaOpcional;

        private CodeActivity AtribuirPontos;

        private CodeActivity CopiarDadosDecisaoIdeia;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logIncioDecisao;

        private SetStateActivity setStateActivity5;

        private SetStateActivity setStateActivity4;

        private Business.Activities.EnviarEmailGrupoActivity enviarEmailGrupoActivity1;

        private Business.Activities.AtribuirStatusWorkflowActivity atribuirStatusWorkflowActivity1;

        private SetStateActivity setStateActivity3;

        private IfElseBranchActivity ifElseBranchActivity9;

        private IfElseBranchActivity ifElseDecisaoReprovar;

        private IfElseBranchActivity ifElseDecisaoAprovar;

        private IfElseActivity ifElseActivity4;

        private Business.Activities.AtribuirStatusWorkflowActivity atribuirStatusDecisao;

        private CodeActivity calcularResultadoComite;

        private Microsoft.SharePoint.WorkflowActions.CreateTask createTaskDecisao;

        private StateActivity stateImplantacao;

        private StateActivity stateDecisao;

        private StateInitializationActivity stateInitializationImplantacao;

        private StateInitializationActivity stateInitializationDecisao;

        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged onDecisaoTaskChanged;

        private EventDrivenActivity eventDrivenDecisao;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logFaultInicioComite;

        private FaultHandlerActivity faultInicioComite;

        private FaultHandlersActivity faultHandlersActivity4;

        private IfElseBranchActivity ifElseBranchActivity4;

        private IfElseBranchActivity ifElseBranchActivity3;

        private IfElseActivity ifElseActivity2;

        private IfElseBranchActivity ifElseBranchActivity6;

        private IfElseBranchActivity ifElseBranchActivity5;

        private IfElseActivity ifElseActivity3;

        private IfElseBranchActivity ifElseBranchActivity2;

        private IfElseBranchActivity ifElseBranchActivity1;

        private IfElseActivity ifElseActivity1;

        private SetStateActivity setStateComiteQA;

        private SetStateActivity setStateComiteRH;

        private SetStateActivity setStateComiteMA;

        private Microsoft.SharePoint.WorkflowActions.CompleteTask completeTask1;

        private CodeActivity concluirMA;

        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged onTaskChanged1;

        private Microsoft.SharePoint.WorkflowActions.CompleteTask completeTask3;

        private CodeActivity concluirQA;

        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged onTaskChanged3;

        private CodeActivity preparaCriacaoTasks;

        private Microsoft.SharePoint.WorkflowActions.CompleteTask completeTask2;

        private CodeActivity concluirRH;

        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged onTaskChanged2;

        private EventDrivenActivity eventDrivenQA;

        private EventDrivenActivity eventDrivenRH;

        private Microsoft.SharePoint.WorkflowActions.CreateTask createTaskQA;

        private Microsoft.SharePoint.WorkflowActions.CreateTask createTaskRH;

        private Microsoft.SharePoint.WorkflowActions.CreateTask createTaskMA;

        private EventDrivenActivity eventDrivenMA;

        private Microsoft.SharePoint.WorkflowActions.CompleteTask completeTaskAreaImpcatada;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logFaultExecucaoAreaImpactada;

        private FaultHandlerActivity faultExecucaoAreaImpactada;

        private FaultHandlersActivity faultHandlersExecucaoAreaImpactada;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logFailInicioAreaImpactada;

        private FaultHandlerActivity faultInicioAreaImpactada;

        private FaultHandlersActivity faultHandlersActivity3;

        private Business.Activities.AtribuirStatusWorkflowActivity atribuirStatusInicioComite;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logInicioComite;

        private Business.Activities.AtribuirStatusWorkflowActivity atribuirStatusConfirmacaoPia;

        private Business.Activities.AtribuirStatusWorkflowActivity atribuirStatusAreaImpactada;

        private CodeActivity copiarDadosAreaImpactada;

        private SetStateActivity setStateComite;

        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged onTaskAreaImpactadaChanged;

        private EventDrivenActivity eventDrivenAreaImpactada;

        private Business.Activities.EnviarEmailGrupoActivity enviarEmailAreaImpactada;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logInicioAreaImpactada;

        private Microsoft.SharePoint.WorkflowActions.CreateTask createTaskAreaImpactada;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logConfirmacaoPiaExecucao;

        private FaultHandlerActivity faultConfirmacaoPiaExecucao;

        private FaultHandlersActivity faultHandlersActivity2;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logFaultConfirmacaoPiaExecucao;

        private StateInitializationActivity initializationAreaImpactada;

        private StateActivity stateAreaImpactada;

        private CodeActivity atribuirCamposPia;

        private SetStateActivity setMaisInformacaoPia;

        private Business.Activities.AtribuirStatusWorkflowActivity atribuirStatusMaisInformacoesPia;

        private Business.Activities.EnviarEmailGrupoActivity enviarEmailMaisInformacoes;

        private Business.Activities.EnviarEmailGrupoActivity enviarEmailIdeiaReprovada;

        private StateActivity stateComiteAvaliador;

        private StateInitializationActivity initializationComiteAvaliador;

        private SetStateActivity setAprovadoPia;

        private IfElseBranchActivity ifElseMaisInformacao;

        private IfElseBranchActivity ifIdeiaReprovada;

        private IfElseBranchActivity ifIdeiaAprovada;

        private IfElseActivity ifElseConfirmacaoPia;

        private CancellationHandlerActivity cancellationHandlerActivity1;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logConfirmacaoPiaEmail;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logConfirmacaoPiaInicioErro;

        private FaultHandlerActivity faultConfirmacaoPiaInicio;

        private FaultHandlersActivity faultHandlersActivity1;

        private Business.Activities.EnviarEmailGrupoActivity enviarEmailConfirmacaoPia;

        private Business.Activities.AtribuirStatusWorkflowActivity atribuirStatusIdeiaReprovadaPia;

        private Microsoft.SharePoint.WorkflowActions.CompleteTask completeTaskConfirmacaoPia;

        private SetStateActivity setReprovadoPia;

        private TerminateActivity terminateActivityInitialState;

        private Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated onWorkflowIniciado;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logErroInitialState;

        private FaultHandlerActivity faultHandleInitialState;

        private FaultHandlersActivity faultInicialState;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logInicioWorkflow;

        private EventDrivenActivity eventDrivenInitialState;

        private StateActivity InitialState;

        private SetStateActivity setIniciadoSalvo;

        private SetStateActivity setIniciadoEnviado;

        private IfElseBranchActivity ifElseIniciadoSalvo;

        private IfElseBranchActivity ifElseIniciadoEnviado;

        private IfElseActivity ifElseInicio;

        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged onTaskConfirmacaoPiaChanged;

        private EventDrivenActivity eventDrivenConfirmacaoPia;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logIdeiaStatus;

        private SetStateActivity setItemSalvoState;

        private StateActivity stateConclusao;

        private StateActivity stateConfirmacaoPIA;

        private SetStateActivity setItemEnviadoState;

        private Microsoft.SharePoint.WorkflowActions.CreateTask createConfirmacaoPiaTask;

        private StateInitializationActivity initializationConfirmacaoPia;

        private IfElseBranchActivity elseItemSalvo;

        private IfElseBranchActivity ifItemEnviado;

        private IfElseActivity ifElseColaborador;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logFaultColaborador;

        private FaultHandlerActivity faultActivityColaborador;

        private FaultHandlersActivity faultColaborador;

        private Microsoft.SharePoint.WorkflowActions.OnWorkflowItemChanged onIdeiaChanged;

        private EventDrivenActivity eventDrivenColaborador;

        private StateActivity stateColaborador;

        private SetStateActivity setStateFaultColaborador;































































































































































































































































































































































































































































































    }
}
