﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;
using Microsoft.SharePoint.WorkflowActions;
using System.Collections.Generic;
using Ajinomoto.FabricaIdeia.Activities;
using Ajinomoto.FabricaIdeia.Business;
using System.Text;

namespace Ajinomoto.FabricaIdeia.FluxoFabrica
{
    public sealed partial class FluxoFabrica : StateMachineWorkflowActivity
    {
        public FluxoFabrica()
        {
            InitializeComponent();
        }

        public SPWorkflowActivationProperties workflowProperties = new SPWorkflowActivationProperties();

        public SPListItem Ideia
        {
            get
            {
                return workflowProperties.Item;
            }
        }
        public string StatusWorkflow
        {
            get
            {
                var ideiaStatusWorkflow = Ideia["StatusWorkflow"];
                return ideiaStatusWorkflow == null ? string.Empty : ideiaStatusWorkflow.ToString();
            }
        }

        public String Workflow_HistoryDescription = default(System.String);
        public String Workflow_HistoryOutcome = default(System.String);

        #region Inicio

        private void logInicioWorkflow_MethodInvoking(object sender, EventArgs e)
        {
            AtribuirErroAoLog(faultHandleInitialState);
        }

        #endregion

        #region Colaborador

        private void logFaultColaborador_MethodInvoking(object sender, EventArgs e)
        {
            AtribuirErroAoLog(faultActivityColaborador);
        }

        private void ColaboradorEnviouIdeia(object sender, ConditionalEventArgs e)
        {
            e.Result = StatusWorkflow == "Enviado";
        }

        #endregion

        #region Aprovacao PIA

        public Guid ConfirmacaoPia_TaskId = default(System.Guid);
        public SPWorkflowTaskProperties ConfirmacaoPia_TaskProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public SPWorkflowTaskProperties ConfirmacaoPia_AfterProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public SPWorkflowTaskProperties ConfirmacaoPia_BeforeProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();

        private void createConfirmacaoPiaTask_MethodInvoking(object sender, EventArgs e)
        {
            ConfirmacaoPia_TaskId = Guid.NewGuid();
            ConfirmacaoPia_TaskProperties.Title = "Avaliação de ideia";
            ConfirmacaoPia_TaskProperties.AssignedTo = "PIA";
            ConfirmacaoPia_TaskProperties.DueDate = DateTime.Today.AddDays(5);
            ConfirmacaoPia_TaskProperties.ExtendedProperties["IdeiaId"] = Ideia.ID;
            ConfirmacaoPia_TaskProperties.ExtendedProperties["Status Workflow"] = "Analise_Pia";
        }

        private void onTaskConfirmacaoPiaChanged_Invoked(object sender, ExternalDataEventArgs e)
        {
            ConfirmacaoPia_AfterProperties = onTaskConfirmacaoPiaChanged.AfterProperties;
            ConfirmacaoPia_BeforeProperties = onTaskConfirmacaoPiaChanged.BeforeProperties;
        }

        private void logConfirmacaoPiaInicioErro_MethodInvoking(object sender, EventArgs e)
        {
            AtribuirErroAoLog(faultConfirmacaoPiaInicio);
        }

        private void logConfirmacaoPiaExecucao_MethodInvoking(object sender, EventArgs e)
        {
            Workflow_HistoryOutcome = AcaoTarefa(ConfirmacaoPia_AfterProperties.ExtendedProperties);
        }

        private void IfIdeiaAprovada_ConfirmacaoPia(object sender, ConditionalEventArgs e)
        {
            e.Result = AcaoTarefa(ConfirmacaoPia_AfterProperties.ExtendedProperties) == "Aprovar";
        }

        private void IfIdeiaReprovada_ConfirmacaoPia(object sender, ConditionalEventArgs e)
        {
            e.Result = AcaoTarefa(ConfirmacaoPia_AfterProperties.ExtendedProperties) == "Reprovar";
        }

        private void logFaultConfirmacaoPiaExecucao_MethodInvoking(object sender, EventArgs e)
        {
            AtribuirErroAoLog(faultConfirmacaoPiaExecucao);
        }

        private void atribuirCamposPia_ExecuteCode(object sender, EventArgs e)
        {

            var tarefaId = onTaskConfirmacaoPiaChanged.TaskId;
            var tarefa = onTaskConfirmacaoPiaChanged.AfterProperties.ExtendedProperties;

            Ideia["ConsultarCPDPIA"] = RetornarValor(tarefa, "ConsultarCPD");
            Ideia["ConsultarEngenhariaPIA"] = RetornarValor(tarefa, "ConsultarEngenharia");
            Ideia["SegmentoOpcionalPIA"] = RetornarValor(tarefa, "SegmentoOpcional");
            Ideia["SetorOpcionalPIA"] = RetornarValor(tarefa, "SetorOpcional");
            Ideia["FeedbackPia"] += "<br />" + RetornarValor(tarefa, "Body");

            Ideia.SystemUpdate();
        }

        #endregion

        #region Area Impactada

        public Guid AreaImpactada_TaskId = default(System.Guid);
        public SPWorkflowTaskProperties AreaImpactada_TaskProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public SPWorkflowTaskProperties AreaImpactada_AfterProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public SPWorkflowTaskProperties AreaImpactada_BeforeProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String AreaImpactada_Grupo = default(System.String);

        private void onTaskAreaImpactadaChanged_Invoked(object sender, ExternalDataEventArgs e)
        {
            AreaImpactada_AfterProperties = onTaskAreaImpactadaChanged.AfterProperties;
            AreaImpactada_BeforeProperties = onTaskAreaImpactadaChanged.BeforeProperties;
        }

        private void createTaskAreaImpactada_MethodInvoking(object sender, EventArgs e)
        {
            AreaImpactada_Grupo = AreaImpactada_ObterGrupoSetor();

            AreaImpactada_TaskId = Guid.NewGuid();
            AreaImpactada_TaskProperties.Title = "Definição de avaliadores";
            AreaImpactada_TaskProperties.AssignedTo = AreaImpactada_Grupo;
            AreaImpactada_TaskProperties.DueDate = DateTime.Today.AddDays(5);
            AreaImpactada_TaskProperties.ExtendedProperties["IdeiaId"] = Ideia.ID;
            AreaImpactada_TaskProperties.ExtendedProperties["Status Workflow"] = "Analise_AreaImpactada";
        }

        private string AreaImpactada_ObterGrupoSetor()
        {
            return ObterValorEmOutraLista("SetorImpactado", "Setor", "Grupo");
        }

        private void copiarDadosAreaImpactada_ExecuteCode(object sender, EventArgs e)
        {
            var tarefa = onTaskAreaImpactadaChanged.AfterProperties.ExtendedProperties;

            Ideia["ConsultarCPDAreaImpactada"] = RetornarValor(tarefa, "ConsultarCPD");
            Ideia["ConsultarEngenhariaAreaImpactada"] = RetornarValor(tarefa, "ConsultarEngenharia");

            Ideia.SystemUpdate();
        }

        private void logFailInicioAreaImpactada_MethodInvoking(object sender, EventArgs e)
        {
            AtribuirErroAoLog(faultInicioAreaImpactada);
        }

        private void logFaultExecucaoAreaImpactada_MethodInvoking(object sender, EventArgs e)
        {
            AtribuirErroAoLog(faultExecucaoAreaImpactada);
        }

        #endregion

        #region Comite

        public List<int> aprovacoesComite;
        public List<Guid> Comite_TaskId = new List<Guid>();
        public List<SPWorkflowTaskProperties> Comite_TaskProperties = new List<SPWorkflowTaskProperties>();
        public Dictionary<string, TaskComite> Comite_GruposComite = new Dictionary<string, TaskComite>();
        public string Comite_GrupoSegmento { get; set; }

        public bool Comite_CPD { get; set; }
        public bool Comite_Engenharia { get; set; }
        public bool Comite_AreaOpcional { get; set; }
        public bool[] Comite_Ignorar { get; set; }
        public List<bool> Comite_HistoricoIgnorar = new List<bool>();

        private void preparaCriacaoTasks_ExecuteCode(object sender, EventArgs e)
        {
            aprovacoesComite = new List<int>();

            // usado para reavaliação
            var areaOpcional = Comite_ObterAreaOpcional();
            PrepararListaAreasIgnoradas(areaOpcional);
            Comite_HistoricoIgnorar.AddRange(Comite_Ignorar.ToList());

            Comite_GrupoSegmento = Comite_ObterGrupoSegmento();

            AdicionarComite("MA", new TaskComite("MA_" + Comite_GrupoSegmento, 0));
            AdicionarComite("RH", new TaskComite("RH_" + Comite_GrupoSegmento, 1));
            AdicionarComite("QA", new TaskComite("QA_" + Comite_GrupoSegmento, 2));
            AdicionarComite("Seguranca", new TaskComite("Seguranca_" + Comite_GrupoSegmento, 3));

            AdicionarComite("CPD", new TaskComite("CPD_" + Comite_GrupoSegmento, 4));
            AdicionarComite("Engenharia", new TaskComite("Engenharia_" + Comite_GrupoSegmento, 5));
            AdicionarComite("AreaOpcional", new TaskComite(areaOpcional, 6));

            for (int i = 0; i < Comite_GruposComite.Count; i++)
            {
                aprovacoesComite.Add(0);
                Comite_TaskId.Add(Guid.NewGuid());
                Comite_TaskProperties.Add(new SPWorkflowTaskProperties());
            }
        }

        private void PrepararListaAreasIgnoradas(string areaOpcional)
        {
            var engenhariaPia = Ideia.ObterValor("ConsultarEngenhariaPIA") == "Sim";
            var engenhariaAreaImpactada = Ideia.ObterValor("ConsultarEngenhariaAreaImpactada") == "Sim";
            var cpdPia = Ideia.ObterValor("ConsultarCPDPIA") == "Sim";
            var cpdAreaImpactada = Ideia.ObterValor("ConsultarCPDAreaImpactada") == "Sim";

            Comite_CPD = cpdPia || cpdAreaImpactada;
            Comite_Engenharia = engenhariaPia || engenhariaAreaImpactada;
            Comite_AreaOpcional = areaOpcional != "AreaOpcional";

            if (Comite_Ignorar == null)
            {
                Comite_Ignorar = new bool[] { false, false, false, false, false, false, false };

                Comite_Ignorar[4] = !Comite_CPD;
                Comite_Ignorar[5] = !Comite_Engenharia;
                Comite_Ignorar[6] = !Comite_AreaOpcional;
            }
        }

        private void AdicionarComite(string area, TaskComite taskComite)
        {
            if (Comite_GruposComite.ContainsKey(area))
                Comite_GruposComite[area] = taskComite;
            else
                Comite_GruposComite.Add(area, taskComite);
        }

        private string Comite_ObterAreaOpcional()
        {
            var areaOpcional = ObterValorEmOutraLista("SetorOpcionalPIA", "Setor", "Grupo");

            return string.IsNullOrEmpty(areaOpcional) ? "AreaOpcional" : areaOpcional;
        }


        private void createComiteTask_MethodInvoking(object sender, EventArgs e)
        {
            // recupera quem chamou e exclui os caracteres createTask. Ex: createTaskXXX
            var nomeGrupo = ((CallExternalMethodActivity)sender).Name.Substring(10);

            var grupo = Comite_GruposComite[nomeGrupo];

            criarTarefaComite(nomeGrupo);
        }

        private void criarTarefaComite(string nomeGrupo)
        {
            var grupo = Comite_GruposComite[nomeGrupo];
            int posicao = grupo.Posicao;

            Comite_TaskProperties[posicao].Title = "Avaliação do comitê";
            Comite_TaskProperties[posicao].AssignedTo = grupo.Responsavel;
            Comite_TaskProperties[posicao].DueDate = DateTime.Today.AddDays(5);
            Comite_TaskProperties[posicao].ExtendedProperties["IdeiaId"] = Ideia.ID;
            Comite_TaskProperties[posicao].ExtendedProperties["Status Workflow"] = "Analise_Comite";

            if (Comite_Ignorar[grupo.Posicao])
            {
                aprovacoesComite[posicao] = 1;
                Comite_TaskProperties[posicao].ExtendedProperties["Status"] = "Concluído";
            }
        }

        private void concluirTarefaComissao_ExecuteCode(object sender, EventArgs e)
        {
            var nomeGrupo = ((Activity)sender).Name.Substring(8);
            var grupo = Comite_GruposComite[nomeGrupo];

            aprovacoesComite[grupo.Posicao] = 1;
        }

        private void IfComiteConcluido(object sender, ConditionalEventArgs e)
        {
            e.Result = aprovacoesComite.Sum() == aprovacoesComite.Count;
        }


        private string Comite_ObterGrupoSegmento()
        {
            return ObterValorEmOutraLista("SegmentoImpactado", "Segmento", "Grupo");
        }

        private string ObterValorEmOutraLista(string campoIdeia, string lista, string campoLista)
        {
            var fieldSegmentoImpactado = Ideia.Fields.TryGetFieldByStaticName(campoIdeia);
            var dadosSegmento = Ideia[fieldSegmentoImpactado.Id];

            if (dadosSegmento != null && !string.IsNullOrEmpty(Ideia[fieldSegmentoImpactado.Id].ToString()))
            {
                var setorId = int.Parse(dadosSegmento.ToString().Split(';')[0]);

                var setores = workflowProperties.Web.Lists[lista];
                var setor = setores.Items.GetItemById(setorId);

                var fieldGrupo = setores.Fields.TryGetFieldByStaticName(campoLista);
                var grupo = setor[fieldGrupo.Id];

                return grupo == null ? string.Empty : grupo.ToString();
            }

            return string.Empty;
        }



        private void logFaultInicioComite_MethodInvoking(object sender, EventArgs e)
        {
            AtribuirErroAoLog(faultInicioComite);
        }

        #endregion

        #region Decisao Comite

        public Guid Decisao_TaskId = default(System.Guid);
        public SPWorkflowTaskProperties Decisao_TaskProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public SPWorkflowTaskProperties Decisao_AfterProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();

        private void onDecisaoTaskChanged_Invoked(object sender, ExternalDataEventArgs e)
        {
            Decisao_AfterProperties = onDecisaoTaskChanged.AfterProperties;
        }

        private void calcularResultadoComite_ExecuteCode(object sender, EventArgs e)
        {
            try
            {
                SPList lista = workflowProperties.Web.Lists.TryGetList("Tarefas Fluxo");
                StringBuilder notas = new StringBuilder();

                if (lista == null)
                    return;

                notas.Append(NotasDaAreaImpactada(lista));

                SPQuery q = new SPQuery();
                q.RowLimit = 100;
                q.Query = "<Where><And><Eq><FieldRef Name=\"IdeiaId\" /><Value Type=\"Number\">" + Ideia.ID + "</Value></Eq><Eq><FieldRef Name=\"StatusWorkflow\" /><Value Type=\"Text\">Analise_Comite</Value></Eq></And></Where>";

                SPListItemCollection linhas = lista.GetItems(q);

                for (int i = 0; i < linhas.Count; i++)
                {
                    var atribuidoA = linhas[i].ObterValor("AssignedTo");
                    var foco = linhas[i].ObterValor("Foco");
                    var relevancia = linhas[i].ObterValor("Relevancia");
                    var risco = linhas[i].ObterValor("Risco");
                    var comentario = linhas[i].ObterValor("Body");

                    atribuidoA = string.IsNullOrEmpty(atribuidoA) ? "vazio" : atribuidoA.Split('#')[1];
                    atribuidoA = string.IsNullOrEmpty(atribuidoA) ? string.Empty : atribuidoA.Split('_')[0];
                    comentario = string.IsNullOrEmpty(comentario) ? string.Empty : comentario.Replace(';', '_').Replace(':', '_').Replace('#', '_');

                    switch (atribuidoA)
                    {
                        case "MA":
                        case "QA":
                        case "RH":
                        case "Seguranca":
                            notas.Append(string.Format(":{0};{1};;;{2};{3}", atribuidoA, risco, comentario, Comite_HistoricoIgnorar[i]));
                            break;

                        case "CPD":
                            notas.Append(string.Format(":{0};{1};{2};;{3};{4}", atribuidoA, risco, foco, comentario, Comite_HistoricoIgnorar[i]));
                            break;

                        case "Engenharia":
                            notas.Append(string.Format(":{0};{1};{2};;{3};{4}", atribuidoA, risco, foco, comentario, Comite_HistoricoIgnorar[i]));
                            break;

                        default:
                            notas.Append(string.Format(":{0};{1};{2};{3};{4};{5}", atribuidoA, risco, foco, relevancia, comentario, Comite_HistoricoIgnorar[i]));
                            break;
                    }
                }

                Ideia["NotasComite"] = notas.ToString().Substring(1).Replace("True", "Sim").Replace("False", "Não");
                Ideia.SystemUpdate();
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex);
            }
        }

        private string NotasDaAreaImpactada(SPList lista)
        {
            string resultado = string.Empty;

            SPQuery q = new SPQuery();
            q.RowLimit = 100;
            q.Query = "<Where><And><Eq><FieldRef Name=\"IdeiaId\" /><Value Type=\"Number\">" + Ideia.ID + "</Value></Eq><Eq><FieldRef Name=\"StatusWorkflow\" /><Value Type=\"Text\">Analise_AreaImpactada</Value></Eq></And></Where>";

            SPListItemCollection linhas = lista.GetItems(q);

            if (linhas.Count > 0)
            {
                var atribuidoA = linhas[0].ObterValor("AssignedTo");
                var foco = linhas[0].ObterValor("Foco");
                var relevancia = linhas[0].ObterValor("Relevancia");
                var risco = linhas[0].ObterValor("Risco");
                var comentario = linhas[0].ObterValor("Body");

                atribuidoA = string.IsNullOrEmpty(atribuidoA) ? "vazio" : atribuidoA.Split('#')[1];
                atribuidoA = string.IsNullOrEmpty(atribuidoA) ? string.Empty : atribuidoA.Split('_')[0];
                comentario = string.IsNullOrEmpty(comentario) ? string.Empty : comentario.Replace(';', '_').Replace(':', '_').Replace('#', '_');

                resultado = string.Format(":Área impactada;{0};{1};{2};{3};Não", risco, foco, relevancia, comentario);
            }

            return resultado;
        }

        private void createTaskDecisao_MethodInvoking(object sender, EventArgs e)
        {
            Decisao_TaskId = Guid.NewGuid();
            Decisao_TaskProperties.Title = "Aprovação do resultado do Comitê";
            Decisao_TaskProperties.AssignedTo = "PIA";
            Decisao_TaskProperties.DueDate = DateTime.Today.AddDays(5);
            Decisao_TaskProperties.ExtendedProperties["IdeiaId"] = Ideia.ID;
            Decisao_TaskProperties.ExtendedProperties["Status Workflow"] = "Decisao_Comite";
        }

        private void CopiarDadosDecisaoIdeia_ExecuteCode(object sender, EventArgs e)
        {

        }

        private void AtribuirPontos_ExecuteCode(object sender, EventArgs e)
        {
            var listaPontos = workflowProperties.Web.Lists.TryGetList("Pontos");
            int pontos = 35;

            var crachaPrincipal = Ideia.ObterValor("Cracha");
            var crachas = new List<string>() { crachaPrincipal };
            var participantes = Ideia.ObterValor("Participantes");

            if (!string.IsNullOrEmpty(participantes))
            {
                foreach (var pessoa in participantes.Split(':'))
                {
                    crachas.Add(pessoa.Split(';')[0]);
                }

                pontos = 70 / crachas.Count;
            }

            foreach (var cracha in crachas)
            {
                SPListItem pontuacao = listaPontos.AddItem();

                pontuacao.AtribuirValor("Title", cracha);
                pontuacao.AtribuirValor("Pontos", pontos);
                pontuacao.AtribuirValor("IdeiaId", Ideia.ID);
                pontuacao.AtribuirValor("Data", DateTime.Today);
                pontuacao.SystemUpdate();
            }
        }


        private void IfIdeiaAprovada_Decisao(object sender, ConditionalEventArgs e)
        {
            e.Result = AcaoTarefa(Decisao_AfterProperties.ExtendedProperties) == "Aprovar";
        }

        private void IfIdeiaReprovada_Decisao(object sender, ConditionalEventArgs e)
        {
            e.Result = AcaoTarefa(Decisao_AfterProperties.ExtendedProperties) == "Reprovar";
        }

        private void logeErroInicioDecisao_MethodInvoking(object sender, EventArgs e)
        {
            AtribuirErroAoLog(faultHandlerInicioDecisao);
        }

        private void logErroExecucaoDecisao_MethodInvoking(object sender, EventArgs e)
        {
            AtribuirErroAoLog(faultHandlerExecucaoDecisao);
        }

        private void prepararSolicitacaoComite_ExecuteCode(object sender, EventArgs e)
        {
            Comite_Ignorar = new bool[] { true, true, true, true, true, true, true };

            var reavaliacaoComite = RetornarValor(onDecisaoTaskChanged.AfterProperties.ExtendedProperties, "ReavaliacaoComite");

            if (reavaliacaoComite == null)
                return;

            var grupos = reavaliacaoComite.ToString().Replace("#", "").Split(';');

            foreach (var grupo in grupos)
            {
                if(Comite_GruposComite.ContainsKey(grupo)) 
                {
                    Comite_Ignorar[Comite_GruposComite[grupo].Posicao] = false;
                }
                else if (grupo == "Area Opcional")
                {
                    Comite_Ignorar[Comite_GruposComite["AreaOpcional"].Posicao] = false;
                }
            }
        }

        #endregion

        #region Implantacao

        public Guid Implantacao_TaskId = default(System.Guid);
        public SPWorkflowTaskProperties Implantacao_TaskProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public SPWorkflowTaskProperties Implantacao_AfterProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String Implantacao_HistoryOutcome = default(System.String);

        private void logImplantacao_MethodInvoking(object sender, EventArgs e)
        {
            Implantacao_HistoryOutcome = AcaoTarefa(Implantacao_TaskProperties.ExtendedProperties);
        }

        private void onTaskImplantacaoChanged_Invoked(object sender, ExternalDataEventArgs e)
        {
            Implantacao_AfterProperties = onTaskImplantacaoChanged.AfterProperties;
        }

        private void createTaskImplantacao_MethodInvoking(object sender, EventArgs e)
        {
            Implantacao_TaskId = Guid.NewGuid();

            Implantacao_TaskProperties.Title = "Implantação da Ideia";
            Implantacao_TaskProperties.AssignedTo = "PIA";
            Implantacao_TaskProperties.DueDate = DateTime.Today.AddDays(5);
            Implantacao_TaskProperties.ExtendedProperties["IdeiaId"] = Ideia.ID;
            Implantacao_TaskProperties.ExtendedProperties["Status Workflow"] = "Implantacao";
        }

        private void IfIdeiaImplantada_Implantacao(object sender, ConditionalEventArgs e)
        {
            e.Result = AcaoTarefa(Implantacao_AfterProperties.ExtendedProperties) == "Implantado";
        }

        private void DobrarPontuacao_ExecuteCode(object sender, EventArgs e)
        {
            SPList lista = workflowProperties.Web.Lists.TryGetList("Pontos");

            if (lista == null)
                return;

            SPQuery q = new SPQuery();
            q.RowLimit = 100;
            q.Query = "<Where><Eq><FieldRef Name=\"IdeiaId\" /><Value Type=\"Number\">" + Ideia.ID + "</Value></Eq></Where>";

            SPListItemCollection linhas = lista.GetItems(q);

            for (int i = 0; i < linhas.Count; i++)
            {
                int pontos = 0;
                SPListItem pontuacao = linhas[i];

                int.TryParse(pontuacao.ObterValor("Pontos"), out pontos);

                pontuacao.AtribuirValor("Pontos", pontos * 2);
                pontuacao.SystemUpdate();
            }
        }

        private void logerroInicioImplantacao_MethodInvoking(object sender, EventArgs e)
        {
            AtribuirErroAoLog(faultHandlerInicioImplantacao);
        }

        private void logErroExecucaoImplantacao_MethodInvoking(object sender, EventArgs e)
        {
            AtribuirErroAoLog(faultHandlerExecucaoImplantacao);
        }

        private void atribuirCamposImplantacao_ExecuteCode(object sender, EventArgs e)
        {
            var tarefa = onTaskImplantacaoChanged.AfterProperties.ExtendedProperties;

            Ideia["DataImplantacao"] = RetornarValor(tarefa, "DataImplantacao");

            Ideia.SystemUpdate();
        }

        #endregion

        private void AtribuirErroAoLog(FaultHandlerActivity faultActivity)
        {
            var ex = faultActivity.Fault;

            if (ex == null)
            {
                Workflow_HistoryDescription = "Valor da exception vazio.";
                Workflow_HistoryOutcome = faultActivity.Name;
            }
            else
            {
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }

                Workflow_HistoryDescription = ex.Message;
                Workflow_HistoryOutcome = ex.StackTrace;
            }
        }

        public object RetornarValor(Hashtable valores, string staticName)
        {
            var fields = workflowProperties.TaskList.Fields;
            var fieldId = fields.TryGetFieldByStaticName(staticName).Id;

            return valores[fieldId];
        }

        private string AcaoTarefa(Hashtable taskProperties)
        {
            var field = workflowProperties.TaskList.Fields.GetFieldByInternalName("Acao");
            var acao = taskProperties[field.Id];

            return acao == null ? string.Empty : acao.ToString();
        }
    }
}
