﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Page Language="C#" CodeBehind="EditarIdeia.aspx.cs" Inherits="Ajinomoto.FabricaIdeia.IdeiaWebPart.EditarIdeia" 
    MasterPageFile="~masterurl/default.master" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Import Namespace="Microsoft.SharePoint" %> <%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Aji" Namespace="Ajinomoto.FabricaIdeia.Business" Assembly="$SharePoint.Project.AssemblyFullName$" %>



<asp:Content ID="Content1" ContentPlaceHolderId="PlaceHolderPageTitle" runat="server">
	<SharePoint:ListFormPageTitle ID="ListFormPageTitle1" runat="server"/>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderId="PlaceHolderPageTitleInTitleArea" runat="server">
	<span class="die">
	<SharePoint:ListProperty Property="LinkTitle" runat="server" id="ID_LinkTitle"/>: </span>
	<SharePoint:ListItemProperty id="ID_ItemProperty" maxlength="40" runat="server"/>
</asp:Content>






<asp:Content ID="Content3" ContentPlaceHolderId="PlaceHolderPageImage" runat="server">
	<img src="/_layouts/images/blank.gif" width='1' height='1' alt="" />
</asp:Content>




<asp:Content ID="Content4" ContentPlaceHolderId="PlaceHolderLeftNavBar" runat="server" >
				<div class="ms-quicklaunchouter">
				<div class="ms-quickLaunch">
				<Sharepoint:UIVersionedContent ID="UIVersionedContent2" runat="server" UIVersion="4">
					<ContentTemplate>
						<h2 style="display:inline;" class="ms-hidden"><SharePoint:EncodedLiteral ID="EncodedLiteral1" runat="server" text="<%$Resources:wss,quiklnch_pagetitle%>" EncodeMethod="HtmlEncode"/></h2>
					</ContentTemplate>
				</SharePoint:UIVersionedContent>
				<SharePoint:UIVersionedContent ID="UIVersionedContent3" UIVersion="3" runat="server">
					<ContentTemplate>
						<h3 class="ms-standardheader"><label class="ms-hidden"><SharePoint:EncodedLiteral ID="EncodedLiteral2" runat="server" text="<%$Resources:wss,quiklnch_pagetitle%>" EncodeMethod="HtmlEncode"/></label>
						<Sharepoint:SPSecurityTrimmedControl ID="SPSecurityTrimmedControl1" runat="server" PermissionsString="ViewFormPages">
							<div class="ms-quicklaunchheader"><SharePoint:SPLinkButton id="idNavLinkViewAll" runat="server" NavigateUrl="~site/_layouts/viewlsts.aspx" Text="<%$Resources:wss,quiklnch_allcontent%>" accesskey="<%$Resources:wss,quiklnch_allcontent_AK%>"/></div>
						</SharePoint:SPSecurityTrimmedControl>
						</h3>
					</ContentTemplate>
				</SharePoint:UIVersionedContent>
				<Sharepoint:SPNavigationManager
				id="QuickLaunchNavigationManager"
				runat="server"
				QuickLaunchControlId="QuickLaunchMenu"
				ContainedControl="QuickLaunch"
				EnableViewState="false"
				CssClass="ms-quicklaunch-navmgr"
				>
				<div>
					<SharePoint:DelegateControl ID="DelegateControl1" runat="server"
						ControlId="QuickLaunchDataSource">
					 <Template_Controls>
						<asp:SiteMapDataSource
						SiteMapProvider="SPNavigationProvider"
						ShowStartingNode="False"
						id="QuickLaunchSiteMap"
						StartingNodeUrl="sid:1025"
						runat="server"
						/>
					 </Template_Controls>
					</SharePoint:DelegateControl>


					<SharePoint:AspMenu
					  id="V4QuickLaunchMenu"
					  runat="server"
					  EnableViewState="false"
					  DataSourceId="QuickLaunchSiteMap"
					  UseSimpleRendering="true"
					  Orientation="Vertical"
					  StaticDisplayLevels="2"
					  MaximumDynamicDisplayLevels="0"
					  SkipLinkText=""
					  CssClass="s4-ql" />
				</ContentTemplate>

				</div>
				</Sharepoint:SPNavigationManager>

					<Sharepoint:SPNavigationManager
					id="TreeViewNavigationManagerV4"
					runat="server"
					ContainedControl="TreeView"
					CssClass="s4-treeView"
					>
					  <SharePoint:SPLinkButton runat="server" NavigateUrl="~site/_layouts/viewlsts.aspx" id="idNavLinkSiteHierarchyV4" Text="<%$Resources:wss,treeview_header%>" accesskey="<%$Resources:wss,quiklnch_allcontent_AK%>" CssClass="s4-qlheader" />
						  <div class="ms-treeviewouter">
							<SharePoint:DelegateControl ID="DelegateControl3" runat="server" ControlId="TreeViewAndDataSource">
							  <Template_Controls>
								<SharePoint:SPHierarchyDataSourceControl
								 runat="server"
								 id="TreeViewDataSourceV4"
								 RootContextObject="Web"
								 IncludeDiscussionFolders="true"
								/>
								<SharePoint:SPRememberScroll runat="server" id="TreeViewRememberScrollV4" onscroll="javascript:_spRecordScrollPositions(this);" style="overflow: auto;height: 400px;width: 155px; ">
								  <Sharepoint:SPTreeView
									id="WebTreeViewV4"
									runat="server"
									ShowLines="false"
									DataSourceId="TreeViewDataSourceV4"
									ExpandDepth="0"
									SelectedNodeStyle-CssClass="ms-tvselected"
									NodeStyle-CssClass="ms-navitem"
									SkipLinkText=""
									NodeIndent="12"
									ExpandImageUrl="/_layouts/images/tvclosed.png"
									ExpandImageUrlRtl="/_layouts/images/tvclosedrtl.png"
									CollapseImageUrl="/_layouts/images/tvopen.png"
									CollapseImageUrlRtl="/_layouts/images/tvopenrtl.png"
									NoExpandImageUrl="/_layouts/images/tvblank.gif"
								  >
								  </Sharepoint:SPTreeView>
								</Sharepoint:SPRememberScroll>
							  </Template_Controls>
							</SharePoint:DelegateControl>
						  </div>
					</Sharepoint:SPNavigationManager>



						<ul class="s4-specialNavLinkList">
							<li>
								<SharePoint:ClusteredSPLinkButton
									runat="server"
									NavigateUrl="~site/_layouts/recyclebin.aspx"
									ImageClass="s4-specialNavIcon"
									ImageUrl="/_layouts/images/fgimg.png"
									ImageWidth=16
									ImageHeight=16
									OffsetX=0
									OffsetY=428
									id="idNavLinkRecycleBin"
									Text="<%$Resources:wss,StsDefault_RecycleBin%>"
									CssClass="s4-rcycl"
									PermissionsString="DeleteListItems" />
							</li>
							<li>
								<SharePoint:ClusteredSPLinkButton
									id="idNavLinkViewAllV4"
									runat="server"
									PermissionsString="ViewFormPages"
									NavigateUrl="~site/_layouts/viewlsts.aspx"
									ImageClass="s4-specialNavIcon"
									ImageUrl="/_layouts/images/fgimg.png"
									ImageWidth=16
									ImageHeight=16
									OffsetX=0
									OffsetY=0
									Text="<%$Resources:wss,quiklnch_allcontent_short%>"
									accesskey="<%$Resources:wss,quiklnch_allcontent_AK%>"/>
							</li>
						</ul>

				</div>
				</div>
</asp:Content>







<asp:Content ID="Content5" ContentPlaceHolderId="PlaceHolderMain" runat="server">
	<div style="padding-left:5px">
	<table cellpadding="0" cellspacing="0" id="onetIDListForm" style="width:100%">
	 <tr>
	  <td>
	    <WebPartPages:WebPartZone runat="server" FrameType="None" ID="Main" Title="loc:Main">
            <ZoneTemplate>
            </ZoneTemplate>
        </WebPartPages:WebPartZone>
	    <img src="/_layouts/images/blank.gif" width='590' height='1' alt="" />
        
	<table  border="0"  width="100%"> 
        <tr>
            <td>
                <SharePoint:FormToolBar ID="spToolBar" runat="server" />
            </td>
        </tr>
		<tr>
			<td>
                <span id="part1">
				<table border="0" cellspacing="0" width="100%">
                    <tr runat="server" ID="feedbackTr" Visible="false">
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Feedback
								</nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
							<asp:Label runat="server" ID="feedbackLabel" />
						</td>
					</tr>
					<tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Crachá<span class="ms-formvalidation"> *</span></nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
                            <table>
                                <tr>
                                    <td>
                                        <SharePoint:FormField runat="server" id="crachaPrincipal" ControlMode="New" FieldName="Cracha" />
                                    </td>
                                    <td>
							            <asp:Button runat="server" ID="buscarNome" onClick="buscarNome_Click" Text="Buscar" CausesValidation="False" />
                                        <SharePoint:FieldDescription runat="server" id="ff2description1" FieldName="Cracha" ControlMode="New"/>
                                    </td>
                                </tr>
                            </table>
						</td>
					</tr>
                    <tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Nome</nobr>
							</H3>
						</td>
                        <td width="400px" valign="top" class="ms-formbody">
                            <asp:Label runat="server" ID="nomePrincipalLabel" />
                        </td>
						<td width="400px" valign="top" class="ms-formbody" style="display: none">
							<SharePoint:FormField runat="server" id="nomePrincipal" ControlMode="New" FieldName="Title" />
							<SharePoint:FieldDescription runat="server" id="ff1description1" FieldName="Title" ControlMode="New"/>
						</td>
					</tr>
					<tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Participantes
								</nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
                            <table width="100%" runat="server" id="adicionarParticipante">
                                <tr>
                                    <td>Crachá do participante:</td>
                                    <td>
                                        <asp:TextBox 
                                            runat="server"
                                            ID="crachaTextBox" />
                                    </td>
                                    <td>
                                        <asp:LinkButton 
                                            runat="server" 
                                            ID="novoParticipante" 
                                            Text="incluir"
                                            onClick="novoParticipante_Click"
                                            CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>

                            <SharePoint:SPGridView
                                ID="gridParticipantes"
                                runat="server"
                                AutoGenerateColumns="False"
                                OnRowCommand="gridParticipantes_RowCommand">
                                <Columns> 
                                    <SharePoint:SPBoundField HeaderText="Nome" DataField="Nome" /> 
                                    <SharePoint:SPBoundField HeaderText="Crachá" DataField="Cracha" /> 
                                    <asp:TemplateField HeaderText=""> 
                                        <ItemTemplate> 
                                            <asp:LinkButton 
                                                ID="_link" 
                                                runat="server"  
                                                CommandArgument='<%# Eval("Cracha") %>' 
                                                Text="excluir" 
                                                CausesValidation="False" /> 
                                        </ItemTemplate> 
                                    </asp:TemplateField> 
                                </Columns> 
                            </SharePoint:SPGridView>

							<!--<SharePoint:FormField runat="server" id="ff31" ControlMode="New" FieldName="Participantes" />
							<SharePoint:FieldDescription runat="server" id="ff3description1" FieldName="Participantes" ControlMode="New"/>-->
						</td>
					</tr>
					<tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Termo de aceite<span class="ms-formvalidation"> *</span>
								</nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
                            "Todas as sugestões de melhoria e inovação propostas pelos colaboradores participantes, bem como os estudos e projetos provenientes das sugestões, mesmo em casos de geração de patentes, são de propriedade da Ajinomoto do Brasil Indústria e Comércio de Alimentos Ltda., não cabendo nenhum tipo de reivindicação de autoria ou qualquer tipo compensação fora do previsto pelo regulamento do sistema.
                            <br /><br />
                            Pela presente, informo que tive conhecimento, por parte da empresa, dos termos da Lei número 9.279, de 14 de maio de 1996, sobretudo o seu artigo 88, que dispõe que toda a invenção e modelos de utilidade que sejam decorrentes do contrato de trabalho pertencem exclusivamente à empresa (empregadora)
							<br /><br />
                            <SharePoint:FormField runat="server" id="ff41" ControlMode="New" FieldName="Termo" />
							<SharePoint:FieldDescription runat="server" id="ff4description1" FieldName="Termo" ControlMode="New"/>
						</td>
					</tr>
					<tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Objetivo<span class="ms-formvalidation"> *</span></nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
							<SharePoint:FormField runat="server" id="ff51" ControlMode="New" FieldName="Objetivo" />
							<SharePoint:FieldDescription runat="server" id="ff5description1" FieldName="Objetivo" ControlMode="New"/>
						</td>
					</tr>
					<tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Segmento Impactado<span class="ms-formvalidation"> *</span></nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
							<SharePoint:FormField runat="server" id="ff61" ControlMode="New" FieldName="SegmentoImpactado" />
							<SharePoint:FieldDescription runat="server" id="ff6description1" FieldName="SegmentoImpactado" ControlMode="New"/>
						</td>
					</tr>
					<tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Setor Impactado<span class="ms-formvalidation"> *</span></nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
							<SharePoint:FormField runat="server" id="ff71" ControlMode="New" FieldName="SetorImpactado" />
							<SharePoint:FieldDescription runat="server" id="ff7description1" FieldName="SetorImpactado" ControlMode="New"/>
						</td>
					</tr>
					<tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Descreva a condição atual<span class="ms-formvalidation"> *</span></nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
							<SharePoint:FormField runat="server" id="ff81" ControlMode="New" FieldName="CondicaoAtual" />
							<SharePoint:FieldDescription runat="server" id="ff8description1" FieldName="CondicaoAtual" ControlMode="New"/>
						</td>
					</tr>
					<tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Qual a proposta?<span class="ms-formvalidation"> *</span></nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
							<SharePoint:FormField runat="server" id="ff91" ControlMode="New" FieldName="Proposta" />
							<SharePoint:FieldDescription runat="server" id="ff9description1" FieldName="Proposta" ControlMode="New"/>
						</td>
					</tr>
					<tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Qual o benefício esperado?<span class="ms-formvalidation"> *</span></nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
							<SharePoint:FormField runat="server" id="ff101" ControlMode="New" FieldName="BeneficioEsperado" />
							<SharePoint:FieldDescription runat="server" id="ff10description1" FieldName="BeneficioEsperado" ControlMode="New"/>
						</td>
					</tr>
                    <tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Aplicável em outra área?</nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
							<table width="100%" runat="server" id="adicionarOutraArea">
                                <tr>
                                    <td>
                                        <SharePoint:LookupField runat="server" id="segmentoAplicavel" ControlMode="New" FieldName="Segmento" />
                                    </td>
                                    <td>
                                        <SharePoint:LookupField runat="server" id="setorAplicavel" ControlMode="New" FieldName="Setor" />
                                    </td>
                                    <td>
                                        <asp:LinkButton 
                                            runat="server" 
                                            ID="novaArea" 
                                            Text="incluir"
                                            onClick="novaArea_Click"
                                            CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>

                            <SharePoint:SPGridView
                                ID="gridOutrasAreas"
                                runat="server"
                                AutoGenerateColumns="False"
                                OnRowCommand="gridOutrasAreas_RowCommand">
                                <Columns> 
                                    <SharePoint:SPBoundField HeaderText="Segmento" DataField="Segmento" /> 
                                    <SharePoint:SPBoundField HeaderText="Setor" DataField="Setor" /> 
                                    <asp:TemplateField HeaderText=""> 
                                        <ItemTemplate> 
                                            <asp:LinkButton 
                                                ID="_link" 
                                                runat="server"  
                                                CommandArgument='<%# Eval("Id") %>' 
                                                Text="excluir" 
                                                CausesValidation="False" /> 
                                        </ItemTemplate> 
                                    </asp:TemplateField> 
                                </Columns> 
                            </SharePoint:SPGridView>
                            
                            <!--<SharePoint:FormField runat="server" id="ff111" ControlMode="New" FieldName="AplicavelOutraArea" />
							<SharePoint:FieldDescription runat="server" id="ff11description1" FieldName="AplicavelOutraArea" ControlMode="New"/>-->
						</td>
					</tr>
                    <tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Necessita investimento?</nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
							<SharePoint:FormField runat="server" id="ff121" ControlMode="New" FieldName="NecessitaInvestimento" />
							<SharePoint:FieldDescription runat="server" id="ff12description1" FieldName="NecessitaInvestimento" ControlMode="New"/>
						</td>
					</tr>
                    <tr>
						<td width="190px" valign="top" class="ms-formlabel">
							<H3 class="ms-standardheader">
								<nobr>Materiais e equipamentos</nobr>
							</H3>
						</td>
						<td width="400px" valign="top" class="ms-formbody">
							<SharePoint:FormField runat="server" id="ff131" ControlMode="New" FieldName="MateriaisEquipamentos" />
							<SharePoint:FieldDescription runat="server" id="ff13description1" FieldName="MateriaisEquipamentos" ControlMode="New"/>
						</td>
					</tr>
					<tr id="idAttachmentsRow">
						<td nowrap="true" valign="top" class="ms-formlabel" width="20%">
							<SharePoint:FieldLabel ControlMode="New" FieldName="Attachments" runat="server"/>
						</td>
						<td valign="top" class="ms-formbody" width="80%">
							<SharePoint:FormField runat="server" id="AttachmentsField" ControlMode="New" FieldName="Attachments" />
							<script>
                                var elm = document.getElementById(&quot;idAttachmentsTable&quot;);
                                if (elm == null || elm.rows.length == 0)
                                document.getElementById(&quot;idAttachmentsRow&quot;).style.display=&apos;none&apos;;
                            </script>
						</td>
					</tr>
                    <tr>
			            <td class="ms-toolbar" nowrap="nowrap">
				            <table>
					            <tr>
						            <td width="99%" class="ms-toolbar" nowrap="nowrap"><IMG SRC="/_layouts/images/blank.gif" width="1" height="18"/></td>
						            <td class="ms-toolbar" nowrap="nowrap">
							            <Aji:SubmeterButton runat="server" id="salvarButton" Coluna="StatusWorkflow" Valor="Salvo" Texto="Salvar" />
						            </td>
						            <td class="ms-separator"> </td>
                                    <td class="ms-toolbar" nowrap="nowrap">
                                        <Aji:SubmeterButton runat="server" id="submeterButton" Coluna="StatusWorkflow" Valor="Enviado" Texto="Enviar" />
						            </td>
						            <td class="ms-separator"> </td>
						            <td class="ms-toolbar" nowrap="nowrap" align="right">
							            <SharePoint:GoBackButton runat="server" ControlMode="New" id="gobackbutton2"/>
						            </td>
					            </tr>
				            </table>
			            </td>
		            </tr>
				</table>
                </span>
                <SharePoint:AttachmentUpload  runat="server"  ControlMode="New" id="attachmentUpload" /> 
                <SharePoint:ItemHiddenVersion  runat="server"  ControlMode="New" id="itemHiddenVersion" />
			</td>
		</tr>		
        </table>
	  </td>
	 </tr>
	</table>
        <SharePoint:scriptlink ID="ScriptLink" Name="SP.js" runat="server" OnDemand="true" Localizable="false"></SharePoint:ScriptLink> 
        <script type="text/javascript" src="/_layouts/JS/SolidQ-SPS-JS.js"></script>
        
        <script type="text/javascript" src="/_layouts/JS/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="/_layouts/JS/Validacao.js"></script>

        <asp:Panel runat="server" id="scriptsPanel">	
            <script type="text/javascript">
	            var setorListId = '<%= this.SetorId %>';
	            var segmentoListId = '<%= this.SegmentoId %>';
		
	            ExecuteOrDelayUntilScriptLoaded(InitCascade, "sp.js");
	            function InitCascade()
	            {		
                    setor = new SolidQLookupField(setorListId ,'Setor');
		            segmento = new SolidQLookupField(segmentoListId ,'Segmento');
			
		            var relImpactado = new SolidQCascadeRelationShip(segmento ,setor ,'Segmento');


		            setorImpactado = new SolidQLookupField(setorListId ,'Setor Impactado');
		            segmentoImpactado = new SolidQLookupField(segmentoListId ,'Segmento Impactado');
			
		            var relImpactado = new SolidQCascadeRelationShip(segmentoImpactado ,setorImpactado ,'Segmento');
	            }
            </script>
        </asp:Panel>
	</div>
    <script type="text/javascript">
            var cracha;
            var termoAceite;
            var objetivo;
            var segmentoImpactadoDropDownList;
            var setorImpactadoDropDownList;
            var condicao;
            var proposta;
            var beneficio;

            $(function () {
                RedimensionarCracha();
                LocalizarCampos();

                addToPostBack(function (t, a) {
                    return ValidarFormulario(t, a);
                });

                
            });

            function RedimensionarCracha() {
                $("[title=Crachá]").css("width", "250px");
            }

            function LocalizarCampos() {
                cracha = ObterLinha("Crachá");
                nome =  ObterLinha("Nome;Name;BlaBla");
                termoAceite = ObterLinha("Termo de aceite");
                objetivo = ObterLinha("Objetivo");
                segmentoImpactadoDropDownList = ObterLinha("Segmento Impactado");
                setorImpactadoDropDownList = ObterLinha("Setor Impactado");

                condicao = ObterLinha("Descreva a condição atual");
                proposta = ObterLinha("Qual a proposta?");
                beneficio = ObterLinha("Qual o benefício esperado?");
            }

            function ValidarFormulario(t, a) {
                var mensagem = "";

                if(t.indexOf("novoParticipante") > -1 
                    || t.indexOf("novaArea") > -1
                    || t.indexOf("gridParticipantes") > -1
                    || t.indexOf("gridOutrasAreas") > -1)
                    return true;

                if(t.indexOf("salvarButton") > -1) {
                    mensagem += VerificarPreenchimento(nome, "Nome");
                }
                else {
                    // campos de preenchimento obrigatorio
                    mensagem += VerificarPreenchimento(cracha, "Crachá");
                    mensagem += VerificarPreenchimento(nome, "Nome");

                    mensagem += VerificarAceitacaoTermo();
                
                    mensagem += VerificarSelecao(objetivo, "Objetivo");

                    mensagem += VerificarDropDown(segmentoImpactadoDropDownList, "Segmento Impactado");
                    mensagem += VerificarDropDown(setorImpactadoDropDownList, "Setor Impactado");

                    mensagem += VerificarTextArea(condicao, "Descreva a condição...");
                    mensagem += VerificarTextArea(proposta, "Qual a proposta?");
                    mensagem += VerificarTextArea(beneficio, "Qual o benefício...");
                }

                if (mensagem != "") {
                    alert(mensagem);

                    return false;
                }

                return true;
            }

            function VerificarAceitacaoTermo() {
                var selecao = VerificarSelecao(termoAceite, "Termo de aceite");

                if(selecao != "")
                    return selecao;

                if (!SelecaoContemOpcao(termoAceite, "Li e Concordo")) {
                    termoAceite.addClass("erro-item");
                    return "Você deve aceitar o Termo de Aceite para prosseguir. \n";
                }
                else {
                    termoAceite.removeClass("erro-item");
                    return "";
                }
            }
        </script>
    <style type="text/css">
        .erro-item 
        {
            background-color: #ff9494;
        }
    </style>
</asp:Content>






<asp:Content ID="Content6" ContentPlaceHolderId="PlaceHolderAdditionalPageHead" runat="server">
    <SharePoint:CssRegistration Name="forms.css" runat="server"/>
</asp:Content>


<asp:Content ID="Content7" ContentPlaceHolderId="PlaceHolderTitleLeftBorder" runat="server">
<table cellpadding="0" height="100%" width="100%" cellspacing="0">
 <tr><td class="ms-areaseparatorleft"><img src="/_layouts/images/blank.gif" width='1' height='1' alt="" /></td></tr>
</table>
</asp:Content>



<asp:Content ID="Content8" ContentPlaceHolderId="PlaceHolderTitleAreaClass" runat="server">
<script type="text/javascript" id="onetidPageTitleAreaFrameScript">
    if (document.getElementById("onetidPageTitleAreaFrame") != null) {
        document.getElementById("onetidPageTitleAreaFrame").className = "ms-areaseparator";
    }
</script>
</asp:Content>



<asp:Content ID="Content9" ContentPlaceHolderId="PlaceHolderBodyAreaClass" runat="server">
<style type="text/css">
.ms-bodyareaframe {
	padding: 8px;
	border: none;
}
</style>
</asp:Content>




<asp:Content ID="Content10" ContentPlaceHolderId="PlaceHolderBodyLeftBorder" runat="server">
<div class='ms-areaseparatorleft'><img src="/_layouts/images/blank.gif" width='8' height='100%' alt="" /></div>
</asp:Content>




<asp:Content ID="Content11" ContentPlaceHolderId="PlaceHolderTitleRightMargin" runat="server">
<div class='ms-areaseparatorright'><img src="/_layouts/images/blank.gif" width='8' height='100%' alt="" /></div>
</asp:Content>



<asp:Content ID="Content12" ContentPlaceHolderId="PlaceHolderBodyRightMargin" runat="server">
<div class='ms-areaseparatorright'><img src="/_layouts/images/blank.gif" width='8' height='100%' alt="" /></div>
</asp:Content>



<asp:Content ID="Content13" ContentPlaceHolderId="PlaceHolderTitleAreaSeparator" runat="server"/>
