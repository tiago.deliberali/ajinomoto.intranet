﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;
using System.Data;
using System.Linq;
using Ajinomoto.FabricaIdeia.Business;
using System.Collections.Generic;
using Ajinomoto.FabricaIdeia.Business;

namespace Ajinomoto.FabricaIdeia.IdeiaWebPart
{
    public partial class EditarIdeia : WebPartPage
    {
        public string SetorId
        {
            get
            {
                if (Cache["EditarIdeia.SetorId"] == null)
                {
                    var id = SPContext.Current.Web.Lists["Setor"].ID;
                    Cache.Add("EditarIdeia.SetorId", id, null, System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null);
                }

                return Cache["EditarIdeia.SetorId"].ToString();
            }
        }
        public string SegmentoId 
        {
            get
            {
                if (Cache["EditarIdeia.SegmentoId"] == null)
                {
                    var id = SPContext.Current.Web.Lists["Segmento"].ID;
                    Cache.Add("EditarIdeia.SegmentoId", id, null, System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null);
                }

                return Cache["EditarIdeia.SegmentoId"].ToString();
            }
        }

        DataTable Participantes
        {
            get
            {
                return (DataTable)ViewState["participantes"];
            }
            set
            {
                ViewState["participantes"] = value;
            }
        }
        DataTable OutrasAreas
        {
            get
            {
                return (DataTable)ViewState["outrasAreas"];
            }
            set
            {
                ViewState["outrasAreas"] = value;
            }
        }

        ControladorSPGrid controladorParticipantes;
        ControladorSPGrid controladorOutrasAreas;

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);

            ribbon.TrimById("Ribbon.ListForm.Display.Manage.EditSeries");
            ribbon.TrimById("Ribbon.ListForm.Display.Manage.VersionHistory");
            ribbon.TrimById("Ribbon.ListForm.Display.Manage.ManagePermissions");
            ribbon.TrimById("Ribbon.ListForm.Display.Manage.DeleteItem");
            ribbon.TrimById("Ribbon.ListForm.Display.Actions");
            ribbon.TrimById("Ribbon.ListForm.Display.HealthActions");
            ribbon.TrimById("Ribbon.ListForm.Display.Solution");

            ribbon.MakeTabAvailable("Ribbon.ListForm.Edit");

            ribbon.TrimById("Ribbon.ListForm.Edit.Commit");
            ribbon.TrimById("Ribbon.ListForm.Edit.Actions.DistributionListsApproval");
            ribbon.TrimById("Ribbon.ListForm.Edit.Actions.EditSeries");
            ribbon.TrimById("Ribbon.ListForm.Edit.Actions.DeleteItem");
            ribbon.TrimById("Ribbon.ListForm.Edit.Actions.ClaimReleaseTask");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ConfigurarDataTables();
            }

            ConfigurarControladores();
            DefinirModo();

            var feedback = SPContext.Current.Item["Body"];
            if (feedback != null
                && !string.IsNullOrEmpty(feedback.ToString()))
            {
                feedbackTr.Visible = true;
                feedbackLabel.Text = feedback.ToString();
            }

            nomePrincipalLabel.Text = ((SPListItem)SPContext.Current.Item).ObterValor("Title");
        }

        private void DefinirModo()
        {
            var statusWorkflow = 
                SPContext.Current.Item["StatusWorkflow"] == null ? string.Empty : SPContext.Current.Item["StatusWorkflow"].ToString();
            
            if (Request.QueryString.AllKeys.Any(x => x == "ID"))
            {
                if (!Request.QueryString.AllKeys.Any(x => x == "Source") &&
                    (string.IsNullOrEmpty(statusWorkflow) || statusWorkflow.Equals("Salvo")))
                {
                    AtribuirModo(SPControlMode.Edit);
                }
                else
                {
                    AtribuirModo(SPControlMode.Display);
                    OcultarCamposDeEdicao();
                }

                CarregarControladores();
            }
            else
            {
                AtribuirModo(SPControlMode.New);
            }
        }

        private void OcultarCamposDeEdicao()
        {
            buscarNome.Visible = false;

            adicionarParticipante.Visible = false;
            adicionarOutraArea.Visible = false;
            submeterButton.Visible = false;
            salvarButton.Visible = false;

            gridParticipantes.Columns[2].Visible = false;
            gridOutrasAreas.Columns[2].Visible = false;

            SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
            ribbon.TrimById("Ribbon.ListForm.Edit.Actions.AttachFile");
        }

        private void AtribuirModo(SPControlMode modo)
        {
            nomePrincipal.ControlMode = modo;
            ff1description1.ControlMode = modo;

            crachaPrincipal.ControlMode = modo;
            ff2description1.ControlMode = modo;

            ff41.ControlMode = modo;
            ff4description1.ControlMode = modo;

            ff51.ControlMode = modo;
            ff5description1.ControlMode = modo;

            ff61.ControlMode = modo;
            ff6description1.ControlMode = modo;

            ff71.ControlMode = modo;
            ff7description1.ControlMode = modo;

            ff81.ControlMode = modo;
            ff8description1.ControlMode = modo;

            ff91.ControlMode = modo;
            ff9description1.ControlMode = modo;

            ff101.ControlMode = modo;
            ff10description1.ControlMode = modo;

            ff121.ControlMode = modo;
            ff12description1.ControlMode = modo;

            ff131.ControlMode = modo;
            ff13description1.ControlMode = modo;

            AttachmentsField.ControlMode = modo;

            attachmentUpload.ControlMode = modo;
            itemHiddenVersion.ControlMode = modo;

            scriptsPanel.Visible = modo != SPControlMode.Display;
        }

        private void CarregarControladores()
        {
            controladorParticipantes.Carregar(SPContext.Current.Item);
            controladorOutrasAreas.Carregar(SPContext.Current.Item);
        }

        private void ConfigurarControladores()
        {
            var controladores = new List<ControladorSPGrid>();

            controladorParticipantes = new ControladorSPGrid(Participantes, gridParticipantes, "Cracha");
            controladorParticipantes.Coluna = "Participantes";
            controladores.Add(controladorParticipantes);
            
            controladorOutrasAreas = new ControladorSPGrid(OutrasAreas, gridOutrasAreas, "ID");
            controladorOutrasAreas.Coluna = "AplicavelOutraArea";
            controladores.Add(controladorOutrasAreas);

            submeterButton.Controladores = controladores;
            salvarButton.Controladores = controladores;
        }


        protected void gridParticipantes_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
        {
            controladorParticipantes.Excluir(e.CommandArgument.ToString());
        }

        protected void novoParticipante_Click(object sender, EventArgs e)
        {
            var nomeParticipante = ObterParticipante(crachaTextBox.Text);

            if (nomeParticipante == null)
                return;

            controladorParticipantes.Incluir(
                crachaTextBox.Text,
                new object[] { nomeParticipante["NomeCompleto"] });

            crachaTextBox.Text = string.Empty;
        }

        private SPListItem ObterParticipante(string cracha)
        {
            int numeroCracha = 0;

            if (int.TryParse(cracha, out numeroCracha))
            {
                return ObterItem("LMF" + cracha.ToString().PadLeft(6, '0'), "Pessoas", "Title");
            }

            return null;
        }



        protected void buscarNome_Click(object sender, EventArgs e)
        {
            var nomeParticipante = ObterParticipante(crachaPrincipal.Value.ToString());

            if (nomeParticipante != null)
            {
                nomePrincipal.Value = nomeParticipante["NomeCompleto"];
                nomePrincipalLabel.Text = nomeParticipante["NomeCompleto"].ToString();
            }
            else
            {
                nomePrincipalLabel.Text = "Colaborador não encontrado.";
                nomePrincipal.Value = string.Empty;
            }
        }



        protected void gridOutrasAreas_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
        {
            controladorOutrasAreas.Excluir(e.CommandArgument.ToString());
        }

        protected void novaArea_Click(object sender, EventArgs e)
        {
            controladorOutrasAreas.Incluir(
                setorAplicavel.Value.ToString(),
                new object[] { 
                    ObterItemPorId(segmentoAplicavel.Value.ToString(), "Segmento")["Title"],
                    ObterItemPorId(setorAplicavel.Value.ToString(), "Setor")["Title"] });
        }

        private SPListItem ObterItemPorId(string valor, string nomeLista)
        {
            return ObterItem(valor, nomeLista, "ID");
        }


        private SPListItem ObterItem(string valor, string nomeLista, string field)
        {
            var lista = SPContext.Current.Web.Lists[nomeLista];
            SPQuery query = new SPQuery();
            query.Query = "<Where><Eq><FieldRef Name=\"" + field + "\" /><Value Type=\"Text\">" + valor + "</Value></Eq></Where>";

            SPListItemCollection coll = lista.GetItems(query);

            if (coll.Count > 0)
                return coll[0];
            else
                return null;
        }


        private void ConfigurarDataTables()
        {
            Participantes = new DataTable();
            Participantes.Columns.Add("Cracha", typeof(string));
            Participantes.Columns.Add("Nome", typeof(string));

            OutrasAreas = new DataTable();
            OutrasAreas.Columns.Add("ID", typeof(string));
            OutrasAreas.Columns.Add("Segmento", typeof(string));
            OutrasAreas.Columns.Add("Setor", typeof(string));
        }
    }
}
