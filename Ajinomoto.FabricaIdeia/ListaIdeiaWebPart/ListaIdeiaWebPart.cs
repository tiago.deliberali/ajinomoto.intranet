﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace Ajinomoto.FabricaIdeia.ListaIdeiaWebPart
{
    [ToolboxItemAttribute(false)]
    public class ListaIdeiaWebPart : WebPart
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            SPWeb web = SPContext.Current.Web;
            SPGroup pia = web.SiteGroups["PIA"];

            if (!web.IsCurrentUserMemberOfGroup(pia.ID))
                throw new Exception("Acesso negado. Apenas membros do PIA tem acesso a essa View.");
        }
    }
}
