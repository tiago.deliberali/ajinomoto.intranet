﻿//Validações de formulário usando javascript

///////////////////////////////////////////////////////////////////////
//  PERMITE TRABALHAR COM EVENTOS ONSUBMIT DO FORMULÁRIO NO ASP.NET
///////////////////////////////////////////////////////////////////////
addToPostBack = function (func) {
    var old__doPostBack = __doPostBack;
    if (typeof __doPostBack != 'function') {
        __doPostBack = func;
    } else {
        __doPostBack = function (t, a) {
            if (func(t, a)) old__doPostBack(t, a);
        }
    }
};


///////////////////////////////////////////////////////////////////////
//  VERIFICA SE FOI PREENCHIDO
///////////////////////////////////////////////////////////////////////
function VerificarPreenchimento(item, nome) {
    if (item.is(":visible") && item.find("input").val() == "") {
        item.addClass("erro-item");
        return "Favor preencher o campo '" + nome + "'\n";
    }
    else {
        item.removeClass("erro-item");
        return "";
    }
}

function VerificarSelecao(item, nome) {
    if (item.is(":visible") && ObterSelecao(item) == undefined) {
        item.addClass("erro-item");
        return "Favor preencher o campo '" + nome + "'\n";
    }
    else {
        item.removeClass("erro-item");
        return "";
    }
}

function ObterSelecao(item) {
    return $("label[for=" + item.find(":checked").attr("id") + "]").html();
}

function VerificarTextArea(item, nome) {
    if (item.is(":visible") && item.find("textarea").val() == "") {
        item.addClass("erro-item");
        return "Favor preencher o campo '" + nome + "'\n";
    }
    else {
        item.removeClass("erro-item");
        return "";
    }
}

function VerificarDropDown(item, nome) {
    if (item.is(":visible") && item.find("select").val() == 0) {
        item.addClass("erro-item");
        return "Favor selecionar um valor na lista '" + nome + "'\n";
    }
    else {
        item.removeClass("erro-item");
        return "";
    }
}



///////////////////////////////////////////////////////////////////////
//  OBTEM ITEMS
///////////////////////////////////////////////////////////////////////
function ObterLinha(textosTraduzidos) {
    var traducoes = textosTraduzidos.split(';');

    for (var i in traducoes) {
        var texto = traducoes[i];
        var item = ObterTextoEmLinha(texto).parent('h3').parent('td').parent('tr');

        if (item.length == 0)
            item = $("h3:contains('" + texto + "')").parent('td').parent('tr');

        if (item.length > 0) // encontrou a tradução na página
            return item;
    }

    return null;
}

function ObterTextoEmLinha(texto) {
    return $("nobr:contains('" + texto + "')");
}

function AlterarTextoEmLinha(textoOriginal, novoTexto) {
    ObterTextoEmLinha(textoOriginal).html(novoTexto);
}




function ObterOpcao(texto) {
    return ObterTextoOpcao(texto).parent('td').parent('tr');
}

function ObterTextoOpcao(textosTraduzidos) {
    var traducoes = textosTraduzidos.split(';');

    for (var i in traducoes) {
        var texto = traducoes[i];
        var item = $("span[title='" + texto + "']");

        if (item.length > 0)
            return item;
    }

    return null;
}

function AlterarTextoOpcao(textoOriginal, novoTexto) {
    ObterTextoOpcao(textoOriginal).children("label").html(novoTexto);
}




function ObterLinhaEmTabela(texto) {
    return $("td[class='ms-formlabel']:contains('" + texto + "')").parent('tr');
}

function ObterInformacaoEmTabela(texto) {
    return $("td[class='ms-descriptiontext']:contains('" + texto + "')").parent('tr');
}

function ObterInformacaoCriacaoAtualizacao(texto) {
    return $("span:contains('" + texto + "')").parent('td').parent('tr');
}


///////////////////////////////////////////////////////////////////////
//  VERIFICA SE POSSUI VALOR ESPECIFICO
///////////////////////////////////////////////////////////////////////
function SelecaoContemOpcao(item, opcao) {
    var opcoes = opcao.split(';');
    var valorItem = ObterSelecao(item);
    var resultado = false;

    for (var i in opcoes) {
        resultado = resultado || (valorItem == opcoes[i]);
    }

    return resultado;
}

function PossuiDescricao() {
    var textoDescricao = descricao.find("textarea").val();

    return textoDescricao != "" && textoDescricao != undefined;
}