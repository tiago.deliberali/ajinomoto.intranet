/*
 * SolidQ Filtered Lookup Columns for Cascading Dropdowns in SharePoint 2010 
 * Version 2.0
 * Author: Jos� Quinto Zamora (@jquintozamora)
 * http://blogs.solidq.com/sharepoint
 * http://blogs.solidq.com/powerpivot
 * http://www.solidq.com
 * Copyright (c) 2011 SolidQ Global
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/mit-license.php
 */
 
		
var debug = true;

/***********************************
	SolidQ SP Lookup Field Class
 ***********************************/
function SolidQLookupField(listGuid, columnName)
{
	//NOTE:
	//In SharePoint, when you put a Single Lookup Column in a List, then this column may be represented or rendered
	//in two ways:
	//Type D:
	//	- If lookup list have less than 20 items
	//	- HTML generated is <select> that stores list of options with "Name" and "ID" of LookupField
	//
	//Type I:
	//	- If lookup list have 20 or more items
	//	- HTML generated is:
	//		- <input> that stores: choices (list of all pairs "Name", "ID"), optHid (id of hidden input that stores Selected Lookup ID), value (stores Selected Lookup Name)
	//		- Hidden <input>: stores selected id and is referenced by optHid
	//		- <img>: represents dropdown arrow
	//		- Dinamically generated <select>
	
	//GUID of the lookup list for this column
	this.listGuid = listGuid;	
	this.getListGuid  = function()
	{
		return this.listGuid;
	};

	//Name of the column in Form 
	//Remarks: Notice that may be in multiLanguage, make sure that pass
	//this parameter in correct language
	this.columnName = columnName;
	this.getColumnName  = function()
	{
		return this.columnName;
	};

	//Instance of <select> field when fieldMode is D
	this.selectField = null;
	
	//Instance of <input> field when fieldMode is I
	this.inputField = null;
	this.getInputFieldValue = function ()
	{
		if (this.inputField != null)
		{
			return this.inputField.value;
		}
		else
		{
			if (debug)
				LogError("SolidQLookupField.getInputFieldValue", "columnName: inputField", "inputField is null.");
			return null;
		}
	};
	
	//Instance of hidden <input> (ID) when fieldMode is I
	this.inputIDField = null;
	this.getInputIDFieldValue = function ()
	{
		if (this.inputIDField != null)
		{
			return this.inputIDField.value;
		}
		else if (this.inputField != null)
		{
			this.inputIDField = document.getElementById(this.inputField.optHid);
			if (this.inputIDField != null)
			{
				return this.inputIDField.value;
			}
			else
			{
				if (debug)
					LogError("SolidQLookupField.getInputIDFieldValue", "columnName: inputIDField", "inputIDField is null.");
				return 0;
			}
		}
		else
		{
			if (debug)
				LogError("SolidQLookupField.getInputIDFieldValue", "columnName: inputField", "inputField is null.");
			return 0;
		}	
	};
	//Variable to track Input changes values in onpropertychange method when fieldMode = I
	this.oldInputFieldValue = '';
	
	//Variable to track Lock in onpropertychange event, when fieldMode = I
	this.inputFieldLock = false;
	
	//fieldMode have three posible values: 
	//'' --> Undefined
	//D  --> DropDown (When have less than 20 items)
	//I  --> Input (When have 20 or more items
	this.fieldMode = '';
	this.getFieldMode = function()
	{
		return this.fieldMode;
	};
	
	
	//Variable that stores 0 if there aren't field selected on load (None is considered as 0), 
	//otherwise, stores the ID from selected item
	this.selectedID = 0;
	this.generateSelectedID = function()
	{
		if (this.fieldMode == 'D')
		{
			//Proyecto tiene menos de 20 items
			var indexSel = this.selectField.selectedIndex;
			//if (debug)
			//	alert(this.getColumnName() + ',' + indexSel);
			var sel = this.selectField.options[indexSel];
			//alert('indexSel: ' + indexSel + ', sel: ' + sel.value);
			this.selectedID = sel.value;
			//alert('selectedID: ' + this.selectedID);
			return this.selectedID;
		}
		else if (this.fieldMode == 'I')
		{
			//Proyecto tiene mas de 20 items --> Input / Select
			this.selectedID = this.getInputIDFieldValue();
			return this.selectedID;
		}
	}
	
	
	//SElected value
	this.selectedValue = '';
	this.generateSelectedValue = function()
	{
		if (this.fieldMode == 'D')
		{
			//Proyecto tiene menos de 20 items
			var indexSel = this.selectField.selectedIndex;
			//if (debug)
			//	alert(this.getColumnName() + ',' + indexSel);
			var sel = this.selectField.options[indexSel];
			//alert('indexSel: ' + indexSel + ', sel: ' + sel.value);
			this.selectedValue = sel.text;
			//alert('selectedID: ' + this.selectedID);
			return this.selectedValue;
		}
		else if (this.fieldMode == 'I')
		{
			//Proyecto tiene mas de 20 items --> Input / Select
			this.selectedValue= this.inputField.value;
			return this.selectedValue;
		}
	}

	
	this.Init();
};


SolidQLookupField.prototype.Init = function ()
{
	//LogError("SolidQLookupField.Init", "columnName: " + this.columnName, "Error getting column name");
					
	this.selectField = getTagFromIdentifierAndTitle("SELECT","Lookup",this.columnName);
	if (this.selectField != null)
	{
		//fieldMode = Dropdown
		this.fieldMode = 'D';
		
		//Stablish Initial selected value
		this.generateSelectedID();
	}
	else
	{
		this.inputField = getTagFromIdentifierAndTitle("INPUT","",this.columnName);
		if (this.inputField != null)
		{
			//fieldMode = Input
			this.fieldMode = 'I';
			this.getInputIDFieldValue();
						
			//track value status
			this.oldInputFieldValue = this.inputField.value;
			
			//Stablish Initial selected value
			this.generateSelectedID();

		}
		else
		{
			this.fieldMode = '';
			LogError("SolidQLookupField.Init", "columnName: " + this.columnName, "Error getting field object, fieldMode is Undefined.");
		}
	}
};

SolidQLookupField.prototype.AttachChangeEvent = function(cascadeObject)
{
	var eventString = '';
	var thisclass = this;
	//alert("cascade" + cascadeObject.parentField.getColumnName() + ', ' + cascadeObject.childField.getColumnName());
	var func = null;
	if (this.fieldMode == 'D')
	{
		/*func = function() { thisclass.OnInputFieldChangeDropDown(cascadeObject); };
		eventString = 'change';
		//Al compatible browsers
		if (this.selectField.addEventListener)
		{
			this.selectField.addEventListener(eventString, func, false);
		} 
		//IE
		else if (this.selectField.attachEvent)
		{ 
			this.selectField.attachEvent('on' + eventString ,func);
		}
		*/
		
		func = function() { thisclass.OnInputFieldChangeDropDown(thisclass,cascadeObject); };
		eventString = 'propertychange';
		//Al compatible browsers
		if (this.selectField.addEventListener)
		{
			this.selectField.addEventListener(eventString, func, false);
		} 
		//IE
		else if (this.selectField.attachEvent)
		{ 
			this.selectField.attachEvent('on' + eventString ,func);
		}


	}
	else if (this.fieldMode == 'I')
	{
		func = function() { thisclass.OnInputFieldChangeInput(thisclass,cascadeObject); };
		eventString = 'propertychange';
		//Al compatible browsers
		if (this.inputField.addEventListener)
		{
			this.inputField.addEventListener(eventString, func, false);
		} 
		//IE
		else if (this.inputField.attachEvent)
		{ 
			this.inputField.attachEvent('on' + eventString ,func);
		}

	}
	else
	{
		if (debug)
			LogError("SolidQLookupField.AttachChangeEvent", "columnName: " + this.columnName, "Error setting events, fieldMode is Undefined.");
		return;
	}
	

};

SolidQLookupField.prototype.RemoveValues = function (typeRemove)
{
	//typeRemove
	//1: Selected Value is none --> in type I don't change input value
	//2: No eliminar el None
	if (this.fieldMode == 'D')
	{	
		if (typeRemove == 1)
		{
			for (i = this.selectField.length - 1; i>=0; i--) 
			{
				//if (!this.selectField.options[i].selected) 
				if (this.selectField.options[i].value != 0)
				{
					this.selectField.remove(i);
				}
			}
		}
		else if (typeRemove == 2)
		{
			for (i = this.selectField.length - 1; i>=0; i--) 
			{
				//if (!this.selectField.options[i].selected) 
				if (this.selectField.options[i].value != 0)
				{
					this.selectField.remove(i);
				}
			}
		}
		else if (typeRemove == 3)
		{
			for (i = this.selectField.length - 1; i>=0; i--) 
			{
				//if (!this.selectField.options[i].selected) 
				if ((this.selectField.options[i].value != 0) && (!this.selectField.options[i].selected))
				{
					this.selectField.remove(i);
				}
			}
		}
		else
		{
			//1. Remove All Items
			//alert(proyecto);
			this.selectField.options.length = 0;
			
			//2. Add None element
			var optNone = document.createElement('option');     
			optNone.value = 0;
			optNone .innerHTML = "(None)";
			this.selectField.appendChild(optNone);
		}
	}
	else if (this.fieldMode == 'I')
	{
		//value is None
		if (typeRemove == 1)
		{
			this.inputField.choices = '(None)|0';
		}
		else if (typeRemove == 3)
		{
			this.inputField.choices = '(None)|0';
			if (this.inputIDField.value != 0)
			{
				this.inputField.choices += '|' + this.inputField.value + '|' + this.inputIDField.value;
			}
		}
		else
		{
			this.inputIDField.value = 0;
			this.inputField.choices = '(None)|0';
			this.inputField.value = '(None)';
			this.generateSelectedID();
		}
		/*if (debug)
			alert(	'RemoveValues: \n' +
					'Type : ' + typeRemove + '\n' +
					'Choices: ' + this.inputField.choices + '\n' + 
					'Value: ' + this.inputField.value + '\n' +
					'Id: ' + this.inputIDField.value + '\n');
		*/

	}
	else
	{
		if (debug)
			LogError("SolidQLookupField.RemoveValues", "columnName: " + this.columnName, "Error removing values, fieldMode is Undefined.");
		return;
	}
	
};

SolidQLookupField.prototype.AddItemCheckingSelected = function (id, title, selected)
{
	if (this.fieldMode == 'D')
	{	
		var opt = document.createElement('option');     
		opt.value = id;
		//Modificado el 29-03-2011 y remodificado el 14/5/2011
		if (selected)
		{
			opt.selected = true;
		}
		opt.innerHTML = title;
		this.selectField.appendChild(opt);
		if (selected)
		{
			this.generateSelectedID();
		}
	}
	else if (this.fieldMode == 'I')
	{
		//this.inputField.choices = '(None)|0';
		//alert('To be Implemented');
		var newTitle = title;
		if (title.indexOf("|") != -1)
		{
			newTitle = '';
			var mySplitResult = title.split('|');  
			for(i = 0; i < mySplitResult.length; i++)
			{     
				if (mySplitResult[i] != '')
					newTitle += '||' + mySplitResult[i];
			} 
			//alert('Title: ' + title + ', NewTitle: ' + newTitle);
		}
		//alert(newTitle);
		/*if (debug)
			alert(	'Antes \n' +
					'Choices: ' + this.inputField.choices + '\n' + 
					'Title: ' + newTitle + '\n' +
					'Id: ' + id + '\n' +
					'Selected: ' + selected + '\n');
		*/
		var cad = '|' + newTitle + '|' + id;
		this.inputField.choices = this.inputField.choices + cad;
		if (selected)
		{
			this.inputField.value = newTitle;
			this.inputIDField.value = id;
			this.generateSelectedID();
		}
		/*if (debug)
			alert(	'Despues \n' +
					'Choices: ' + this.inputField.choices + '\n' + 
					'Value: ' + this.inputField.value + '\n' +
					'Id: ' + this.inputIDField.value + '\n');
		*/
	}
	else
	{
		if (debug)
			LogError("SolidQLookupField.RemoveValues", "columnName: " + this.columnName, "Error removing values, fieldMode is Undefined.");
		return;
	}
};

SolidQLookupField.prototype.GetAllIDs = function ()
{
	//alert('Ento a GetAllIDs');
	var values = [];  
	if (this.fieldMode == 'D')
	{	 
		for(i = 0; i < this.selectField.length; i++)
		{     
			values.push(this.selectField.options[i].value);
		} 
	}
	else if (this.fieldMode == 'I')
	{
		var vals = this.inputField.choices.split("|");
		for (i=0; i < vals.length; i=i+2)
		{
			values.push(vals[i+1]);
		}
	}
	else
	{
		if (debug)
			LogError("SolidQLookupField.RemoveValues", "columnName: " + this.columnName, "Error removing values, fieldMode is Undefined.");
		return;
	}
	//alert('salgo de GetAllIDs');
	return values;
};

SolidQLookupField.prototype.DeleteItems = function(ids)
{
	if (this.fieldMode == 'D')
	{	
		//Delete all ids passed
		
		for (i = this.selectField.length - 1; i>=0; i--) 
		{
			var elem = this.selectField.options[i].value;
			for(b = 0; b < ids.length; b++)
			{
			    if (elem == ids[b]) 
			    {
			    	//alert('removing: ' + elem + ', at position: ' + i);
			     	this.selectField.remove(i);
			    }
		    }
		}
	}
	else if (this.fieldMode == 'I')
	{
		var choices = this.inputField.choices.split('|');
		for (i = choices.length - 1; i>=0; i--) 
		{
			var elem = choices[i];
			for(b = 0; b < ids.length; b++)
			{
			    if (elem == ids[b]) 
			    {
			    	choices.splice(i-1,2);
			    }
		    }
		}
		var cadChoices = choices[0];
		for(b = 1; b < choices.length; b++)
		{
			cadChoices += '|' + choices[b];
		}
		this.inputField.choices = cadChoices;
		
		//if (selected != -1)
		//{
		//	this.inputIDField.value = id;
		//	this.inputField.value = title;
		//}

	}
	else
	{
		if (debug)
			LogError("SolidQLookupField.RemoveValues", "columnName: " + this.columnName, "Error removing values, fieldMode is Undefined.");
		return;
	}
};




/**************
	Event Methods
	Remarks: In event methods we can't invoke this.* properties, because it is running in anohter execution context. 
			Instead we can use event.srcElement and we can pass through parameter current object too
****************/
SolidQLookupField.prototype.OnInputFieldChangeDropDown = function(lookupObj,cascadeObject)
{
	//if (debug)
	//	alert('event.propertyName: ' + event.propertyName);
	var chosenoption = event.srcElement.options[event.srcElement.selectedIndex];
	if (chosenoption != null)
	{
		//alert('Se lanza esto con propiedad: ' + event.propertyName);
		//if (event.propertyName == 'value')
		if (event.propertyName == 'value')
		{
			var newValue = chosenoption.value;
			var oldValue = lookupObj.oldInputFieldValue;
			//if (debug)
			//	alert('Old Client Value: ' + oldValue + ', New Client Value: ' + event.srcElement.value);
			if (oldValue != newValue)
			{
				
				if (lookupObj.inputFieldLock == false)
				{
					//Locking client access, not forgive to unlock in OnClientChange async response
					lookupObj.inputFieldLock = true;
					
					//if (debug)
					//		alert('Entro en Filter del proertychanged con childSelected: ' + lookupObj.generateSelectedID());
							
					if (lookupObj.generateSelectedID() != 0)
					{
						//typeRemove = 0, in order to change selected value
						cascadeObject.FilterListByFieldID(0);	
					}
					else
					{
						//removes all options except None (value = 0)
						cascadeObject.childField.RemoveValues(2);
					}
						
					
					//Assign new oldValue
					lookupObj.oldInputFieldValue = newValue;

					//No se desbloquea despues del aynch response del OnClientChangeGeneric porque si esa respuesta
					//no llega nunca el sistema queda inconsistente.
					lookupObj.inputFieldLock = false;
				}
			}	
		}
	}
	
};

SolidQLookupField.prototype.OnInputFieldChangeInput = function(lookupObj,cascadeObject)
{
	//if (debug)
	//	alert('event.propertyName: ' + event.propertyName);
	if (event.propertyName == 'value')
	{
		var newValue = event.srcElement.value;
		var oldValue = lookupObj.oldInputFieldValue;
		//if (debug)
		//	alert('Old Client Value: ' + oldValue + ', New Client Value: ' + event.srcElement.value);
		if (oldValue != newValue)
		{
			var clientChoices = event.srcElement.choices.split("|");
			var clientChoiceEncontrado = false;
			for (i=0; i < clientChoices.length; i=i+2)
			{
				//alert('newValue: ' + newValue + 'ClientChoice: ' + clientChoices[i] );
				if (newValue == clientChoices[i])
				{
					clientChoiceEncontrado = true;
					break;
				}
			}
			if (clientChoiceEncontrado)
			{
				if (lookupObj.inputFieldLock == false)
				{
					//Locking client access, not forgive to unlock in OnClientChange async response
					lookupObj.inputFieldLock = true;
					
					//if (debug)
					//		alert('Entro en Filter del proertychanged con childSelected: ' + lookupObj.selectedID);
							
					if (lookupObj.generateSelectedID() != 0)
					{
						//typeRemove = 0, in order to change selected value
						cascadeObject.FilterListByFieldID(0);	
					}
					else
					{
						cascadeObject.childField.RemoveValues(2);
					}
						
					
					//Assign new oldValue
					lookupObj.oldInputFieldValue = newValue;

					//No se desbloquea despues del aynch response del OnClientChangeGeneric porque si esa respuesta
					//no llega nunca el sistema queda inconsistente.
					lookupObj.inputFieldLock = false;
				}
			}
		}	
	}
};







/***********************************
	SolidQ Cascade Relationship Class
 ***********************************/
function SolidQCascadeRelationShip (parentField, childField, staticNameFieldInChildList)
{
	this.parentField = parentField;
	this.childField = childField;
	this.staticNameFieldInChildList = staticNameFieldInChildList;
	this.Init();
}
SolidQCascadeRelationShip.prototype.Init = function ()
{
	//Asignar a la parent list un manejador de eventos para que al cambiar su valor se actualize el del hijo
	this.parentField.AttachChangeEvent(this);
	
	//Filtrar la relacion
	//	Si el parent tiene un valor preseleccionado --> Filtrar el hijo por ese valor (si el hijo tenia un valor seleccionado debe mantenerlo)
	//	Si el parent no tiene valor --> Eliminar todos los items del hijo (Filtrar por None)
	//if (debug)
	//	alert('ParentField-SelectedID: ' + this.parentField.selectedID + '\n');
	var selectedParent = this.parentField.selectedID;
	var selectedChild = this.childField.selectedID;
	//Parent = None and Child = None
	if ((selectedParent == 0) && (selectedChild == 0))
	{
		//type 1: selected value is none
		this.childField.RemoveValues(1);
	}
	//Parent = X and Child = X
	else if ((selectedParent != 0) && (selectedChild != 0))
	{
		//Filter values don't modify selected in child
		this.FilterListByFieldID(3);
	}
	//Parent = X and Child = None
	else if ((selectedParent != 0) && (selectedChild == 0))
	{
		//Filter values don't modify selected in child
		this.FilterListByFieldID(1);
	}
	//Parent = None and Child = X
	else if ((selectedParent == 0) && (selectedChild != 0))
	{
		//Imposible
	}
	
		
	

}
SolidQCascadeRelationShip.prototype.FilterListByFieldID = function(typeRemove)
{
	var listGuid = this.childField.getListGuid();
	var parentLookupID = this.parentField.generateSelectedID();
	//Asumimos que el nombre est�tico de la columna es el mismo que el del Formulario
	var staticNameFieldInChildListAux = this.staticNameFieldInChildList; //this.parentField.getColumnName();
	//if (debug)
	//	alert('listGuid : ' + listGuid + ', ColumnName: ' + staticNameFieldInChildListAux + ', LookupID: ' + parentLookupID);
						
	if ((listGuid != null) && (parentLookupID != null) && (staticNameFieldInChildListAux != null))
	{
		try 
		{
			var clientContext = new SP.ClientContext.get_current();
			if (clientContext != undefined && clientContext != null) 
			{
				var web = clientContext.get_web();
		        var list = web.get_lists().getById(listGuid);
		        var query = '<View Scope=\'RecursiveAll\'>'+
								'<Query>'+
									'<Where>'+
										'<Eq>'+
											'<FieldRef Name=\'' + staticNameFieldInChildListAux + '\' LookupId=\'TRUE\'/>' +
											'<Value Type=\'Lookup\'>' + parentLookupID +'</Value>'+
										'</Eq>'+
									'</Where>'+
								'</Query>'+
							'</View>';
		        var camlQuery = new SP.CamlQuery();
		        camlQuery.set_viewXml(query);
		        this.productcollection = list.getItems(camlQuery);
		        clientContext.load(this.productcollection);
		        
		        
		        var thisclass = this;
		        var func =  function() { thisclass.processFilter(typeRemove); };
		        clientContext.executeQueryAsync(Function.createDelegate(this, func), Function.createDelegate(this, this.processError));
			} 
			else
			{
				LogError("SolidQCascadeRelationShip.FilterListByFieldID", "columnName: " + this.childField.getColumnName(),'No se ha podido conectar a SharePoint para obtener los proyectos.');
			}
		}
		catch (e) 
		{
			LogError("SolidQCascadeRelationShip.FilterListByFieldID", "columnName: " + this.childField.getColumnName(), 'Excepci�n. ' + e.message + '\n' + e.stackTrace);
	    }
	}
}

SolidQCascadeRelationShip.prototype.processFilter = function (typeRemove) 
{
	//alert(this.childField.getColumnName());
	try
	{
		/*var allValues = this.childField.GetAllIDs();
		//alert('Todos los IDs: ' + allValues);
		
		var filteredValues = [];
		
		
		var listItemEnumerator = this.productcollection.getEnumerator();   
		while (listItemEnumerator.moveNext()) 
		{  
			var oListItem = listItemEnumerator.get_current();
			var id = oListItem.get_item('ID');
			filteredValues.push(id);
		};
			
		var arrayDelete = [];
		var valuesToDeleteCount = 0;
		for (var i = 0; i < allValues.length; i++) 
		{
			var elem = allValues[i];
			var encontrado = false;
			for (var a = 0; a < filteredValues.length ; a++)
			{
				if (elem == filteredValues[a])
				{
					encontrado = true;
					break;
				}
			}
			if (encontrado == false)
			{
				if (elem != 0)
				{
					arrayDelete[valuesToDeleteCount] = elem;
					valuesToDeleteCount += 1;
				}
			}
		}
		
		if (debug)
		{
			alert('allValues: ' + allValues + '\n' + 'filteredValues : ' + filteredValues + '\n' + 'deleteValues: ' +  arrayDelete);
		}
		
		if (arrayDelete.length > 0)
		{
			this.childField.DeleteItems(arrayDelete);
		}*/
			

			
		// OLD CODE 
		//Antes de borrar todos los elementos del childField, nos guardamoe el selectedID para luego ponerlo otra vez como seleccionado
		var selectedID = this.childField.selectedID;
		/*if (debug)
		{
			alert(	'ANTES \n' +
					'ParentField: ' + this.parentField.getColumnName() + '\n' +
					'ParentFields: ' + this.parentField.GetAllIDs() + '\n' +
					'ParentField-SelectedID: ' + this.parentField.selectedID + '\n' +
					'ChildField: ' + this.childField.getColumnName() + '\n' +
					'ChildFields: ' + this.childField.GetAllIDs() + '\n' + 
					'ChilField-SelectedID: ' + selectedID  + '\n' );
		}*/
		
		this.childField.RemoveValues(typeRemove);

		var filteredValues = [];

		var listItemEnumerator = this.productcollection.getEnumerator();   
		while (listItemEnumerator.moveNext()) 
		{  
			var oListItem = listItemEnumerator.get_current();
			
			var id = oListItem.get_item('ID');
			var title = oListItem.get_item('Title');
			//if (debug)
			//	alert('id: ' + id + ', selectedID: ' + selectedID);
			if (id == selectedID)
			{
				if (typeRemove != 3)
				{
					this.childField.AddItemCheckingSelected(id, title, true);
				}
			}
			else
			{
				this.childField.AddItemCheckingSelected(id, title, false);
			}
			filteredValues.push(id);
		}
		// End Old Code
		/*if (debug)
		{
			alert(	'DESPUES \n' +
					'ParentField: ' + this.parentField.getColumnName() + '\n' +
					'ParentFields: ' + this.parentField.GetAllIDs() + '\n' +
					'ParentField-SelectedID: ' + this.parentField.selectedID + '\n' +
					'ChildField: ' + this.childField.getColumnName() + '\n' +
					'ChildField-FilteredValues: ' + filteredValues + '\n' +
					'ChildFields: ' + this.childField.GetAllIDs() + '\n' + 
					'ChilField-SelectedID: ' + selectedID  + '\n' );
		}*/

	}
	catch (e) 
	{
		LogError("SolidQCascadeRelationShip.processFilter", "columnName: " + this.childField.getColumnName(), 'Excepci�n. ' + e.message + '\n' + e.stackTrace);
    }
}

SolidQCascadeRelationShip.prototype.processError = function(sender, args) 
{
	LogError("SolidQCascadeRelationShip.processError", "columnName: " + this.childField.getColumnName(), 'Excepci�n. ' + args.get_message() + '\n' +  args.get_stackTrace());
}





function getTagFromIdentifierAndTitle(tagName, identifier, title) 
{
	var len = identifier.length;
	var tags = document.getElementsByTagName(tagName);
	for (var i=0; i < tags.length; i++) 
	{
		var tempString = tags[i].id;
		if ((tags[i].title == title) && ((identifier == '') || (tempString.indexOf(identifier) == tempString.length - len))) 
		{
			return tags[i];
		}
	}
	return null;
}
function getFirstTagFromIdentifier(tagName, identifier) 
{
	var len = identifier.length;
	var tags = document.getElementsByTagName(tagName);
	for (var i=0; i < tags.length; i++) 
	{
		var tempString = tags[i].id;
		if (tempString.indexOf(identifier) == tempString.length - len)
		{
			return tags[i];
		}
	}
	return null;
}

function getQueryStringParameter(ji) 
{
	hu = window.location.search.substring(1);
	gy = hu.split("&");
	for (i=0;i<gy.length;i++)
	{
		ft = gy[i].split("=");
		if (ft[0] == ji) 
		{
			return ft[1];
		}
	}
}
	
function LogError(func, param, msg) {
	var message = "Error in function " + func + "\n" +
		"Parameter: " + param + "\n" +
		"Message: " + msg + "\n";
		
	alert(message);
}
