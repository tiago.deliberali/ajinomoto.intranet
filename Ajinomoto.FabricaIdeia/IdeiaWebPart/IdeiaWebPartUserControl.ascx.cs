﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Collections.Generic;
using Ajinomoto.FabricaIdeia.Business;
using Microsoft.SharePoint.WebControls;
using System.Linq;

namespace Ajinomoto.FabricaIdeia.IdeiaWebPart
{
    public partial class IdeiaWebPartUserControl : UserControl
    {
        public IdeiaWebPart WebPart { get; set; }

        public string StatusWorkFlow { get; set; }

        public string AtribuidoA { get; set; }

        public string ModoEdicao { get; set; }

        public SPListItem IdeiaItem
        {
            get
            {
                var ideiaViewState = ViewState["IdeiaItem"];
                var tarefa = (SPListItem)SPContext.Current.Item;

                if (ideiaViewState == null)
                    ideiaViewState = ObterIdeia(tarefa);

                return (SPListItem)ideiaViewState;
            }
        }

        public SPListItem Tarefa
        {
            get
            {
                return SPContext.Current.Item as SPListItem;
            }
        }

        delegate string ProcessarLinha(string linha);



        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                StatusWorkFlow = Tarefa.ObterValor("StatusWorkflow");
                AtribuidoA = ObterAtribuicao();

                PreencherDetalhesIdeia();
                AtribuirModoEdicao();
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex);
            }
        }

        private void AtribuirModoEdicao()
        {
            SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
            ribbon.TrimById("Ribbon.ListForm.Edit.Actions.ClaimReleaseTask");
            ribbon.TrimById("Ribbon.ListForm.Edit.Actions.DeleteItem");

            if (Request.QueryString.AllKeys.Any(x => x == "ID"))
            {
                var status = Tarefa.ObterValor("Status");

                if (!Request.QueryString.AllKeys.Any(x => x == "Source")
                    && (status.Equals("Concluído") || status.Equals("Completed")))
                {
                    DesativarEdicao(ribbon);
                }
            }
            else
            {
                DesativarEdicao(ribbon);
            }
        }

        private void DesativarEdicao(SPRibbon ribbon)
        {
            ribbon.TrimById("Ribbon.ListForm.Edit.Actions.AttachFile");
            ribbon.TrimById("Ribbon.ListForm.Edit.Commit.Publish");

            ModoEdicao = "False";
        }

        private string ObterAtribuicao()
        {
            var valor = Tarefa.ObterValor("AssignedTo");

            if (string.IsNullOrEmpty(valor))
                return string.Empty;

            //ex: 12;#RH
            var dados = valor.Split('#');
                
            if (dados.Length > 1)
                valor = dados[1];

            if (valor.Contains("_"))
                valor = valor.Substring(0, valor.IndexOf('_'));

            return valor;
        }


        private void PreencherDetalhesIdeia()
        {
            segmentoSetorLabel.Text = 
                FormatarValor(ObterValorCampoIdeia("SegmentoImpactado")) + " / " 
                + FormatarValor(ObterValorCampoIdeia("SetorImpactado"));

            condicaoLabel.Text = ObterValorCampoIdeia("CondicaoAtual");

            propostaLabel.Text = ObterValorCampoIdeia("Proposta");
            beneficioLabel.Text = ObterValorCampoIdeia("BeneficioEsperado");
            investimentoLabel.Text = FormatarValor(ObterValorCampoIdeia("NecessitaInvestimento"));
            materiaisLabel.Text = ObterValorCampoIdeia("MateriaisEquipamentos");

            ApresentarAnexos();

            PreencherLista(objetivosRepeater, "Objetivo", ProcessarObjetivos);
            PreencherLista(outrasAreasRepeater, "AplicavelOutraArea", ProcessarOutrasAreas);

            if (Tarefa.ObterValor("StatusWorkflow").ToString().Equals("Decisao_Comite"))
            {
                NotasComitePanel.Visible = true;
                PreencherLista(notasRepeater, "NotasComite", ProcessarNotasComite);
            }
        }

        private string FormatarValor(string valor)
        {
            valor = valor.Replace("True", "Sim").Replace("False", "Não");

            if (valor.Contains("#"))
                valor = valor.Substring(valor.IndexOf('#') + 1);

            return valor;
        }

        private void ApresentarAnexos()
        {
            var anexos = new List<Anexo>();

            foreach (var item in IdeiaItem.Attachments)
            {
                anexos.Add(new Anexo()
                {
                    Nome = item.ToString(),
                    Url = IdeiaItem.Attachments.UrlPrefix + item
                });
            }

            anexosRepeater.DataSource = anexos;
            anexosRepeater.DataBind();
        }

        private void PreencherLista(Repeater repeater, string fieldName, ProcessarLinha processamento)
        {
            var objetivos = new List<string>();

            foreach (var item in ObterValorCampoIdeia(fieldName).Replace(";#", ":").Split(':'))
            {
                var itemProcessado = processamento(item);

                if (!string.IsNullOrEmpty(itemProcessado))
                    objetivos.Add(itemProcessado);
            }

            repeater.DataSource = objetivos;
            repeater.DataBind();
        }

        private string ObterValorCampoIdeia(string fieldName)
        {
            return IdeiaItem.ObterValor(fieldName);
        }

        

        private SPListItem ObterIdeia(SPListItem tarefa)
        {
            SPField ideiaIdField = tarefa.Fields.TryGetFieldByStaticName("IdeiaId");

            var ideiaIdValue = tarefa[ideiaIdField.Id];

            if (ideiaIdValue == null)
                return null;

            int ideiaId = 0;
            int.TryParse(ideiaIdValue.ToString(), out ideiaId);

            return tarefa.Web.Lists["Ideias"].GetItemById(ideiaId);
        }

        private string ProcessarObjetivos(string linha)
        {
            return linha;
        }

        private string ProcessarOutrasAreas(string linha)
        {
            if (string.IsNullOrEmpty(linha))
                return string.Empty;

            var dados = linha.Split(';');

            return dados[1] + " / " + dados[2];
        }

        private string ProcessarNotasComite(string linha)
        {
            if (string.IsNullOrEmpty(linha))
                return string.Empty;

            var dados = linha.Split(';');

            if (dados[5] == "Sim") // deve ignorar esse registro
                return string.Empty;
            else
                return linha;
        }
    }
}
