﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IdeiaWebPartUserControl.ascx.cs" Inherits="Ajinomoto.FabricaIdeia.IdeiaWebPart.IdeiaWebPartUserControl" %>

<script type="text/javascript" src="/_layouts/JS/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/_layouts/JS/Validacao.js"></script>

<script type="text/javascript">
    // opcoes
    var salvar;
    var aprovar;
    var reprovar;
    var maisInformacao;
    var solicitarReavaliacao;
    var implantado;
    var naoImplantado;

    // campos padrão da tarefas
    var atribuidaA;
    var dataInicio;
    var dataFim;
    var descricao;

    // campos opcionais
    var consultarCPD;
    var consultarEngenharia;
    var segmentoOpcional;
    var setorOpcional;
    var dataImplantacao;
    var reavaliacao;

    var risco;
    var relevancia;
    var foco;

    // campos customizados normalmente visiveis
    var acao;
    var observacoes;

    $(function () {
        LocalizarCampos();
        OcultarCampos();
        AlterarValorBotao();

        acao.find("input").change(function () {
            ExibirReavaliacao();
        });

        addToPostBack(function (t, a) {
            return ValidarFormulario();
        });

        //ObterLinha("IdeiaId").hide();

        switch ("<%= this.StatusWorkFlow %>") {

            case "Analise_Pia":
                acao.show();
                AlterarTextoOpcao("Aprovar", "Validar");

                consultarCPD.show();
                consultarEngenharia.show();
                segmentoOpcional.show();
                setorOpcional.show();
                break;

            case "Analise_AreaImpactada":
                consultarCPD.show();
                consultarEngenharia.show();
                ExibirPerguntasComite();
                break;

            case "Analise_Comite":
                ExibirPerguntasComite();
                break;

            case "Decisao_Comite":
                acao.show();
                solicitarReavaliacao.show();
                maisInformacao.hide();
                ExibirReavaliacao();
                break;

            case "Implantacao":
                acao.show();

                aprovar.hide();
                reprovar.hide();
                maisInformacao.hide();

                implantado.show();
                naoImplantado.show();
                dataImplantacao.show();
                break;

            default:
                break;
        }
    });

    function LocalizarCampos() {
        // opcoes
        aprovar = ObterOpcao("Aprovar");
        reprovar = ObterOpcao("Reprovar");
        maisInformacao = ObterOpcao("Solicitar mais informações");
        solicitarReavaliacao = ObterOpcao("Solicitar reavaliação");
        implantado = ObterOpcao("Implantado");
        naoImplantado = ObterOpcao("Não Implantado");

        // campos padrão da tarefas
        atribuidaA = ObterLinha("Atribuída a");
        dataInicio = ObterLinha("Data de Início");
        dataFim = ObterLinha("Data de Conclusão");
        descricao = ObterLinha("Descrição");

        // campos opcionais
        consultarCPD = ObterLinha("Consultar CPD");
        consultarEngenharia = ObterLinha("Consultar Engenharia");
        segmentoOpcional = ObterLinha("Segmento Opcional");
        setorOpcional = ObterLinha("Setor Opcional");
        dataImplantacao = ObterLinha("Data de Implantação");
        reavaliacao = ObterLinha("Selecionar áreas");

        risco = ObterLinha("Risco");
        relevancia = ObterLinha("Relevancia");
        foco = ObterLinha("Foco");

        // atuaiza o valor das perguntas
        AlterarTextoEmLinha("Risco", "Ideia apresenta riscos que a inviabilize?");
        AlterarTextoEmLinha("Relevancia", "O problema apresentado pela ideia tem correlação com as estratégias e prioridades do setor?");
        AlterarTextoEmLinha("Foco", "A ideia contribui para a solução do problema apresentado?");

        // campos customizados normalmente visiveis
        acao = ObterLinha("Ação");
        observacoes = ObterLinha("Observações");
    }

    function ExibirPerguntasComite() {
        var atribuicao = "<%= this.AtribuidoA %>";

        switch (atribuicao) {
            case "QA":
            case "RH":
            case "Seguranca":
            case "MA":
                risco.show();
                break;

            case "CPD":
            case "Engenharia":
                risco.show();
                foco.show();
                break;

            default:
                risco.show();
                foco.show();
                relevancia.show();
                break;
        }
    }

    function OcultarCampos() {
        // campos não editáveis
        //ObterLinha("Título").attr("disabled", true);
        ObterLinha("Status").attr("disabled", true);

        ObterLinhaEmTabela("Content Type").hide();
        ObterInformacaoCriacaoAtualizacao("Created at").hide();
        ObterInformacaoCriacaoAtualizacao("Last modified").hide();
        ObterInformacaoEmTabela("Version").hide();
        
        ObterLinhaEmTabela("Tipo de Conteúdo").hide();
        ObterInformacaoCriacaoAtualizacao("Criado").hide();
        ObterInformacaoCriacaoAtualizacao("Modificado pela").hide();
        ObterInformacaoEmTabela("Versão").hide();

        ObterLinha("TopicosAvaliacao").hide();
        ObterLinha("PontosTopicos").hide();
        ObterLinha("% de Conclusão").hide();
        ObterLinha("Predecessoras").hide();
        ObterLinha("Prioridade").hide();
        ObterLinha("Grupo de Tarefas").hide();
        ObterLinha("Workflow Name").hide();
        ObterLinha("Corpo").hide();
        ObterLinha("Status Workflow").hide();

        consultarEngenharia.hide();
        consultarCPD.hide();
        segmentoOpcional.hide();
        setorOpcional.hide();
        dataImplantacao.hide();
        reavaliacao.hide();
        acao.hide();

        implantado.hide();
        naoImplantado.hide();
        solicitarReavaliacao.hide();

        risco.hide();
        relevancia.hide();
        foco.hide();

        atribuidaA.hide();
    }

    function ExibirReavaliacao() {
        if (ObterSelecao(acao) == "Solicitar reavaliação")
            reavaliacao.show();
        else
            reavaliacao.hide();
    }

    function AlterarValorBotao() {
        var modoEdicao = '<%= this.ModoEdicao %>';

        $("input").each(function () {
            if ($(this).attr("value") == "Save" || $(this).attr("value") == "Salvar") {
                if (modoEdicao == 'False')
                    $(this).remove();
                else
                    $(this).attr("value", "Concluir");
            }
        });

        $("span[class=ms-cui-ctl-largelabel]:contains('Save')").html("Concluir");
        $("span[class=ms-cui-ctl-largelabel]:contains('Salvar')").html("Concluir");

        // original: left: -256px; top: -416px;
        $("span[class=ms-cui-ctl-largelabel]:contains('Concluir')").parent().find("img").css("left", "-256").css("top", "-128px");
    }

    function ValidarFormulario() {
        var mensagem = "";

        // campos de preenchimento obrigatorio
        mensagem += VerificarSelecao(consultarEngenharia, "Consultar Engenharia");
        mensagem += VerificarSelecao(consultarCPD, "Consultar CPD");
        mensagem += VerificarSelecao(reavaliacao, "Selecionar áreas");
        mensagem += VerificarPreenchimento(dataImplantacao, "Data de Implantação");

        // relação entre descricao e resposta
        descricao.removeClass("erro-item");

        mensagem += VerificarJustificativa(acao, "Solicitar mais informações;Não Implantado;Reprovar", "Ação");

        mensagem += VerificarJustificativa(risco, "Sim", "A ideia contribui para a solução...");
        mensagem += VerificarJustificativa(relevancia, "Não", "O problema apresentado...");
        mensagem += VerificarJustificativa(foco, "Não", "Ideia apresenta riscos...");

        if (mensagem != "") {
            alert(mensagem);

            return false;
        }

        return true;
    }

    function VerificarJustificativa(item, opcao, nome) {
        var selecao = VerificarSelecao(item, nome);

        if(selecao != "")
            return selecao;

        if (item.is(":visible") && !PossuiDescricao() && SelecaoContemOpcao(item, opcao)) {
            item.addClass("erro-item");
            descricao.addClass("erro-item");

            return "Favor preencher a justificativa pela sua escolha em '" + nome + "'\n";
        }
        else {
            item.removeClass("erro-item");
            return "";
        }
    }
</script>
<style type="text/css">
    .erro-item 
    {
        background-color: #ff9494;
    }
</style>
<asp:Panel runat="server" ID="NotasComitePanel" Visible="false">
    <h2>Notas do Comitê</h2>
    <table border="0" cellspacing="0" width="100%">
	    <tr>
		    <td width="190px" valign="top" class="ms-formlabel">
			    <asp:Repeater runat="server" ID="notasRepeater">
                    <HeaderTemplate>
                        <table width="100%">
                            <tr>
                                <td>Grupo</td>
                                <td>Risco</td>
                                <td>Foco</td>
                                <td>Relevância</td>
                                <td>Comentários</td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="ms-formlabel"><%# Container.DataItem.ToString().Split(';')[0] %></td>
                            <td class="ms-formbody"><%# Container.DataItem.ToString().Split(';')[1] %></td>
                            <td class="ms-formbody"><%# Container.DataItem.ToString().Split(';')[2] %></td>
                            <td class="ms-formbody"><%# Container.DataItem.ToString().Split(';')[3] %></td>
                            <td class="ms-formbody"><%# Container.DataItem.ToString().Split(';')[4] %></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
		    </td>
	    </tr>
    </table>
</asp:Panel>
<h2>Dados da ideia</h2>
<table border="0" cellspacing="0" width="100%">
	<tr>
		<td width="190px" valign="top" class="ms-formlabel">
			<H3 class="ms-standardheader">
				<nobr>Objetivo</nobr>
			</H3>
		</td>
		<td width="400px" valign="top" class="ms-formbody">
			<asp:Repeater runat="server" ID="objetivosRepeater">
                <HeaderTemplate><ul></HeaderTemplate>
                <ItemTemplate><li><%# Container.DataItem %></li></ItemTemplate>
                <FooterTemplate></ul></FooterTemplate>
            </asp:Repeater>
		</td>
	</tr>
    <tr>
		<td width="190px" valign="top" class="ms-formlabel">
			<H3 class="ms-standardheader">
				<nobr>Segmento / Setor impactado</nobr>
			</H3>
		</td>
		<td width="400px" valign="top" class="ms-formbody">
			<asp:Label runat="server" ID="segmentoSetorLabel" />
		</td>
	</tr>
    <tr>
		<td width="190px" valign="top" class="ms-formlabel">
			<H3 class="ms-standardheader">
				<nobr>Condição atual</nobr>
			</H3>
		</td>
		<td width="400px" valign="top" class="ms-formbody">
			<asp:Label runat="server" ID="condicaoLabel" />
		</td>
	</tr>
    <tr>
		<td width="190px" valign="top" class="ms-formlabel">
			<H3 class="ms-standardheader">
				<nobr>Qual a proposta?</nobr>
			</H3>
		</td>
		<td width="400px" valign="top" class="ms-formbody">
			<asp:Label runat="server" ID="propostaLabel" />
		</td>
	</tr>
    <tr>
		<td width="190px" valign="top" class="ms-formlabel">
			<H3 class="ms-standardheader">
				<nobr>Qual o benefício esperado?</nobr>
			</H3>
		</td>
		<td width="400px" valign="top" class="ms-formbody">
			<asp:Label runat="server" ID="beneficioLabel" />
		</td>
	</tr>
    <tr>
		<td width="190px" valign="top" class="ms-formlabel">
			<H3 class="ms-standardheader">
				<nobr>Aplicável em outra área?</nobr>
			</H3>
		</td>
		<td width="400px" valign="top" class="ms-formbody">
			<asp:Repeater runat="server" ID="outrasAreasRepeater">
                <HeaderTemplate><ul></HeaderTemplate>
                <ItemTemplate><li><%# Container.DataItem %></li></ItemTemplate>
                <FooterTemplate></ul></FooterTemplate>
            </asp:Repeater>
		</td>
	</tr>
    <tr>
		<td width="190px" valign="top" class="ms-formlabel">
			<H3 class="ms-standardheader">
				<nobr>Necessita investimento?</nobr>
			</H3>
		</td>
		<td width="400px" valign="top" class="ms-formbody">
			<asp:Label runat="server" ID="investimentoLabel" />
		</td>
	</tr>
    <tr>
		<td width="190px" valign="top" class="ms-formlabel">
			<H3 class="ms-standardheader">
				<nobr>Materiais e equipamentos</nobr>
			</H3>
		</td>
		<td width="400px" valign="top" class="ms-formbody">
			<asp:Label runat="server" ID="materiaisLabel" />
		</td>
	</tr>
    <tr id="idAttachmentsRow">
		<td nowrap="true" valign="top" class="ms-formlabel" width="20%">
			<SharePoint:FieldLabel ID="FieldLabel1" ControlMode="New" FieldName="Attachments" runat="server"/>
		</td>
		<td valign="top" class="ms-formbody" width="80%">
			<asp:Repeater runat="server" ID="anexosRepeater">
                <HeaderTemplate><ul></HeaderTemplate>
                <ItemTemplate>
                    <li><a href='<%# Eval("Url") %>'><%# Eval("Nome") %></a></li>
                </ItemTemplate>
                <FooterTemplate></ul></FooterTemplate>
            </asp:Repeater>
		</td>
	</tr>
</table>