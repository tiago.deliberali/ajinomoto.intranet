﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.WebControls;

namespace Ajinomoto.FabricaIdeia.Business
{
    public class SubmeterButton : SaveButton
    {
        public string Coluna { get; set; }
        public string Valor { get; set; }
        public string Texto { get; set; }

        public List<ControladorSPGrid> Controladores { get; set; }

        protected override void OnInit(EventArgs e)
        {
            this.Text = Texto;
            base.OnInit(e);
        }

        protected override bool SaveItem()
        {
            if (!string.IsNullOrEmpty(Coluna))
            {
                var field = Item.Fields.GetFieldByInternalName(Coluna);

                if (field != null)
                    Item[Coluna] = Valor;
            }

            AdicionarValorControladores();

            return base.SaveItem();
        }

        private void AdicionarValorControladores()
        {
            foreach (var controlador in Controladores)
            {
                var field = Item.Fields.GetFieldByInternalName(controlador.Coluna);

                if (field != null)
                {
                    Item[controlador.Coluna] = controlador.ToString();
                }
            }
        }
    }
}
