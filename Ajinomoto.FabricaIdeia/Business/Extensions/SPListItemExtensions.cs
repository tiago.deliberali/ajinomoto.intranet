﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;

namespace Ajinomoto.FabricaIdeia.Business
{
    public static class SPListItemExtensions
    {
        public static string ObterValor(this SPListItem item, string fieldName)
        {
            if (item == null)
                return string.Empty;

            SPField field = item.Fields.TryGetFieldByStaticName(fieldName);

            if (field == null)
                return string.Empty;

            return item[field.Id] == null ? string.Empty : item[field.Id].ToString();
        }

        public static void AtribuirValor(this SPListItem item, string fieldName, object valor)
        {
            if (item == null)
                return;

            SPField field = item.Fields.TryGetFieldByStaticName(fieldName);

            if (field == null)
                return;

            item[field.Id] = valor;
        }
    }
}
