﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajinomoto.FabricaIdeia.Business.Activities
{
    public enum StatusWorkflow
    {
        Vazio,
        Salvo,
        Enviado,
        Concluido,
        Cancelado
    }
}
