﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajinomoto.FabricaIdeia.Business
{
    [Serializable]
    public class TaskComite
    {
        public string Responsavel { get; set; }
        public int Posicao { get; set; }

        public TaskComite(string responsavel, int posicao)
        {
            this.Posicao = posicao;
            this.Responsavel = responsavel;
        }
    }
}
