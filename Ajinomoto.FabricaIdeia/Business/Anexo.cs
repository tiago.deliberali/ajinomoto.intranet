﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajinomoto.FabricaIdeia.Business
{
    public class Anexo
    {
        public string Nome { get; set; }
        public string Url { get; set; }
    }
}
