﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.SharePoint.WebControls;

namespace Ajinomoto.FabricaIdeia.Business
{
    public class ControladorSPGrid
    {
        DataTable dados;
        SPGridView grid;
        string chave;

        public string Coluna { get; set; }

        public ControladorSPGrid(DataTable dados, SPGridView grid, string chave)
        {
            this.dados = dados;
            this.grid = grid;
            this.chave = chave;
        }

        public void Incluir(string chave, object[] valores)
        {
            if (!string.IsNullOrEmpty(chave) && !RegistroExiste(chave))
            {
                var linha = dados.NewRow();

                linha[0] = chave;

                for (int i = 0; i < valores.Length; i++)
                {
                    linha[i + 1] = valores[i];
                }

                dados.Rows.Add(linha);
                AtualizarGrid();
            }
        }

        public void Excluir(string valor)
        {
            if (!string.IsNullOrEmpty(valor) && RegistroExiste(valor))
            {
                var linhas = SelecionarDados(valor);
                dados.Rows.Remove(linhas[0]);
                AtualizarGrid();
            }
        }

        public bool RegistroExiste(string valor)
        {
            return SelecionarDados(valor).Length > 0;
        }

        private DataRow[] SelecionarDados(string valor)
        {
            return dados.Select(chave + "='" + valor.Trim() + "'");
        }

        public void AtualizarGrid()
        {
            grid.DataSource = dados.DefaultView;
            grid.DataBind();
        }

        public override string ToString()
        {
            if (dados == null || dados.Columns.Count == 0 || dados.Rows.Count == 0)
                return string.Empty;

            StringBuilder valor = new StringBuilder();
            StringBuilder valorLinha = new StringBuilder();

            foreach (DataRow linha in dados.Rows)
            {
                valorLinha = new StringBuilder();

                foreach (DataColumn coluna in dados.Columns)
                {
                    valorLinha.Append(";" + linha[coluna]);
                }

                valor.Append(":" + valorLinha.ToString().Substring(1));
            }

            return valor.ToString().Substring(1);
        }

        public void Carregar(Microsoft.SharePoint.SPItem item)
        {
            var field = item.Fields.TryGetFieldByStaticName(Coluna);

            if (field == null)
                return;

            var valor = item[Coluna];

            if (valor == null || string.IsNullOrEmpty(valor.ToString()))
                return;

            var linhas = valor.ToString().Split(':');

            foreach (var linha in linhas)
	        {
                var dados = linha.Split(';');
                var chave = dados[0];
                object[] valores = dados.Length == 1 ? null : dados.Skip(1).ToArray();

                Incluir(chave, valores);
	        }
        }
    }
}
