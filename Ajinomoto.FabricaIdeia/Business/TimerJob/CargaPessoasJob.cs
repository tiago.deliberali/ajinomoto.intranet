﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;

namespace Ajinomoto.FabricaIdeia.Business.TimerJob
{
    public class CargaPessoasJob : SPJobDefinition
    {
        public const string JobName = "CargaPessoasJob";

        public CargaPessoasJob()
            : base()
        {
        }

        public CargaPessoasJob(SPWebApplication webApplication) :
            base(JobName, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = "Colaboradores Fábrica de Ideias - Sincronismo";
        }

        public override void Execute(Guid targetInstanceId)
        {
            SPWebApplication webApplication = this.Parent as SPWebApplication;

            foreach (SPSite site in webApplication.Sites)
            {
                foreach (SPWeb web in site.AllWebs)
                {
                    if (web.Lists.TryGetList("Pessoas") != null)
                    {
                        Logger.RegistrarAviso("Processando SPSite' " + site.Url + "', SPWeb: " + web.Url);
                        var gestor = new GestorPessoas(web);
                        gestor.CarregarLista();
                    }
                }
            }
            
        }
    }
}
