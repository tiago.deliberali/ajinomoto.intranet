﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.OracleClient;
using Microsoft.SharePoint;

namespace Ajinomoto.FabricaIdeia.Business.TimerJob
{
    public class ViewPessoas
    {
        private DbConnection conexao;
        private DbCommand comando;
        private SPWeb web;

        private string connectionString = string.Empty;
        private string command = string.Empty;

        public ViewPessoas(SPWeb web)
        {
            this.web = web;
        }

        public List<Pessoa> ObterLista()
        {
            var pessoas = new List<Pessoa>();

            try
            {
                CriarConexao();

                using (var reader = comando.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        pessoas.Add(new Pessoa()
                        {
                            RE = reader.GetString(0),
                            NomeCompleto = reader.GetString(1),
                            Nascimento = reader.GetDateTime(2),
                            Departamento = reader.GetString(3),
                            Mes = reader.GetInt32(4),
                            Dia = reader.GetInt32(5),
                            Data = reader.GetDateTime(6),
                            Unidade = reader.GetString(7),
                        });
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                FinalizarObjetosDoBanco();
            }

            return pessoas;
        }

        private void FinalizarObjetosDoBanco()
        {
            if (comando != null)
                comando.Dispose();

            if (conexao != null)
            {
                conexao.Close();
                conexao.Dispose();
            }
        }


        private void AdicionarParametro(string nome, object valor)
        {
            var parametro = comando.CreateParameter();
            parametro.ParameterName = nome;
            parametro.Value = valor;
            comando.Parameters.Add(parametro);
        }

        private void CriarConexao()
        {
            CarregarConfiguracoes();

            conexao = new OracleConnection(connectionString);
            conexao.Open();

            comando = conexao.CreateCommand();
            comando.CommandText = command;
        }

        private void CarregarConfiguracoes()
        {
            var config = web.Lists.TryGetList("Configuracao");

            foreach (SPListItem item in config.Items)
            {
                if (item.ObterValor("Title") == "ConnectionString")
                    connectionString = item.ObterValor("Valor");

                if (item.ObterValor("Title") == "Command")
                    command = item.ObterValor("Valor");
            }

            Logger.RegistrarAviso("Dados de configuração: " +
                    connectionString + "; " + command);
        }
    }
}
