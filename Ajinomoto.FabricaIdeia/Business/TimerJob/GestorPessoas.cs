﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;

namespace Ajinomoto.FabricaIdeia.Business.TimerJob
{
    public class GestorPessoas
    {
        private SPWeb site;


        public GestorPessoas(SPWeb site)
        {
            this.site = site;
        }


        public void CarregarLista()
        {
            try
            {
                MontarLista();
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex);
                Logger.EnviaEmail(site, "lmadm_sysfaid@br.ajinomoto.com", "Sincronismo Colaboradores - Fábrica de ideias", ex);
            }
        }

        private void LimparLista()
        {
            SPList lista = site.Lists.TryGetList("Pessoas");

            if (lista == null)
                return;

            SPQuery q = new SPQuery();
            q.RowLimit = 100;
            SPListItemCollection coll = lista.GetItems(q);

            while (coll.Count > 0)
            {
                StringBuilder sbDelete = new StringBuilder();
                sbDelete.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Batch>");

                for (int i = 0; i < coll.Count; i++)
                {
                    SPListItem item = coll[i];
                    sbDelete.Append(
                        string.Format(
                            "<Method><SetList Scope=\"Request\">{0}</SetList><SetVar Name=\"ID\">{1}</SetVar><SetVar Name=\"Cmd\">Delete</SetVar></Method>",
                            lista.ID,
                            item.ID));
                }
                sbDelete.Append("</Batch>");

                site.ProcessBatchData(sbDelete.ToString());

                LimparLixeira();

                lista.Update();

                coll = lista.GetItems(q);
            }
        }

        private void LimparLixeira()
        {
            try
            {
                var recycledIds = new List<Guid>();
                foreach (SPRecycleBinItem item in site.RecycleBin)
                {
                    if (item.DirName.EndsWith("Pessoas"))
                        recycledIds.Add(item.ID);
                }
                site.RecycleBin.Delete(recycledIds.ToArray());
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex);
            }
        }



        private void MontarLista()
        {
            SPList lista = site.Lists.TryGetList("Pessoas");

            if (lista == null)
                throw new Exception("Lista 'Pessoas' não encontrada.");

            PreencherLista(lista);
        }

        private void PreencherLista(SPList lista)
        {
            try
            {
                var view = new ViewPessoas(site);
                var pessoas = view.ObterLista();

                Logger.RegistrarAviso("Registros importados (Fábrica de Ideias): " + pessoas.Count);

                LimparLista();

                foreach (var pessoa in pessoas)
                {
                    SPListItem item = lista.Items.Add();
                    item[lista.Fields.TryGetFieldByStaticName("NomeCompleto").Id] = pessoa.NomeCompleto;
                    item[lista.Fields.TryGetFieldByStaticName("Title").Id] = pessoa.RE;
                    item.Update();
                }
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex);
            }
        }
    }
}
