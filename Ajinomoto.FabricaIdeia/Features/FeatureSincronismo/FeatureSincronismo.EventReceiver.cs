using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Security;
using Microsoft.SharePoint.Administration;
using Ajinomoto.FabricaIdeia.Business;
using Ajinomoto.FabricaIdeia.Business.TimerJob;

namespace Ajinomoto.FabricaIdeia.Features.FeatureSincronismo
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("1b06e8ff-f8a7-45ba-8c25-e7743470b6eb")]
    public class FeatureSincronismoEventReceiver : SPFeatureReceiver
    {
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            try
            {
                InstalaJob(properties.Feature.Parent as SPWebApplication);
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex, "Application");
            }
        }

        private static void InstalaJob(SPWebApplication webApplication)
        {
            RemoveJob(webApplication);

            var job =
                new CargaPessoasJob(webApplication);

            SPDailySchedule agenda = new SPDailySchedule();
            agenda.BeginHour = 0;
            agenda.BeginMinute = 30;
            agenda.BeginSecond = 0;

            agenda.EndHour = 1;
            agenda.EndMinute = 30;
            agenda.EndSecond = 0;

            job.Schedule = agenda;
            job.Update();
        }

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            try
            {
                RemoveJob(properties.Feature.Parent as SPWebApplication);
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex, "Application");
            }
        }

        private static void RemoveJob(SPWebApplication webApplication)
        {
            foreach (SPJobDefinition job in webApplication.JobDefinitions)
            {
                if (job.Name == CargaPessoasJob.JobName)
                    job.Delete();
            }
        }
    }
}
