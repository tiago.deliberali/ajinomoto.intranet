﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Collections.Generic;
using Ajinomoto.Intranet.Business.Aniversariantes;
using System.Linq;

namespace Ajinomoto.Intranet.SP.AniversariantesWebPart
{

    public partial class AniversariantesWebPartUserControl : UserControl
    {
        public AniversariantesWebPart WebPart { get; set; }
        public string UrlAniversariantes { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var siteCollection = SPContext.Current.Site;
            var site = siteCollection.RootWeb;
            var listaAniversario = site.Lists.TryGetList("Lista Aniversario");

            if (listaAniversario != null)
            {
                var aniversariantes = ObterAniersariantesEmCache(listaAniversario);

                AniversariantesRepeater.DataSource = 
                    aniversariantes
                        .Where(x =>
                            (
                                string.IsNullOrEmpty(WebPart.Unidade) 
                                || x.Unidade == WebPart.Unidade
                            )
                            && x.Data >= DateTime.Today 
                            && x.Data <= DateTime.Today.AddDays(WebPart.Dias))
                        .OrderBy(x => x.Data)
                        .ThenBy(x => x.NomeCompleto);

                AniversariantesRepeater.DataBind();

                UrlAniversariantes = listaAniversario.DefaultViewUrl;

                if (!string.IsNullOrEmpty(WebPart.Unidade))
                {
                    UrlAniversariantes += "?FilterField1=Unidade&FilterValue1=" + Uri.EscapeDataString(WebPart.Unidade);
                }
            }
        }

        private List<Aniversariante> ObterAniersariantesEmCache(SPList listaAniversario)
        {
            List<Aniversariante> lista = (List<Aniversariante>)Cache["wp_listaAniversariantes"];

            if (lista == null)
            {
                lista = new List<Aniversariante>();

                foreach (SPListItem item in listaAniversario.Items)
                {
                    lista.Add(new Aniversariante()
                    {
                        NomeCompleto = item["Nome"].ToString(),
                        Data = DateTime.Parse(item["Data"].ToString()),
                        Departamento = item["Departamento"].ToString(),
                        Unidade = item["Unidade"].ToString(),
                    });
                }

                Cache.Add("wp_listaAniversariantes",
                    lista,
                    null,
                    DateTime.Today.AddHours(23).AddMinutes(59), 
                    System.Web.Caching.Cache.NoSlidingExpiration,
                    System.Web.Caching.CacheItemPriority.Normal,
                    null);
            }
            return lista;
        }
    }
}
