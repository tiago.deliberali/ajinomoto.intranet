﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AniversariantesWebPartUserControl.ascx.cs" Inherits="Ajinomoto.Intranet.SP.AniversariantesWebPart.AniversariantesWebPartUserControl" %>
<%@ Import Namespace="Ajinomoto.Intranet.Business.Aniversariantes" %>
<asp:Repeater runat="server" ID="AniversariantesRepeater">
    <HeaderTemplate>
        <table>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td style="padding-top: 5px; padding-bottom: 5px; font: 8pt Verdana, Arial, sans-serif;">
                <%# ((Aniversariante)Container.DataItem).Data.Date == DateTime.Today ? "Hoje" : ((Ajinomoto.Intranet.Business.Aniversariantes.Aniversariante)Container.DataItem).Data.ToShortDateString()%><br />
                <b><%# ((Aniversariante)Container.DataItem).NomeCompleto %></b><br />
                <%# ((Aniversariante)Container.DataItem).Departamento + " - " + ((Aniversariante)Container.DataItem).Unidade %>
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
            <tr>
                <td style="padding-top: 5px; padding-bottom: 5px;">
                    <a href="<%= this.UrlAniversariantes %>">Ver aniversariantes</a>
                </td>
            </tr>
        </table>
    </FooterTemplate>
</asp:Repeater>