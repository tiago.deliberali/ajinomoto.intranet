﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace Ajinomoto.Intranet.SP.AniversariantesWebPart
{
    [ToolboxItemAttribute(false)]
    public class AniversariantesWebPart : WebPart
    {
        [WebBrowsable,
        Category("Ajinomoto"),
        Personalizable(PersonalizationScope.Shared),
        WebDisplayName("Unidade"),
        WebDescription("Digite a Unidade dos aniversariantes a ser exibida. Deixe em branco para todos.")]
        public string Unidade { get; set; }

        [WebBrowsable,
        Category("Ajinomoto"),
        Personalizable(PersonalizationScope.Shared),
        WebDisplayName("Dias para exibição"),
        WebDescription("Define quantos dias além da data de hoje será considerado para exibir os aniversariantes.")]
        public int Dias { get; set; }


        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Ajinomoto.Intranet.SP/AniversariantesWebPart/AniversariantesWebPartUserControl.ascx";

        protected override void CreateChildControls()
        {
            Control control = Page.LoadControl(_ascxPath);
            if (control != null)
            {
                ((AniversariantesWebPartUserControl)control).WebPart = this;
            }
            Controls.Add(control);
        }
    }
}
