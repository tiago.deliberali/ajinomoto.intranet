﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" 
  xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" 
  exclude-result-prefixes="xsl msxsl ddwrt" 
  xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" 
  xmlns:asp="http://schemas.microsoft.com/ASPNET/20" 
  xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
  xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal" 
  xmlns:o="urn:schemas-microsoft-com:office:office" ddwrt:ghost="show_all">
  
  <xsl:output method="html" indent="yes" />
  
  <xsl:template match="dsQueryResponse" 
  ddwrt:ghost="" xmlns:ddwrt2="urn:frontpage:internal">
    <table width="95%" style="margin-top:20px; margin-left:20px;text-align: center;">
      <tr style="height: 30px; font-size: 10pt; font-weight: bold" bgcolor="#BBBBBB">
        <td>Data</td>
        <td>Nome</td>
        <td>Departamento</td>
        <td>Unidade</td>
      </tr>
      <xsl:apply-templates select="Rows/Row" />
    </table>
  </xsl:template>

  <xsl:template match="Row">
    <xsl:variable name="line-color">
      <xsl:choose>
        <xsl:when test="position() mod 2 = 0">lightgray</xsl:when>
        <xsl:otherwise>white</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
  
    <xsl:variable name="Hoje" select="ddwrt:FormatDateTime(ddwrt:Today(), 1033, 'yyyyMMdd')" />
    <xsl:variable name="Data" select="ddwrt:FormatDateTime(@Data, 1033, 'yyyyMMdd')" />
    
    <tr style="height: 30px; font-size: 8pt;" bgcolor="{$line-color}">
      <td>
        <xsl:choose>
          <xsl:when test="$Data = $Hoje">
            Hoje
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@Data" />
          </xsl:otherwise>
        </xsl:choose>
      </td>
      <td style="font-weight: bold;">
        <xsl:value-of select="@Title" />
      </td>
      <td>
        <xsl:value-of select="@Departamento" />
      </td>
      <td>
        <xsl:value-of select="@Unidade" />
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
