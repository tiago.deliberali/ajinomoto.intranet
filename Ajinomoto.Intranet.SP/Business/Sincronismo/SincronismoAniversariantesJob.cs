﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Ajinomoto.Intranet.Business.Aniversariantes;
using Microsoft.SharePoint;

namespace Ajinomoto.Intranet.Business.Sincronismo
{
    public class SincronismoAniversariantesJob : SPJobDefinition
    {
        public const string JobName = "IntranetAniversariantesJob";

        public SincronismoAniversariantesJob()
            : base()
        {
        }

        public SincronismoAniversariantesJob(SPWebApplication webApplication) :
            base(JobName, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = "Sincronismo Intranet Aniversariantes";
        }

        public override void Execute(Guid targetInstanceId)
        {
            SPWebApplication webApplication = this.Parent as SPWebApplication;

            foreach (SPSite site in webApplication.Sites)
            {
                if (site.RootWeb.Lists.TryGetList("Configuracao") != null)
                {
                    Logger.RegistrarAviso("Processando webApplication: " + site.Url);
                    var gestor = new GestorAniversariantesSP(site);
                    gestor.CarregarLista();
                }
            }
            
        }
    }
}
