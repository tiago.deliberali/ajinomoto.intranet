﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;

namespace Ajinomoto.Intranet.Business
{
    public class Configuracao
    {
        public const string NomeLista = "Configuracao";
        Dictionary<string, string> configuracoes;
        SPWeb site;

        public Configuracao(SPWeb site)
        {
            this.site = site;
            configuracoes = new Dictionary<string, string>();

            SPList lista = ObterListaConfiguracao();

            if (lista != null)
            {
                foreach (SPListItem item in lista.Items)
                    configuracoes.Add(item["Nome"].ToString(), item["Valor"].ToString());
            }
            else
            {
                CriarListaConfiguracao(site);
            }
        }

        private SPList ObterListaConfiguracao()
        {
            SPList lista = site.Lists.TryGetList(NomeLista);
            return lista;
        }

        public string this[string nome]
        {
            get
            {
                if (configuracoes.ContainsKey(nome))
                    return configuracoes[nome];
                else
                    return string.Empty;
            }
        }

        public void CadastrarConfiguracao(string nome, string valor, string descricao)
        {
            if (configuracoes.ContainsKey(nome))
                return;

            SPList lista = ObterListaConfiguracao();

            SPListItem novoItem = lista.Items.Add();
            novoItem["Nome"] = nome;
            novoItem["Valor"] = valor;
            novoItem["Descricao"] = descricao;

            novoItem.Update();

            configuracoes.Add(nome, valor);
        }


        public static void CriarListaConfiguracao(SPWeb site)
        {
            var lista = site.Lists.TryGetList(Configuracao.NomeLista);

            if (lista == null)
            {
                var listaId = site.Lists.Add(
                    Configuracao.NomeLista,
                    "Configurações relativas a intranet",
                    SPListTemplateType.GenericList);

                lista = site.Lists[listaId];
                lista.OnQuickLaunch = false;

                var title = lista.Fields.GetFieldByInternalName("Title");
                title.Title = "Nome";
                title.Update();

                var valorFieldName = lista.Fields.Add("Valor", SPFieldType.Text, true);
                var descricaoFiieldName = lista.Fields.Add("Descricao", SPFieldType.Text, false);

                lista.Update();


                var view = lista.DefaultView;
                view.ViewFields.Add(valorFieldName);
                view.ViewFields.Add(descricaoFiieldName);
                view.Update();
            }
        }
    }
}
