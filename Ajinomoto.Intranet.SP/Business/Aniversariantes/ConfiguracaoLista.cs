﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Microsoft.SharePoint;

namespace Ajinomoto.Intranet.Business.Aniversariantes
{
    public class ConfiguracaoLista
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Template { get; set; }
        public string Url { get; set; }
        public int Dias { get; set; }
        public string MapaUnidades { get; set; }
        public string EmailErro { get; set; }

        public ConfiguracaoLista(SPWeb site)
        {
            var config = new Configuracao(site);

            Nome = config["listaAniversariantes"];
            Descricao = config["descricaoAniversariantes"];
            Template = config["templateAniversariantes"];
            EmailErro = config["emailErro"];
            Url = site.Url;

            int dias = 0;
            if (int.TryParse(config["diasAniversariantes"], out dias))
                Dias = dias;
        }

        public override string ToString()
        {
            return string.Format("Nome: {0}, Descricao: {1}, Template: {2}, Url: {3}, Dias: {4}, MapaUnidades: {5}", 
                Nome, Descricao, Template, Url, Dias, MapaUnidades);
        }
    }
}
