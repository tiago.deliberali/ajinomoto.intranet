﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajinomoto.Intranet.Business.Aniversariantes
{
    public enum ColunasViewAniversariantes
    {
        RE = 0,
        NomeCompleto = 1,
        Nascimento = 2,
        Departamento = 3,
        Mes = 4,
        Dia = 5,
        Data = 6,
        Unidade = 7,
    }
}
