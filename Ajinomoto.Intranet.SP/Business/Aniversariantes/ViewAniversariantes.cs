﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using Microsoft.SharePoint;


namespace Ajinomoto.Intranet.Business.Aniversariantes
{
    public class ViewAniversariantes
    {
        private DbConnection conexao;
        private DbCommand comando;
        private SPWeb site;

        string comandoSQL;
        string comandoInicio;
        string comandoFim;

        public ViewAniversariantes(SPWeb site)
        {
            this.site = site;
        }

        public List<Aniversariante> ObterLista(DateTime data, int dias, string banco)
        {
            var aniversariantes = new List<Aniversariante>();

            try
            {
                CriarConexao();
                CriarCommando(data, dias, banco);

                using (var reader = comando.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        aniversariantes.Add(new Aniversariante()
                        {
                            RE = reader.GetString(0),
                            NomeCompleto = reader.GetString(1),
                            Nascimento = reader.GetDateTime(2),
                            Departamento = reader.GetString(3),
                            Mes = reader.GetInt32(4),
                            Dia = reader.GetInt32(5),
                            Data = reader.GetDateTime(6),
                            Unidade = reader.GetString(7),
                        });
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                FinalizarObjetosDoBanco();
            }

            return aniversariantes;
        }

        private void FinalizarObjetosDoBanco()
        {
            if (comando != null)
                comando.Dispose();

            if (conexao != null)
            {
                conexao.Close();
                conexao.Dispose();
            }
        }

        private void CriarCommando(DateTime data, int dias, string banco)
        {
            if (!string.IsNullOrEmpty(banco))
                comandoSQL = 
                    comandoSQL.Replace(
                        "UVW_ANIVER_MES_CORP_ALL", 
                        "\"UVW_ANIVER_MES_CORP\"@" + banco);

            comando = conexao.CreateCommand();
            comando.CommandText = comandoSQL;

            AdicionarParametro(comandoInicio, data);
            AdicionarParametro(comandoFim, data.AddDays(dias));
        }

        private void AdicionarParametro(string nome, object valor)
        {
            var parametro = comando.CreateParameter();
            parametro.ParameterName = nome;
            parametro.Value = valor;
            comando.Parameters.Add(parametro);
        }

        private void CriarConexao()
        {
            var config = new Configuracao(site);
            var connectionString = config["ConnectionString"];
            var provider = config["ConnectionStringProvider"];

            if (provider.Equals("OracleClient"))
            {
                conexao = new OracleConnection(connectionString);

                comandoSQL = "SELECT * FROM UVW_ANIVER_MES_CORP_ALL WHERE \"Data\" BETWEEN :inicio AND :fim";
                comandoInicio = ":inicio";
                comandoFim = ":fim";
            }
            else
            {
                conexao = new SqlConnection(connectionString);

                comandoSQL = "SELECT * FROM UVW_ANIVER_MES_CORP_ALL WHERE [Data] BETWEEN @inicio AND @fim";
                comandoInicio = "@inicio";
                comandoFim = "@fim";
            }

            conexao.Open();
        }
    }
}
