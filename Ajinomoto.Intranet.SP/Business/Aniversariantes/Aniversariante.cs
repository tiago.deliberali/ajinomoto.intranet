﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajinomoto.Intranet.Business.Aniversariantes
{
    public class Aniversariante
    {
        public string RE { get; set; }
        public string NomeCompleto { get; set; }
        public DateTime Nascimento { get; set; }
        public string Departamento { get; set; }
        public int Mes { get; set; }
        public int Dia { get; set; }
        public DateTime Data { get; set; }
        public string Unidade { get; set; }
    }
}
