﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Microsoft.SharePoint;
using System.Collections.Specialized;

namespace Ajinomoto.Intranet.Business.Aniversariantes
{
    public class GestorAniversariantesSP
    {
        private SPWeb site;
        private SPSite siteCollection;
        private ConfiguracaoLista configuracaoLista;

        public GestorAniversariantesSP(SPSite siteCollection)
        {
            this.siteCollection = siteCollection;
            this.site = siteCollection.RootWeb;
            this.configuracaoLista = new ConfiguracaoLista(this.site);
            Logger.RegistrarAviso("Arquivo de configuracao gerado: " + configuracaoLista.ToString());
        }

        public void CarregarLista()
        {
            try
            {
                MontarLista();
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex);
                Logger.EnviaEmail(siteCollection.RootWeb, configuracaoLista.EmailErro, "Sincronismo Lista Aniversariante", ex);
            }
        }

        private void LimparLista()
        {
            LimparLista(string.Empty);
        }

        private void LimparLista(string unidade)
        {
            SPList lista = site.Lists.TryGetList(configuracaoLista.Nome);

            if (lista == null)
                return;
            
            SPQuery q = new SPQuery();
            q.RowLimit = 100;

            if (!string.IsNullOrEmpty(unidade))
                q.Query = "<Where><Eq><FieldRef Name=\"Unidade\" /><Value Type=\"Text\">" + unidade + "</Value></Eq></Where>";

            SPListItemCollection coll = lista.GetItems(q);

            while (coll.Count > 0)
            {
                StringBuilder sbDelete = new StringBuilder();
                sbDelete.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Batch>");

                for (int i = 0; i < coll.Count; i++)
                {
                    SPListItem item = coll[i];
                    sbDelete.Append(
                        string.Format(
                            "<Method><SetList Scope=\"Request\">{0}</SetList><SetVar Name=\"ID\">{1}</SetVar><SetVar Name=\"Cmd\">Delete</SetVar></Method>", 
                            lista.ID, 
                            item.ID));
                }
                sbDelete.Append("</Batch>");

                site.ProcessBatchData(sbDelete.ToString());

                LimparLixeira();

                lista.Update();

                coll = lista.GetItems(q);
            }
        }

        private void LimparLixeira()
        {
            try
            {
                var recycledIds = new List<Guid>();
                foreach (SPRecycleBinItem item in site.RecycleBin)
                {
                    if (item.DirName.EndsWith(configuracaoLista.Nome))
                        recycledIds.Add(item.ID);
                }
                site.RecycleBin.Delete(recycledIds.ToArray());
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex);
            }
        }



        private void MontarLista()
        {
            SPList lista = site.Lists.TryGetList(configuracaoLista.Nome);

            if (lista == null)
                lista = CriarLista(site);

            PreencherLista(lista);
        }

        private SPList CriarLista(SPWeb site)
        {
            SPList lista = site.Lists.TryGetList(configuracaoLista.Nome);

            if (lista == null)
            {
                var listaId = site.Lists.Add(
                    configuracaoLista.Nome,
                    "Lista de Aniversariantes",
                    SPListTemplateType.GenericList);

                lista = site.Lists[listaId];
                lista.OnQuickLaunch = false;

                var title = lista.Fields.GetFieldByInternalName("Title");
                title.Title = "Nome";
                title.Update();

                var dataFieldName = lista.Fields.Add("Data", SPFieldType.DateTime, true);
                var departamentoFieldName = lista.Fields.Add("Departamento", SPFieldType.Text, false);
                var unidadeFieldName = lista.Fields.Add("Unidade", SPFieldType.Text, false);

                lista.Update();

                var dataField = (SPFieldDateTime)lista.Fields[dataFieldName];
                dataField.DisplayFormat = SPDateTimeFieldFormatType.DateOnly;
                dataField.Update();

                var defaultView = lista.DefaultView;
                defaultView.ViewFields.Add(departamentoFieldName);
                defaultView.ViewFields.Add(unidadeFieldName);
                defaultView.Update();

                var viewFields = new StringCollection();
                viewFields.Add("Data");
                viewFields.Add("Departamento");
                viewFields.Add("Unidade");

                var viewAniversariantes = lista.Views.Add("Aniversariantes",
                    viewFields,
                    "<OrderBy><FieldRef Name=\"Data\"/><FieldRef Name=\"Title\"/></OrderBy>",
                    30,
                    false,
                    true);
                viewAniversariantes.XslLink = "view_aniversariantes.xsl";
                viewAniversariantes.Update();
            }

            return lista;
        }

        private void PreencherLista(SPList lista)
        { 
            var view = new ViewAniversariantes(site);
            var plantas = CarregarPlantas();

            foreach (var planta in plantas)
            {
                PreencherListaDePlanta(lista, view, planta);
            }
        }

        private List<KeyValuePair<string, string>> CarregarPlantas()
        {
            var config = new Configuracao(siteCollection.RootWeb);

            var plantas = new List<KeyValuePair<string, string>>();
            var definicaoPlantas = config["Plantas"];

            if (string.IsNullOrEmpty(definicaoPlantas))
            {
                plantas.Add(new KeyValuePair<string, string>(string.Empty, string.Empty));

                return plantas;
            }


            foreach (var item in definicaoPlantas.Split(';'))
            {
                var dados = item.Split(':');
                plantas.Add(new KeyValuePair<string, string>(dados[0], dados[1]));
            }

            return plantas;
        }

        private void PreencherListaDePlanta(SPList lista, ViewAniversariantes view, KeyValuePair<string, string> planta)
        {
            try
            {
                var aniversariantes = view.ObterLista(DateTime.Today, configuracaoLista.Dias, planta.Value);

                Logger.RegistrarAviso("Registros importados de " + planta.Key + ": " + aniversariantes.Count);

                LimparLista(planta.Key);

                foreach (var aniversariante in aniversariantes)
                {
                    SPListItem item = lista.Items.Add();
                    item["Title"] = aniversariante.NomeCompleto;
                    item["Data"] = aniversariante.Data;
                    item["Departamento"] = aniversariante.Departamento;
                    item["Unidade"] = aniversariante.Unidade;
                    item.Update();
                }
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex);
            }
        }
    }
}
