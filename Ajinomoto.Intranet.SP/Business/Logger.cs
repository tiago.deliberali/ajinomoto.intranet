﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint;

namespace Ajinomoto.Intranet.Business
{
    public class Logger
    {
        private static string sourceName = "Application";
        public static List<string> erros = new List<string>();
        
        public static void RegistrarAviso(string mensagem)
        {
            EscreverMensagem(mensagem, EventLogEntryType.Information);
        }



        public static void RegistrarErro(string mensagem)
        {
            EscreverMensagem(mensagem, EventLogEntryType.Error);
        }

        public static void RegistrarErro(Exception ex)
        {
            RegistrarErro(MensagemFinal(ex));
        }

        public static void RegistrarErro(Exception ex, string sourceName)
        {
            Logger.sourceName = sourceName;
            RegistrarErro(MensagemFinal(ex));
        }

        private static void EscreverMensagem(string mensagem, EventLogEntryType tipoEvento)
        {
            erros.Add(mensagem);
            EventLog.WriteEntry(sourceName, "Ajinomoto.Intranet.Logger: " + mensagem, tipoEvento);
        }


        public static string MensagemFinal(Exception ex)
        {
            var erro = new StringBuilder();
            while (ex.InnerException != null)
            {
                erro.AppendLine(ex.Message + "\n" + "Stack Trace:" + ex.StackTrace);
                ex = ex.InnerException;

            }

            erro.AppendLine(ex.Message + "\n" + "Stack Trace:" + ex.StackTrace);
            return erro.ToString();
        }

        public static void EnviaEmail(SPWeb site, string para, string sistema, Exception ex)
        {
            SPUtility.SendEmail(
                site,
                false,
                false,
                para,
                "Falha no sistema: " + sistema,
                MensagemFinal(ex));
        }
    }
}

