using System;
using System.Runtime.InteropServices;
using Ajinomoto.Intranet.Business;
using Microsoft.SharePoint;

namespace Ajinomoto.Intranet.SP.Features.IntranetFeature
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("9bee9eaf-746f-4a78-8ba2-a04336ea91c2")]
    public class IntranetAjinomotoFeatureEventReceiver : SPFeatureReceiver
    {
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            try
            {
                SPSite siteCollection = properties.Feature.Parent as SPSite;
                CriarListaConfiguracao(siteCollection.RootWeb);
                CarregaConfiguracoes(siteCollection.RootWeb);
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex, "Application");
            }
        }

        private void CriarListaConfiguracao(SPWeb site)
        {
            Configuracao.CriarListaConfiguracao(site);
        }

        private void CarregaConfiguracoes(SPWeb site)
        {
            var config = new Configuracao(site);

            config.CadastrarConfiguracao("listaAniversariantes", "Lista Aniversario", "Nome da lista de aniversariantes. Deve ser id�ntico ao noma da Lista no Sharepoint");
            config.CadastrarConfiguracao("diasAniversariantes", "3", "N�mero de dias a serem somados � data de hoje para compor o intervalo para busca de aniversariantes");
            config.CadastrarConfiguracao("emailErro", "murilo_bittencourt@br.ajinomoto.com", "Email do respons�vel a ser avisado em caso de falha do sincronismo da lista de Aniversariantes");

            config.CadastrarConfiguracao("ConnectionString", "Data Source=VETORFP_SP;Integrated Security=False;User Id=intranet_consulta;Password=ajintranetconsulta", "Query de conex�o para acessar a view UVW_ANIVER_MES_CORP_ALL e retornar os aniversariantes do dia");
            config.CadastrarConfiguracao("ConnectionStringProvider", "OracleClient", "Provider a ser suado pela aplica��o. Para Oracle, usar OracleClient. Qualquer outro valor usar� o SqlServerProvider");

            config.CadastrarConfiguracao("Plantas", "S�o Paulo:senior_sp;Valpara�so:senior_vp;Limeira:senior_lm;Pederneiras:senior_pd;Laranjal Paulista:senior_lp", "Nome das unidades e nome da base do senior a ser usada.");
        }
    }
}
