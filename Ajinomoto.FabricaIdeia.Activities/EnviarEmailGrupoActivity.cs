﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;
using Microsoft.SharePoint.WorkflowActions;
using System.Workflow.ComponentModel;
using System.ComponentModel;
using System.Workflow.ComponentModel.Compiler;
using Ajinomoto.FabricaIdeia.Activities;
using System.Net.Mail;


namespace Ajinomoto.FabricaIdeia.Business.Activities
{
    public class EnviarEmailGrupoActivity : Activity
    {
        public EnviarEmailGrupoActivity()
        {

        }

        public static DependencyProperty WorkflowPropertiesProperty =
            DependencyProperty.Register("WorkflowProperties", typeof(SPWorkflowActivationProperties), typeof(EnviarEmailGrupoActivity));

        [DescriptionAttribute("WorkflowProperties")]
        [BrowsableAttribute(true)]
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Visible)]
        [ValidationOption(ValidationOption.Optional)]
        public SPWorkflowActivationProperties WorkflowProperties
        {
            get
            {
                return (SPWorkflowActivationProperties)base.GetValue(EnviarEmailGrupoActivity.WorkflowPropertiesProperty);
            }
            set
            {
                base.SetValue(EnviarEmailGrupoActivity.WorkflowPropertiesProperty, value);
            }
        }

        public static DependencyProperty GrupoProperty =
            DependencyProperty.Register("Grupo", typeof(string), typeof(EnviarEmailGrupoActivity));

        [DescriptionAttribute("Grupo")]
        [BrowsableAttribute(true)]
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Visible)]
        [ValidationOption(ValidationOption.Optional)]
        public string Grupo
        {
            get
            {
                return (string)base.GetValue(EnviarEmailGrupoActivity.GrupoProperty);
            }
            set
            {
                base.SetValue(EnviarEmailGrupoActivity.GrupoProperty, value);
            }
        }

        // Triggers when the activity is executed. 
        protected override System.Workflow.ComponentModel.ActivityExecutionStatus Execute(System.Workflow.ComponentModel.ActivityExecutionContext executionContext)
        {
            var site = WorkflowProperties.Web;
            var mail = new MailMessage();
            var emailAdm = "lmadm_sysfaid@br.ajinomoto.com";

            if (Grupo == "[Ideia]")
            {
                SPField criadoPor = WorkflowProperties.Item.Fields.TryGetFieldByStaticName("Author");
                var usuario = new SPFieldUserValue(site, WorkflowProperties.Item[criadoPor.Id].ToString());

                mail.To.Add(usuario.User.Email);
                mail.From = new MailAddress(emailAdm);
                mail.Subject = "Status da ideia alterado";
                mail.Body = "O status da sua ideia foi alterado. Favor verificar.";
            }
            else
            {
                ObterEmailsParaEnvio(site, mail.To);
                mail.From = new MailAddress(emailAdm);
                mail.Subject = "Nova tarefa: Fábrica de ideias";
                mail.Body = "Novas tarefas atribuidas para você referente a Fábrica de Ideias.";
            }
            EnviarMensagem(mail);

            return base.Execute(executionContext);
        }

        private void ObterEmailsParaEnvio(SPWeb site, MailAddressCollection to)
        {
            string aprovadores = string.Empty;

            if (string.IsNullOrEmpty(Grupo))
                return;

            try
            {
                var grupo = site.SiteGroups[Grupo];

                foreach (SPUser usuario in grupo.Users)
                    if (!string.IsNullOrEmpty(usuario.Email))
                        to.Add(usuario.Email);
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex);
            }
        }

        private void EnviarMensagem(MailMessage message)
        {
            try
            {
                SmtpClient client =
                        new SmtpClient(
                            WorkflowProperties.Site.WebApplication.OutboundMailServiceInstance.Parent.Name);

                client.Send(message);
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex);
            }
        }

    }
}
