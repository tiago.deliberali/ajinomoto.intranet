﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajinomoto.FabricaIdeia.Business.Activities
{
    public enum StatusWorkflow
    {
        Vazio,
        Salvo,
        Enviado,
        Analise_Pia,
        Analise_AreaImpactada,
        Analise_Comite,
        Concluido,
        Cancelado,
        Decisao_Comite,
        Implantacao,
        Inviavel,
        Implantado
    }
}
