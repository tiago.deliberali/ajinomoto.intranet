﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;

namespace Ajinomoto.Intranet.Workflow.Business
{
    public class HandleEventFiring : SPItemEventReceiver
    {
        public void HabilitarEventos()
        {
            this.EventFiringEnabled = true;
        }

        public void DesabilitarEventos()
        {
            this.EventFiringEnabled = false;
        }
    }
}
