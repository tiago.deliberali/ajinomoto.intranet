﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Ajinomoto.Intranet.Workflow.Business
{
    [Serializable]
    public class AggregationCalendars
    {
        [XmlArray]
        public List<AggregationCalendar> AggregationCalendar { get; set; }
    }

    [Serializable]
    public class AggregationCalendar
    {
        [XmlAttribute]
        public string Id { get; set; }

        [XmlAttribute]
        public string Type { get; set; }

        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string Description { get; set; }

        [XmlAttribute]
        public string Color { get; set; }

        [XmlAttribute]
        public string AlwaysShow { get; set; }

        [XmlAttribute]
        public string CalendarUrl { get; set; }

        [XmlElement]
        public Settings Settings { get; set; }
    }

    [Serializable]
    public class Settings
    {
        [XmlAttribute]
        public string WebUrl { get; set; }

        [XmlAttribute]
        public string ListId { get; set; }

        [XmlAttribute]
        public string ViewId { get; set; }

        [XmlAttribute]
        public string ListFormUrl { get; set; }
    }
}
