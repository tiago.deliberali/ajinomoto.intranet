﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using Microsoft.SharePoint;
using System.Collections.Specialized;
using Ajinomoto.Intranet.Business;
using Microsoft.SharePoint.WebPartPages;
using System.Web.UI.WebControls.WebParts;
using System.Reflection;
using System.Xml;

namespace Ajinomoto.Intranet.Workflow.Business
{
    public class PreparoCalendario
    {
        private SPSite site;
        private SPWeb web;
        private SPList list;

        public PreparoCalendario(SPSite site)
        {
            this.site = site;
        }



        public void AdicionarActionParaTodasListas()
        {
            foreach (SPWeb web in site.AllWebs)
            {
                foreach (SPList lista in web.Lists)
                {
                    AdicionarAction(web, lista);
                }
            }
        }

        public void AdicionarAction(SPWeb web, SPList lista)
        {
            var temAcao = lista.UserCustomActions.Any(x => x.Name == ConfiguracaoCalendario.ActionCriarCalendario);

            if (!temAcao && PodeExecutar(lista))
            {
                var acao = lista.UserCustomActions.Add();
                acao.Url = string.Format("/_layouts/FluxoAprovacao.aspx?web={0}&lista={1}",
                    web.ID,
                    lista.ID);
                acao.Name = ConfiguracaoCalendario.ActionCriarCalendario;
                acao.Location = "Microsoft.SharePoint.ListEdit";
                acao.Group = "GeneralSettings";
                acao.Title = "Criar fluxo de aprovação";
                acao.Rights = SPBasePermissions.ManageLists;
                acao.Update();
            }
        }

        private bool PodeExecutar(SPList lista)
        {
            var temStatusAnterior = lista.Fields.TryGetFieldByStaticName("StatusAnterior") != null;
            var baseCalendario = lista.BaseTemplate == SPListTemplateType.Events;

            return !temStatusAnterior && baseCalendario;
        }



        public void Preparar(Guid webId, Guid listId)
        {
            try
            {
                web = site.AllWebs[webId];
                list = web.Lists[listId];

                web.AllowUnsafeUpdates = true;
                web.Update();

                if (PodeExecutar(list))
                {
                    ConfiguraLista();
                    PrepararViews();
                    CarregarAggregationViews();
                }

                ExcluirAcao();
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex);
            }
            finally
            {
                web.AllowUnsafeUpdates = false;
                web.Update();
            }
        }

        private void ConfiguraLista()
        {
            CriaHiddenField("StatusAnterior", SPFieldType.Number);
            CriaHiddenField("DataInicialAnterior", SPFieldType.DateTime);

            list.EnableModeration = true;
            list.DraftVersionVisibility = DraftVisibilityType.Reader;

            list.ReadSecurity = 1;
            list.WriteSecurity = 2;
            list.EnableAttachments = false;
            list.Update();
        }

        private void CriaHiddenField(string nomeField, SPFieldType tipo)
        {
            if (list.Fields.TryGetFieldByStaticName(nomeField) == null)
            {
                list.Fields.Add(nomeField, tipo, false);

                SPField novoField = list.Fields.TryGetFieldByStaticName(nomeField);
                novoField.Hidden = true;
                novoField.Update(); 
            }
        }

        private void PrepararViews()
        {
            var viewPrincipal = list.DefaultView;
            viewPrincipal.Query = "<Where><And><DateRangesOverlap><FieldRef Name=\"EventDate\" /><FieldRef Name=\"EndDate\" /><FieldRef Name=\"RecurrenceID\" /><Value Type=\"DateTime\"><Month /></Value></DateRangesOverlap><Eq><FieldRef Name=\"_ModerationStatus\" /><Value Type=\"ModStat\">0</Value></Eq></And></Where>";
            viewPrincipal.ViewData = "<FieldRef Name=\"Title\" Type=\"CalendarMonthTitle\" /><FieldRef Name=\"Title\" Type=\"CalendarWeekTitle\" /><FieldRef Name=\"Author\" Type=\"CalendarWeekLocation\" /><FieldRef Name=\"Title\" Type=\"CalendarDayTitle\" /><FieldRef Name=\"Location\" Type=\"CalendarDayLocation\" />";
            viewPrincipal.Update();
            SetCalendarViewStyle("week", viewPrincipal.Url);

            SPView viewPendentes = ObterViewOuCriar("Pendentes", list);
            viewPendentes.Query = "<Where><And><DateRangesOverlap><FieldRef Name=\"EventDate\" /><FieldRef Name=\"EndDate\" /><FieldRef Name=\"RecurrenceID\" /><Value Type=\"DateTime\"><Month /></Value></DateRangesOverlap><Eq><FieldRef Name=\"_ModerationStatus\" /><Value Type=\"ModStat\">2</Value></Eq></And></Where>";
            viewPendentes.Update();
            SetCalendarViewStyle("week", viewPendentes.Url);

            SPView viewRejeitadas = ObterViewOuCriar("Rejeitados", list);
            viewRejeitadas.Query = "<Where><And><DateRangesOverlap><FieldRef Name=\"EventDate\" /><FieldRef Name=\"EndDate\" /><FieldRef Name=\"RecurrenceID\" /><Value Type=\"DateTime\"><Month /></Value></DateRangesOverlap><And><Eq><FieldRef Name=\"_ModerationStatus\" /><Value Type=\"ModStat\">1</Value></Eq><Eq><FieldRef Name=\"Author\" /><Value Type=\"Integer\"><UserID Type=\"Integer\" /></Value></Eq></And></And></Where>";
            viewRejeitadas.Update();
            SetCalendarViewStyle("week", viewRejeitadas.Url);
        }

        private SPView ObterViewOuCriar(string nomeView, SPList lista)
        {
            try
            {
                return lista.Views[nomeView];
            }
            catch (Exception)
            {
                var fields = new StringCollection();
                fields.Add("EventDate");
                fields.Add("EndDate");
                fields.Add("Title");
                fields.Add("fRecurrence");

                string query = "";

                var novaView =
                    lista.Views.Add(
                        nomeView,
                        fields,
                        query,
                        30,
                        false,
                        false,
                        SPViewCollection.SPViewType.Calendar,
                        false);

                novaView.Toolbar = "Standard";
                novaView.ViewData = "<FieldRef Name=\"Title\" Type=\"CalendarMonthTitle\" /><FieldRef Name=\"Title\" Type=\"CalendarWeekTitle\" /><FieldRef Name=\"Author\" Type=\"CalendarWeekLocation\" /><FieldRef Name=\"Title\" Type=\"CalendarDayTitle\" /><FieldRef Name=\"Location\" Type=\"CalendarDayLocation\" />";
                novaView.Update();

                return novaView;
            }
        }



        private void CarregarAggregationViews()
        {
            var viewPrincipal = list.DefaultView;

            var calendarios = CarregaConfiguracoes(list);

            viewPrincipal.CalendarSettings = ConstroiNovaStringConfiguracao(calendarios);

            viewPrincipal.Update();
        }

        private string ConstroiNovaStringConfiguracao(AggregationCalendars calendarios)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<AggregationCalendar>));
            StringBuilder resultado = new StringBuilder();
            StringWriter sw = new StringWriter(resultado);

            serializer.Serialize(sw, calendarios.AggregationCalendar);

            var viewsAgregadas = string.Format("<AggregationCalendars>{0}</AggregationCalendars>",
                resultado
                    .ToString()
                    .Substring(172)
                    .Replace("</ArrayOfAggregationCalendar>", "")
                    .Replace("\r", "")
                    .Replace("\n", ""));

            return viewsAgregadas;
        }

        private AggregationCalendars CarregaConfiguracoes(SPList list)
        {
            var viewPendentes = list.Views["Pendentes"];
            var viewCancelados = list.Views["Rejeitados"];

            var calendarios = new AggregationCalendars();
            calendarios.AggregationCalendar = new List<AggregationCalendar>();
            calendarios.AggregationCalendar.Add(new AggregationCalendar()
            {
                Id = String.Format("{{{0}}}", Guid.NewGuid()),
                Type = "SharePoint",
                Name = "Pendentes",
                Description = "Itens pendentes de aprovação",
                Color = "1",
                AlwaysShow = "False",
                CalendarUrl = list.ParentWeb.Url + "/" + viewPendentes.Url,
                Settings = new Settings()
                {
                    WebUrl = list.ParentWeb.Url,
                    ListId = String.Format("{{{0}}}", list.ID),
                    ViewId = String.Format("{{{0}}}", viewPendentes.ID),
                    ListFormUrl = list.ParentWeb.Url + "/" + list.Forms[PAGETYPE.PAGE_DISPLAYFORM].Url
                }
            });

            calendarios.AggregationCalendar.Add(new AggregationCalendar()
            {
                Id = String.Format("{{{0}}}", Guid.NewGuid()),
                Type = "SharePoint",
                Name = "Rejeitados",
                Description = "Itens rejeitados",
                Color = "3",
                AlwaysShow = "False",
                CalendarUrl = list.ParentWeb.Url + "/" + viewCancelados.Url,
                Settings = new Settings()
                {
                    WebUrl = list.ParentWeb.Url,
                    ListId = String.Format("{{{0}}}", list.ID),
                    ViewId = String.Format("{{{0}}}", viewCancelados.ID),
                    ListFormUrl = list.ParentWeb.Url + "/" + list.Forms[PAGETYPE.PAGE_DISPLAYFORM].Url
                }
            });
            return calendarios;
        }



        private void ExcluirAcao()
        {
            if (list.UserCustomActions.Any(x => x.Name == ConfiguracaoCalendario.ActionCriarCalendario))
            {
                var acao = list.UserCustomActions.First(x => x.Name == ConfiguracaoCalendario.ActionCriarCalendario);
                acao.Delete();
            }
        }




        private void SetCalendarViewStyle(string defaultView, string url)
        {
            SPLimitedWebPartManager mgr = null;

            
            mgr = web.GetLimitedWebPartManager(url, PersonalizationScope.Shared);

            foreach (System.Web.UI.WebControls.WebParts.WebPart wp in mgr.WebParts)
            {
                if (!(wp is ListViewWebPart))
                    continue;

                //Convert the webpart to list view webpart type
                ListViewWebPart listWP = wp as ListViewWebPart;

                if (listWP != null)
                {
                    PropertyInfo ViewProp = listWP.GetType().GetProperty("View", BindingFlags.NonPublic | BindingFlags.Instance);
                    SPView spView = ViewProp.GetValue(listWP, null) as SPView;
                    PropertyInfo viewProp = spView.GetType().GetProperty("SchemaXml", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                    string root = viewProp.GetValue(spView, null) as string;
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(root);
                    XmlNode node = doc.SelectSingleNode("/View/CalendarViewStyles");

                    if (node == null)
                    {
                        XmlNode stylesNodes = doc.CreateElement("CalendarViewStyles");
                        stylesNodes.InnerXml = SetViewStyleNode(defaultView);
                        doc.DocumentElement.AppendChild(stylesNodes);
                    }
                    else
                    {
                        node.InnerXml = SetViewStyleNode(defaultView);
                    }

                    PropertyInfo NodeProp = spView.GetType().GetProperty("Node", BindingFlags.NonPublic | BindingFlags.Instance);
                    XmlNode newNode = NodeProp.GetValue(spView, null) as XmlNode;
                    newNode.InnerXml = doc.DocumentElement.InnerXml;
                    spView.Update();
                    mgr.SaveChanges(listWP);
                }

            }
        }

        private string SetViewStyleNode(string viewStyleTypeAsDefault)
        {
            System.Text.StringBuilder sbViewStyleXml = new System.Text.StringBuilder(16);

            string[] viewStyleStr =
            {
                "",
                "<CalendarViewStyle Title='Week Group' Type='weekgroup' Template='CalendarViewweekgroupChrome' Sequence='2' Default='{0}' />",
                "<CalendarViewStyle Title='Day' Type='day' Template='CalendarViewdayChrome' Sequence='3' Default='{0}' />",
                "<CalendarViewStyle Title='Week' Type='week' Template='CalendarViewweekChrome' Sequence='4' Default='{0}' />",
                "<CalendarViewStyle Title='Month' Type='month' Template='CalendarViewmonthChrome' Sequence='5' Default='{0}' />"
            };

            switch (viewStyleTypeAsDefault)
            {
                case "daygroup":
                    viewStyleStr[0] = string.Format(viewStyleStr[0], "TRUE");
                    viewStyleStr[1] = string.Format(viewStyleStr[1], "FALSE");
                    viewStyleStr[2] = string.Format(viewStyleStr[2], "FALSE");
                    viewStyleStr[3] = string.Format(viewStyleStr[3], "FALSE");
                    viewStyleStr[4] = string.Format(viewStyleStr[4], "FALSE");
                    break;

                case "weekgroup":
                    viewStyleStr[0] = string.Format(viewStyleStr[0], "FALSE");
                    viewStyleStr[1] = string.Format(viewStyleStr[1], "TRUE");
                    viewStyleStr[2] = string.Format(viewStyleStr[2], "FALSE");
                    viewStyleStr[3] = string.Format(viewStyleStr[3], "FALSE");
                    viewStyleStr[4] = string.Format(viewStyleStr[4], "FALSE");
                    break;

                case "day":
                    viewStyleStr[0] = string.Format(viewStyleStr[0], "FALSE");
                    viewStyleStr[1] = string.Format(viewStyleStr[1], "FALSE");
                    viewStyleStr[2] = string.Format(viewStyleStr[2], "TRUE");
                    viewStyleStr[3] = string.Format(viewStyleStr[3], "FALSE");
                    viewStyleStr[4] = string.Format(viewStyleStr[4], "FALSE");
                    break;

                case "week":
                    viewStyleStr[0] = string.Format(viewStyleStr[0], "FALSE");
                    viewStyleStr[1] = string.Format(viewStyleStr[1], "FALSE");
                    viewStyleStr[2] = string.Format(viewStyleStr[2], "FALSE");
                    viewStyleStr[3] = string.Format(viewStyleStr[3], "TRUE");
                    viewStyleStr[4] = string.Format(viewStyleStr[4], "FALSE");
                    break;

                default: //this is for month view
                    viewStyleStr[0] = string.Format(viewStyleStr[0], "FALSE");
                    viewStyleStr[1] = string.Format(viewStyleStr[1], "FALSE");
                    viewStyleStr[2] = string.Format(viewStyleStr[2], "FALSE");
                    viewStyleStr[3] = string.Format(viewStyleStr[3], "FALSE");
                    viewStyleStr[4] = string.Format(viewStyleStr[4], "TRUE");
                    break;
            }

            foreach (string strStyle in viewStyleStr)
            {
                sbViewStyleXml.Append(strStyle);
            }

            string viewStyleXml = sbViewStyleXml.ToString();
            sbViewStyleXml = null;
            return viewStyleXml;
        }
    }
}
