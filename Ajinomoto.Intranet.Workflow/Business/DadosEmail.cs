﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajinomoto.Intranet.Workflow.Business
{
    public class DadosEmail
    {
        private StringBuilder mensagem = new StringBuilder();

        public string De { get; set; }
        public string Para { get; set; }
        public string Assunto { get; set; }
        public string Corpo
        {
            get
            {
                return mensagem.ToString();
            }
        }

        public void AdicionarLinha(string linha)
        {
            mensagem.AppendLine(CodificarEmHtml(linha) + "<br />");
        }

        public void AdicionarLinhaComLink(string url, string texto)
        {
            mensagem.AppendLine(
                string.Format("<a href='{0}'>{1}</a>", 
                    CodificarEmHtml(url), 
                    CodificarEmHtml(texto)));
        }

        public void LimparCorpoEmail()
        {
            mensagem = new StringBuilder();
        }

        private static string CodificarEmHtml(string linha)
        {
            return System.Web.HttpUtility.HtmlEncode(linha);
        }
    }
}
