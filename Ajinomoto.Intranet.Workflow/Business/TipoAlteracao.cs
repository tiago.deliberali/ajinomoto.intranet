﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajinomoto.Intranet.Workflow.Business
{
    public enum TipoAlteracao
    {
        SemAlteracao = 1,
        ItemCriado,
        ItemAprovado,
        ItemReprovado
    }
}
