﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajinomoto.Intranet.Workflow.Business
{
    public class Evento
    {
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Local { get; set; }

        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }

        public string CriadoPor { get; set; }
        public string Email { get; set; }

        public bool DiaInteiro { get; set; }
        public string Lista { get; set; }

        public List<KeyValuePair<string, string>> ValoresParaEmail { get; set; }
    }
}
