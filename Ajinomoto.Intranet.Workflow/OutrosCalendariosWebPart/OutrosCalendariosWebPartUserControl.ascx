﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OutrosCalendariosWebPartUserControl.ascx.cs" Inherits="Ajinomoto.Intranet.Workflow.OutrosCalendariosWebPart.OutrosCalendariosWebPartUserControl" %>

<%@ Import Namespace="Ajinomoto.Intranet.Workflow" %>
<%@ Import Namespace="Ajinomoto.Intranet.Workflow.OutrosCalendariosWebPart" %>

<style>
    .item_lista 
    {
        float: left; 
        display: block; 
        margin: 5px; 
        padding: 5px;
        font-family: Verdana, Arial, sans-serif;
        font-size: 8pt;
    }
    
    .item_lista a
    {
        color: #003759 !important;   
    }
    
    .item_calendario
    {
        border: 1px solid #003759;
        background-color: rgb(224, 232, 242);
    }
    
    #retorna A:visited 
    {
	    COLOR: blue
    }
</style>
<asp:Repeater runat="server" ID="CalendariosRepeater">
    <HeaderTemplate>
        <div style="display: block; float: left; padding-top: 5px;">
            <div class="item_lista">
                <%= ((OutrosCalendariosWebPart)this.Parent).Texto %></div>
    </HeaderTemplate>
    <ItemTemplate>
        <div class="item_lista item_calendario">
            <a href="<%# ((Calendario)Container.DataItem).Url %>"><%# ((Calendario)Container.DataItem).Nome%></a>
        </div>
    </ItemTemplate>
    <FooterTemplate>
        </div>
        <div style="clear: both;"></div>
    </FooterTemplate>
</asp:Repeater>
<asp:Panel runat="server" ID="detalhesPanel">
    <div dir="ltr" style="text-align: center; padding-top: 5px;">
        <div class="ms-rteThemeForeColor-2-0">
            <table width="98%" class="ms-rteTable-default ms-rteThemeForeColor-2-0" cellspacing="0" style="font-size: 1em; margin: 5px;">
                <tbody>
                    <tr class="ms-rteTableHeaderRow-default ms-rteThemeBackColor-1-2 ms-rteThemeForeColor-2-0">
                        <% foreach (string titulo in this.CalendarioWebPart.Titulos.Split(';')) { %>
		                    <th class="ms-rteTableHeaderFirstCol-default ms-rteThemeBackColor-1-2 ms-rteThemeForeColor-2-0">
                                <%= titulo %>
                            </th>
	                    <% } %>
                    </tr>
                    <tr>
                        <% foreach (string valor in this.CalendarioWebPart.Valores.Split(';')) { %>
		                    <td>
                                <%= valor %>
                            </td>
	                    <% } %>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Panel>