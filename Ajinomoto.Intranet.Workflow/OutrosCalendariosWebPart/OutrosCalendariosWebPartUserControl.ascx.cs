﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Collections.Generic;
using System.Linq;

namespace Ajinomoto.Intranet.Workflow.OutrosCalendariosWebPart
{
    public partial class OutrosCalendariosWebPartUserControl : UserControl
    {
        public OutrosCalendariosWebPart CalendarioWebPart { get; set; }
        public delegate bool MecanismoBusca(string valor);
        
        protected void Page_Load(object sender, EventArgs e)
        {
            detalhesPanel.Visible = 
                CalendarioWebPart.ExibirDetalhes
                && !string.IsNullOrEmpty(CalendarioWebPart.Titulos)
                && !string.IsNullOrEmpty(CalendarioWebPart.Valores);

            var listas = SPContext.Current.Web.Lists;

            var contemTermo = DefineMecanismoDeBusca();

            if (listas != null)
            {
                var calendarios = new List<Calendario>();

                foreach (SPList lista in listas)
                {
                    if (lista.BaseTemplate == SPListTemplateType.Events
                        && contemTermo(lista.Description)
                        && !Request.Url.ToString().Contains(lista.DefaultViewUrl))
                    {
                        calendarios.Add(new Calendario()
                        {
                            Nome = lista.Title,
                            Url = lista.DefaultViewUrl
                        });
                    }
                }

                if (calendarios.Count > 0)
                {
                    CalendariosRepeater.DataSource = calendarios.OrderBy(x => x.Nome);
                    CalendariosRepeater.DataBind();
                }
            }
        }

        private MecanismoBusca DefineMecanismoDeBusca()
        {
            MecanismoBusca busca;
            string filtro = CalendarioWebPart.FiltroNome;

            if (string.IsNullOrEmpty(filtro))
                busca = SemFiltro;
            else if (filtro.StartsWith("^"))
                busca = BuscarNoInicio;
            else if (filtro.StartsWith("$"))
                busca = BuscarNoFim;
            else
                busca = Buscar;

            return busca;
        }

        public bool BuscarNoInicio(string valor)
        {
            return valor.StartsWith(CalendarioWebPart.FiltroNome.Substring(1), StringComparison.InvariantCultureIgnoreCase);
        }

        public bool BuscarNoFim(string valor)
        {
            return valor.EndsWith(CalendarioWebPart.FiltroNome.Substring(1), StringComparison.InvariantCultureIgnoreCase);
        }

        public bool Buscar(string valor)
        {
            return valor.ToLower().Contains(CalendarioWebPart.FiltroNome.ToLower());
        }

        public bool SemFiltro(string valor)
        {
            return true;
        }
    }
}
