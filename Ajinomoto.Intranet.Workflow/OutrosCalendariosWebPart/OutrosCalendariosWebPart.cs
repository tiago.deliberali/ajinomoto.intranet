﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace Ajinomoto.Intranet.Workflow.OutrosCalendariosWebPart
{
    [ToolboxItemAttribute(false)]
    public class OutrosCalendariosWebPart : WebPart
    {
        [WebBrowsable,
        Category("Ajinomoto"),
        Personalizable(PersonalizationScope.Shared),
        WebDisplayName("Filtro"),
        WebDescription("Palavra a ser procurada na descrição dos calendários existentes para serem apresentados. Use ^ para procurar no início, $ para procurar no final. Fora esses casos, a palavra será procurada em qualquer parte do nome.")]
        public string FiltroNome { get; set; }

        [WebBrowsable,
        Category("Ajinomoto"),
        Personalizable(PersonalizationScope.Shared),
        WebDisplayName("Texto"),
        WebDescription("Texto apresentado antes da lista de calendários")]
        public string Texto { get; set; }




        [WebBrowsable,
        Category("Ajinomoto"),
        Personalizable(PersonalizationScope.Shared),
        WebDisplayName("Exibir detalhes"),
        WebDescription("Exibe ou oculta os detalhes da sala")]
        public bool ExibirDetalhes { get; set; }

        [WebBrowsable,
        Category("Ajinomoto"),
        Personalizable(PersonalizationScope.Shared),
        WebDisplayName("Títulos"),
        WebDescription("Títulos separados por ;")]
        public string Titulos { get; set; }

        [WebBrowsable,
        Category("Ajinomoto"),
        Personalizable(PersonalizationScope.Shared),
        WebDisplayName("Valores"),
        WebDescription("Valores separados por ;")]
        public string Valores { get; set; }

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Ajinomoto.Intranet.Workflow/OutrosCalendariosWebPart/OutrosCalendariosWebPartUserControl.ascx";

        protected override void CreateChildControls()
        {
            Control control = Page.LoadControl(_ascxPath);
            if (control != null)
            {
                ((OutrosCalendariosWebPartUserControl)control).CalendarioWebPart = this;
            }
            Controls.Add(control);
        }
    }
}
