using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Security;
using Ajinomoto.Intranet.Business;
using System.Linq;
using Ajinomoto.Intranet.Workflow.Business;

namespace Ajinomoto.Intranet.Workflow.Features.CalendariosFeature
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("afd66b79-8415-4d89-b2ea-235a97ab4720")]
    public class CalendariosFeatureEventReceiver : SPFeatureReceiver
    {
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            try
            {
                SPSite siteCollection = properties.Feature.Parent as SPSite;
                CarregaConfiguracoes(siteCollection.RootWeb);
                CarregaActionCriacaoDeCalendarioComAprovacao(siteCollection);

                CriarListaDeConfiguracoesParaTodosWeb(siteCollection);
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex, "Application");
            }
        }

        private void CriarListaDeConfiguracoesParaTodosWeb(SPSite siteCollection)
        {
            foreach (SPWeb web in siteCollection.AllWebs)
            {
                Configuracao.CriarListaConfiguracao(web);
            }
        }

        private void CarregaActionCriacaoDeCalendarioComAprovacao(SPSite site)
        {
            var criacao = new PreparoCalendario(site);
            criacao.AdicionarActionParaTodasListas();
        }

        private void CarregaConfiguracoes(SPWeb site)
        {
            var config = new Configuracao(site);

            config.CadastrarConfiguracao("emailFrom_AprovacaoSala", "intranet@br.ajinomoto.com", "Remetente dos emails enviados pelo workflow de aprova��o");
            config.CadastrarConfiguracao("grupoAprovacaoSala", "AprovacaoSala", "Grupo que cont�m os aprovadores de ocupa��o");
        }
    }
}
