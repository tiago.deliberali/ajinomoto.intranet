﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Ajinomoto.Intranet.Workflow.Business;
using System.Linq;
using System.Collections.Specialized;

namespace Ajinomoto.Intranet.Workflow.Layouts1.Ajinomoto.Intranet.Workflow
{
    public partial class FluxoAprovacao : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!SPContext.Current.Web.DoesUserHavePermissions(SPBasePermissions.ManageLists))
                throw new Exception("Você não tem autorização para exibir esse conteúdo");

            var web = new Guid(Request["web"]);
            var lista = new Guid(Request["lista"]);

            PreparoCalendario calendario = 
                new PreparoCalendario(SPContext.Current.Site);

            calendario.Preparar(web, lista);
            
        }
    }
}
