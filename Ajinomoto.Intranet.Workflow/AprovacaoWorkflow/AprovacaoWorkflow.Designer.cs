﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace Ajinomoto.Intranet.Workflow.AprovacaoWorkflow
{
    public sealed partial class AprovacaoWorkflow
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind3 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind4 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind5 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind6 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference1 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference2 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference3 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.ComponentModel.ActivityBind activitybind7 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind8 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind10 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken1 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind9 = new System.Workflow.ComponentModel.ActivityBind();
            this.logToHistoryListActivity2 = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.logToHistoryListActivity3 = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.logToHistoryListActivity1 = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.faultHandlerActivity1 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.itemReprovadoEmail = new System.Workflow.Activities.CodeActivity();
            this.enviaConfirmacaoParaOutros = new System.Workflow.Activities.CodeActivity();
            this.itemAprovadoEmail = new System.Workflow.Activities.CodeActivity();
            this.itemCriadoEmail = new System.Workflow.Activities.CodeActivity();
            this.generalFaultHandlerActivity = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.cancellationHandlerActivity1 = new System.Workflow.ComponentModel.CancellationHandlerActivity();
            this.faultHandlersActivity1 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifReprovado = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifItemAprovado = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifItemCriado = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlersActivity2 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifTipoAlteracao = new System.Workflow.Activities.IfElseActivity();
            this.logInicio = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.onWorkflowActivated1 = new Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated();
            // 
            // logToHistoryListActivity2
            // 
            this.logToHistoryListActivity2.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logToHistoryListActivity2.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind1.Name = "AprovacaoWorkflow";
            activitybind1.Path = "HistoryDescription";
            activitybind2.Name = "AprovacaoWorkflow";
            activitybind2.Path = "HistoryOutcome";
            this.logToHistoryListActivity2.Name = "logToHistoryListActivity2";
            this.logToHistoryListActivity2.OtherData = "";
            this.logToHistoryListActivity2.UserId = -1;
            this.logToHistoryListActivity2.MethodInvoking += new System.EventHandler(this.logToHistoryListActivity2_MethodInvoking);
            this.logToHistoryListActivity2.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            this.logToHistoryListActivity2.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            // 
            // logToHistoryListActivity3
            // 
            this.logToHistoryListActivity3.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logToHistoryListActivity3.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind3.Name = "AprovacaoWorkflow";
            activitybind3.Path = "HistoryDescription";
            activitybind4.Name = "AprovacaoWorkflow";
            activitybind4.Path = "HistoryOutcome";
            this.logToHistoryListActivity3.Name = "logToHistoryListActivity3";
            this.logToHistoryListActivity3.OtherData = "";
            this.logToHistoryListActivity3.UserId = -1;
            this.logToHistoryListActivity3.MethodInvoking += new System.EventHandler(this.logToHistoryListActivity3_MethodInvoking);
            this.logToHistoryListActivity3.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind3)));
            this.logToHistoryListActivity3.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind4)));
            // 
            // logToHistoryListActivity1
            // 
            this.logToHistoryListActivity1.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logToHistoryListActivity1.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind5.Name = "AprovacaoWorkflow";
            activitybind5.Path = "HistoryDescription";
            activitybind6.Name = "AprovacaoWorkflow";
            activitybind6.Path = "HistoryOutcome";
            this.logToHistoryListActivity1.Name = "logToHistoryListActivity1";
            this.logToHistoryListActivity1.OtherData = "";
            this.logToHistoryListActivity1.UserId = -1;
            this.logToHistoryListActivity1.MethodInvoking += new System.EventHandler(this.logToHistoryListActivity1_MethodInvoking_1);
            this.logToHistoryListActivity1.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind5)));
            this.logToHistoryListActivity1.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind6)));
            // 
            // faultHandlerActivity1
            // 
            this.faultHandlerActivity1.Activities.Add(this.logToHistoryListActivity2);
            this.faultHandlerActivity1.FaultType = typeof(System.Exception);
            this.faultHandlerActivity1.Name = "faultHandlerActivity1";
            // 
            // itemReprovadoEmail
            // 
            this.itemReprovadoEmail.Name = "itemReprovadoEmail";
            this.itemReprovadoEmail.ExecuteCode += new System.EventHandler(this.itemReprovadoEmail_ExecuteCode);
            // 
            // enviaConfirmacaoParaOutros
            // 
            this.enviaConfirmacaoParaOutros.Name = "enviaConfirmacaoParaOutros";
            this.enviaConfirmacaoParaOutros.ExecuteCode += new System.EventHandler(this.enviaConfirmacaoParaOutros_ExecuteCode);
            // 
            // itemAprovadoEmail
            // 
            this.itemAprovadoEmail.Name = "itemAprovadoEmail";
            this.itemAprovadoEmail.ExecuteCode += new System.EventHandler(this.itemAprovadoEmail_ExecuteCode);
            // 
            // itemCriadoEmail
            // 
            this.itemCriadoEmail.Name = "itemCriadoEmail";
            this.itemCriadoEmail.ExecuteCode += new System.EventHandler(this.itemCriadoEmail_ExecuteCode);
            // 
            // generalFaultHandlerActivity
            // 
            this.generalFaultHandlerActivity.Activities.Add(this.logToHistoryListActivity3);
            this.generalFaultHandlerActivity.FaultType = typeof(System.Exception);
            this.generalFaultHandlerActivity.Name = "generalFaultHandlerActivity";
            // 
            // cancellationHandlerActivity1
            // 
            this.cancellationHandlerActivity1.Activities.Add(this.logToHistoryListActivity1);
            this.cancellationHandlerActivity1.Name = "cancellationHandlerActivity1";
            // 
            // faultHandlersActivity1
            // 
            this.faultHandlersActivity1.Activities.Add(this.faultHandlerActivity1);
            this.faultHandlersActivity1.Name = "faultHandlersActivity1";
            // 
            // ifReprovado
            // 
            this.ifReprovado.Activities.Add(this.itemReprovadoEmail);
            ruleconditionreference1.ConditionName = "ItemReprovado";
            this.ifReprovado.Condition = ruleconditionreference1;
            this.ifReprovado.Name = "ifReprovado";
            // 
            // ifItemAprovado
            // 
            this.ifItemAprovado.Activities.Add(this.itemAprovadoEmail);
            this.ifItemAprovado.Activities.Add(this.enviaConfirmacaoParaOutros);
            ruleconditionreference2.ConditionName = "ItemAprovado";
            this.ifItemAprovado.Condition = ruleconditionreference2;
            this.ifItemAprovado.Name = "ifItemAprovado";
            // 
            // ifItemCriado
            // 
            this.ifItemCriado.Activities.Add(this.itemCriadoEmail);
            ruleconditionreference3.ConditionName = "ItemCriado";
            this.ifItemCriado.Condition = ruleconditionreference3;
            this.ifItemCriado.Name = "ifItemCriado";
            // 
            // faultHandlersActivity2
            // 
            this.faultHandlersActivity2.Activities.Add(this.generalFaultHandlerActivity);
            this.faultHandlersActivity2.Name = "faultHandlersActivity2";
            // 
            // ifTipoAlteracao
            // 
            this.ifTipoAlteracao.Activities.Add(this.ifItemCriado);
            this.ifTipoAlteracao.Activities.Add(this.ifItemAprovado);
            this.ifTipoAlteracao.Activities.Add(this.ifReprovado);
            this.ifTipoAlteracao.Activities.Add(this.faultHandlersActivity1);
            this.ifTipoAlteracao.Activities.Add(this.cancellationHandlerActivity1);
            this.ifTipoAlteracao.Name = "ifTipoAlteracao";
            // 
            // logInicio
            // 
            this.logInicio.Description = "Log";
            this.logInicio.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logInicio.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind7.Name = "AprovacaoWorkflow";
            activitybind7.Path = "HistoryDescription";
            activitybind8.Name = "AprovacaoWorkflow";
            activitybind8.Path = "HistoryOutcome";
            this.logInicio.Name = "logInicio";
            this.logInicio.OtherData = "";
            this.logInicio.UserId = -1;
            this.logInicio.MethodInvoking += new System.EventHandler(this.logToHistoryListActivity1_MethodInvoking);
            this.logInicio.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind7)));
            this.logInicio.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind8)));
            activitybind10.Name = "AprovacaoWorkflow";
            activitybind10.Path = "workflowId";
            // 
            // onWorkflowActivated1
            // 
            correlationtoken1.Name = "workflowToken";
            correlationtoken1.OwnerActivityName = "AprovacaoWorkflow";
            this.onWorkflowActivated1.CorrelationToken = correlationtoken1;
            this.onWorkflowActivated1.EventName = "OnWorkflowActivated";
            this.onWorkflowActivated1.Name = "onWorkflowActivated1";
            activitybind9.Name = "AprovacaoWorkflow";
            activitybind9.Path = "workflowProperties";
            this.onWorkflowActivated1.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.onWorkflowActivated1_Invoked);
            this.onWorkflowActivated1.SetBinding(Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated.WorkflowIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind10)));
            this.onWorkflowActivated1.SetBinding(Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated.WorkflowPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind9)));
            // 
            // AprovacaoWorkflow
            // 
            this.Activities.Add(this.onWorkflowActivated1);
            this.Activities.Add(this.logInicio);
            this.Activities.Add(this.ifTipoAlteracao);
            this.Activities.Add(this.faultHandlersActivity2);
            this.Name = "AprovacaoWorkflow";
            this.CanModifyActivities = false;

        }

        #endregion

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logToHistoryListActivity3;

        private FaultHandlerActivity generalFaultHandlerActivity;

        private FaultHandlersActivity faultHandlersActivity2;

        private CodeActivity enviaConfirmacaoParaOutros;

        private CodeActivity itemReprovadoEmail;

        private CodeActivity itemCriadoEmail;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logToHistoryListActivity2;

        private FaultHandlerActivity faultHandlerActivity1;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logToHistoryListActivity1;

        private CancellationHandlerActivity cancellationHandlerActivity1;

        private FaultHandlersActivity faultHandlersActivity1;

        private CodeActivity itemAprovadoEmail;

        private IfElseBranchActivity ifReprovado;

        private IfElseBranchActivity ifItemAprovado;

        private IfElseBranchActivity ifItemCriado;

        private IfElseActivity ifTipoAlteracao;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logInicio;

        private Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated onWorkflowActivated1;









































































    }
}
