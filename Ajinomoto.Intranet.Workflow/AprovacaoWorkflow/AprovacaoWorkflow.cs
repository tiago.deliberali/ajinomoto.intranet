﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;
using Microsoft.SharePoint.WorkflowActions;
using Ajinomoto.Intranet.Workflow.Business;
using Ajinomoto.Intranet.Business;
using DDay.iCal;
using DDay.iCal.Serialization.iCalendar;
using System.Net.Mime;
using System.Net.Mail;
using System.Collections.Generic;

namespace Ajinomoto.Intranet.Workflow.AprovacaoWorkflow
{
    public sealed partial class AprovacaoWorkflow : SequentialWorkflowActivity
    {
        public AprovacaoWorkflow()
        {
            InitializeComponent();
        }

        public Guid workflowId = default(System.Guid);
        public SPWorkflowActivationProperties workflowProperties = new SPWorkflowActivationProperties();




        public TipoAlteracao Alteracao = TipoAlteracao.SemAlteracao;

        private void onWorkflowActivated1_Invoked(object sender, ExternalDataEventArgs e)
        {
            var eventos = new HandleEventFiring();

            try
            {
                Logger.RegistrarAviso("wf item: " + workflowProperties.ItemId + " - Nome: " + workflowProperties.Item.Title);
                eventos.DesabilitarEventos();

                var aprovacao = workflowProperties.Item.ModerationInformation;

                SPField statusAnteriorField = workflowProperties.Item.Fields.TryGetFieldByStaticName("StatusAnterior");
                SPField dataIncialAnteriorField = workflowProperties.Item.Fields.TryGetFieldByStaticName("DataInicialAnterior");
                SPField dataIncialField = workflowProperties.Item.Fields.TryGetFieldByStaticName("EventDate");

                DateTime dataInicial = Convert.ToDateTime(workflowProperties.Item[dataIncialField.Id]);
                DateTime dataInicialAnterior = dataInicial;

                if (workflowProperties.Item[dataIncialAnteriorField.Id] != null)
                    dataInicialAnterior = Convert.ToDateTime(workflowProperties.Item[dataIncialAnteriorField.Id]);

                if (workflowProperties.Item[statusAnteriorField.Id] == null)
                {
                    workflowProperties.Item[statusAnteriorField.Id] = (int)aprovacao.Status;
                    workflowProperties.Item[dataIncialAnteriorField.Id] = dataInicial;
                    workflowProperties.Item.SystemUpdate();

                    Alteracao = Business.TipoAlteracao.ItemCriado;
                }
                else
                {
                    SPModerationStatusType statusAnterior =
                            (SPModerationStatusType)Enum.ToObject(
                                typeof(SPModerationStatusType),
                                Convert.ToInt32(workflowProperties.Item[statusAnteriorField.Id]));

                    if (statusAnterior != aprovacao.Status)
                    {
                        if (aprovacao.Status == SPModerationStatusType.Approved)
                            Alteracao = Business.TipoAlteracao.ItemAprovado;

                        else if (aprovacao.Status == SPModerationStatusType.Denied)
                            Alteracao = Business.TipoAlteracao.ItemReprovado;

                        else if (aprovacao.Status == SPModerationStatusType.Pending)
                            Alteracao = TipoAlteracao.ItemCriado; // mesmo comportamento de um item novo

                        workflowProperties.Item[statusAnteriorField.Id] = (int)aprovacao.Status;
                        workflowProperties.Item.SystemUpdate();
                    }
                    else if (!dataInicialAnterior.Equals(dataInicial))
                    {
                        workflowProperties.Item[dataIncialAnteriorField.Id] = dataInicial;
                        workflowProperties.Item.SystemUpdate();

                        Alteracao = Business.TipoAlteracao.ItemCriado; // mesmo comportamento de um item novo
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                eventos.HabilitarEventos();
            }
        }




        public String HistoryDescription = default(System.String);
        public String HistoryOutcome = default(System.String);

        private void logToHistoryListActivity1_MethodInvoking(object sender, EventArgs e)
        {
            HistoryDescription = "Workflow executado";
            HistoryOutcome = Alteracao.ToString();
        }

        public DadosEmail email = new DadosEmail();
        public Evento evento = new Evento();

        private void itemCriadoEmail_ExecuteCode(object sender, EventArgs e)
        {
            var site = workflowProperties.Web;
            var config = new Configuracao(workflowProperties.Site.RootWeb);

            CarregaDadosEvento();

            email.De = config["emailFrom_AprovacaoSala"];

            email.Para = ObterEmailsParaEnvio(site, config, string.Empty);

            email.Assunto = "Solicitação: " +
                workflowProperties.List.Title +
                " em " + evento.Inicio.ToString();

            PreencheCorpoEmail(string.Empty);

            MailMessage message = PreparaMensagem();

            EnviarMensagem(message);
        }

        private MailMessage PreparaMensagem()
        {
            MailMessage message = new MailMessage();

            message.BodyEncoding =
                System.Text.Encoding.GetEncoding(
                    workflowProperties.Site.WebApplication.OutboundMailCodePage);

            foreach (var mailTo in email.Para.Split(';'))
            {
                if (!string.IsNullOrEmpty(mailTo))
                    message.To.Add(new MailAddress(mailTo));
            }

            message.From = new MailAddress(email.De);
            message.Subject = email.Assunto;
            message.Body = email.Corpo;
            message.IsBodyHtml = true;

            return message;
        }

        private string ObterEmailsParaEnvio(SPWeb site, Configuracao config, string padrao)
        {
            var configAprovadores = new Configuracao(site);

            string aprovadores = string.Empty;

            var nomeGrupoAprovacao = configAprovadores[padrao + workflowProperties.List.Title];
            if (string.IsNullOrEmpty(nomeGrupoAprovacao))
                return string.Empty;

            var grupo = site.SiteGroups[nomeGrupoAprovacao];

            foreach (SPUser usuario in grupo.Users)
                if (!string.IsNullOrEmpty(usuario.Email))
                    aprovadores += ";" + usuario.Email;

            if (!string.IsNullOrEmpty(aprovadores))
                aprovadores = aprovadores.Substring(1);

            return aprovadores;
        }



        private void itemReprovadoEmail_ExecuteCode(object sender, EventArgs e)
        {
            PrepararEmail("Solicitação Negada");

            MailMessage message = PreparaMensagem();

            EnviarMensagem(message);
        }

        private void itemAprovadoEmail_ExecuteCode(object sender, EventArgs e)
        {
            PrepararEmail("Solicitação Aprovada");

            MailMessage message = PreparaMensagemCalendario();

            message.Subject = evento.Titulo;

            EnviarMensagem(message);
        }

        private void EnviarMensagem(MailMessage message)
        {
            SmtpClient client =
                new SmtpClient(
                    workflowProperties.Site.WebApplication.OutboundMailServiceInstance.Parent.Name);

            client.Send(message);
        }



        private void PrepararEmail(string status)
        {
            var site = workflowProperties.Web;
            var config = new Configuracao(workflowProperties.Site.RootWeb);

            CarregaDadosEvento();

            var aprovacao = workflowProperties.Item.ModerationInformation;

            email.De = config["emailFrom_AprovacaoSala"];

            email.Para = evento.Email;

            email.Assunto = status +
                ": " + workflowProperties.List.Title +
                " em " + evento.Inicio.ToString();

            PreencheCorpoEmail(aprovacao.Comment);
        }

        private MailMessage PreparaMensagemCalendario()
        {
            MailMessage message = PreparaMensagem();

            message.Attachments.Add(CriaAnexoICal());

            return message;
        }

        private System.Net.Mail.Attachment CriaAnexoICal()
        {
            string icalData = PreparaICalData();

            System.Net.Mail.Attachment attachment =
                System.Net.Mail.Attachment.CreateAttachmentFromString(
                icalData,
                new ContentType("text/calendar"));

            attachment.TransferEncoding = TransferEncoding.Base64;
            attachment.Name = "EventDetails.ics"; //not visible in outlook

            return attachment;
        }

        private string PreparaICalData()
        {
            iCalendar iCal = new iCalendar();

            // outlook 2003 needs this property,
            //  or we'll get an error (a Lunar error!)
            //iCal.Method = "PUBLISH";
            iCal.Method = "REQUEST";

            // Create the event
            Event evt = iCal.Create<Event>();

            evt.Summary = evento.Titulo;

            evt.Start =
                new iCalDateTime(
                    evento.Inicio.Year,
                    evento.Inicio.Month,
                    evento.Inicio.Day,
                    evento.Inicio.Hour,
                    evento.Inicio.Minute,
                    evento.Inicio.Second);

            evt.Duration = evento.Fim - evento.Inicio;
            evt.Description = evento.Descricao;

            evt.Location = evento.Lista;
            if (!string.IsNullOrEmpty(evento.Local))
                evt.Location += " - " + evento.Local;

            evt.IsAllDay = evento.DiaInteiro;

            //if (recurrenceDaysInterval > 0)
            //{
            //    RecurrencePattern rp = new RecurrencePattern();
            //    rp.Frequency = FrequencyType.Daily;
            //    rp.Interval = recurrenceDaysInterval; // interval of days

            //    rp.Count = recurrenceCount;
            //    evt.RecurrenceRules.Add(rp);
            //}

            //organizer is mandatory for outlook 2007 - think about
            // trowing an exception here.
            if (String.IsNullOrEmpty(evento.CriadoPor))
                throw new Exception("Organizer não pode ficar vazio!");

            evt.Organizer = new Organizer();
            evt.Organizer.CommonName = evento.CriadoPor;
            evt.Organizer.SentBy = new Uri("MAILTO:" + evento.Email);
            evt.Organizer.Value = new Uri("MAILTO:" + evento.Email);
            //evt.Organizer.CommonName = "Intranet sharepoint";
            //evt.Organizer.SentBy = new Uri("MAILTO:intranet@br.ajinomoto.com");
            //evt.Organizer.Value = new Uri("MAILTO:intranet@br.ajinomoto.com");

            //"REQUEST" will update an existing event with the same
            // UID (Unique ID) and a newer time stamp.
            //evt.UID = Guid.NewGuid().ToString();
            //iCal.Method = "REQUEST";

            // extra fields
            iCal.ProductID = "Microsoft Exchange Server 2010";

            evt.Attendees = new List<IAttendee>();
            evt.Attendees.Add(new Attendee()
            {
                Role = "REQ-PARTICIPANT",
                ParticipationStatus = "NEEDS-ACTION",
                RSVP = true,
                CommonName = evento.CriadoPor,
                Value = new Uri("MAILTO:" + evento.Email)
            });

            evt.Status = EventStatus.Confirmed;

            evt.UID = Guid.NewGuid().ToString();
            evt.Class = "PUBLIC";
            evt.Transparency = TransparencyType.Opaque;
            evt.Priority = 5;

            // Save into calendar file.
            iCalendarSerializer serializer =
              new iCalendarSerializer();
            //serializer.Serialize(@"iCalendar.ics");

            string icalData = serializer.SerializeToString(iCal);

            return icalData;
        }





        private void CarregaDadosEvento()
        {
            var site = workflowProperties.Web;
            var item = workflowProperties.Item;

            var startDateField = workflowProperties.Item.Fields.GetFieldByInternalName("EventDate");
            var endDateField = workflowProperties.Item.Fields.GetFieldByInternalName("EndDate");
            var createdByField = workflowProperties.Item.Fields.GetFieldByInternalName("Author");
            var locationField = workflowProperties.Item.Fields.GetFieldByInternalName("Location");
            var allDayField = workflowProperties.Item.Fields.GetFieldByInternalName("fAllDayEvent");
            var descriptionField = workflowProperties.Item.Fields.GetFieldByInternalName("Description");

            var config = new Configuracao(workflowProperties.Site.RootWeb);
            var fields = new List<SPField>();
            var camposParaEmail = config["CamposParaEmail"];

            foreach (var campo in camposParaEmail.Split(';'))
            {
                if (workflowProperties.Item.Fields.ContainsFieldWithStaticName(campo))
                    fields.Add(workflowProperties.Item.Fields.GetFieldByInternalName(campo));
            }

            var usuario = new SPFieldUserValue(site, item[createdByField.Id].ToString());
            evento.CriadoPor = usuario.User.Name;
            evento.Email = usuario.User.Email;

            evento.Inicio = Convert.ToDateTime(item[startDateField.Id]);
            evento.Fim = Convert.ToDateTime(item[endDateField.Id]);
            evento.Local = Convert.ToString(item[locationField.Id]);
            evento.DiaInteiro = Convert.ToBoolean(item[allDayField.Id]);
            evento.Titulo = item.Title;
            evento.Lista = workflowProperties.List.Title;
            evento.Descricao = Convert.ToString(item[descriptionField.Id]);

            evento.ValoresParaEmail = new List<KeyValuePair<string, string>>();

            foreach (var field in fields)
            {
                if (field != null)
                    evento.ValoresParaEmail.Add(
                        new KeyValuePair<string, string>(
                            field.Title,
                            Convert.ToString(item[field.Id])));
            }
        }

        private void PreencheCorpoEmail(string comentarios)
        {
            SPListItem item = workflowProperties.Item;

            email.AdicionarLinha("Dados da solicitação:");
            email.AdicionarLinha("");
            email.AdicionarLinha(evento.Lista);
            email.AdicionarLinha("Título: " + item.Title);
            email.AdicionarLinha("Início: " + evento.Inicio.ToString());
            email.AdicionarLinha("Fim: " + evento.Fim.ToString());

            email.AdicionarLinha("");
            foreach (var valor in evento.ValoresParaEmail)
            {
                email.AdicionarLinha(valor.Key + ": " + FormatarValor(valor.Value));
            }

            if (!string.IsNullOrEmpty(comentarios))
            {
                email.AdicionarLinha("");
                email.AdicionarLinha("Comentários:");
                email.AdicionarLinha(comentarios);
            }

            email.AdicionarLinha("");
            email.AdicionarLinhaComLink(
                workflowProperties.Site.Url + workflowProperties.List.DefaultViewUrl
                    + "?CalendarDate=" + evento.Inicio.ToString("yyyy-MM-dd"),
                "Veja a solicitação na sua lista");
        }

        private string FormatarValor(string valor)
        {
            if (string.IsNullOrEmpty(valor))
                return string.Empty;

            switch (valor.ToLower())
            {
                case "true":
                    return "Sim";
                case "false":
                    return "Não";
                default:
                    return valor;
            }
        }

        private void logToHistoryListActivity1_MethodInvoking_1(object sender, EventArgs e)
        {
            HistoryDescription = "Atividade cancelada";
            HistoryOutcome = "Atividade cancelada";
        }

        private void logToHistoryListActivity2_MethodInvoking(object sender, EventArgs e)
        {
            var ex = faultHandlerActivity1.Fault;

            while (ex.InnerException != null)
                ex = ex.InnerException;

            HistoryOutcome = ex.Message;
            HistoryDescription = ex.StackTrace;
        }


        private void enviaConfirmacaoParaOutros_ExecuteCode(object sender, EventArgs e)
        {
            try
            {
                var site = workflowProperties.Web;
                var config = new Configuracao(workflowProperties.Site.RootWeb);

                CarregaDadosEvento();

                email.De = config["emailFrom_AprovacaoSala"];

                email.Para = ObterEmailsParaEnvio(site, config, "PosAprovacao_");

                email.Assunto = "Solicitação aprovada: " +
                    workflowProperties.List.Title +
                    " em " + evento.Inicio.ToString();

                // corpo já está preenchido...

                MailMessage message = PreparaMensagem();

                EnviarMensagem(message);
            }
            catch (Exception ex)
            {
                Logger.RegistrarErro(ex);
            }
        }

        private void logToHistoryListActivity3_MethodInvoking(object sender, EventArgs e)
        {
            var ex = generalFaultHandlerActivity.Fault;

            while (ex.InnerException != null)
                ex = ex.InnerException;

            HistoryOutcome = ex.Message;
            HistoryDescription = ex.StackTrace;
        }
    }
}
