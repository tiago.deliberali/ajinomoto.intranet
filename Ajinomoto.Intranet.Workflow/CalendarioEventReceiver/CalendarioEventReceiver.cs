﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Security;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;
using Ajinomoto.Intranet.Workflow.Business;
using Ajinomoto.Intranet.Business;

namespace Ajinomoto.Intranet.Workflow.CalendarioEventReceiver
{
    /// <summary>
    /// List Events
    /// </summary>
    public class CalendarioEventReceiver : SPListEventReceiver
    {
       /// <summary>
       /// A list was added.
       /// </summary>
       public override void ListAdded(SPListEventProperties properties)
       {
           base.ListAdded(properties);

           Configuracao.CriarListaConfiguracao(properties.Web);

           var criacao = new PreparoCalendario(new SPSite(properties.SiteId));
           criacao.AdicionarAction(properties.Web, properties.List);
       }


    }
}
